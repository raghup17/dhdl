import dhdl.graph._
import dhdl.codegen._
import dhdl.DHDLTest
// import dhdl.DHDLApp
// import dhdl.DHDLTest

object ThreeDConv extends DHDLTest {

  def tileConvolverTile(tileSize: Int, yTileSize: Int, offset: CombNode,
                        tileX: MemNode, tileH: MemNode, tileY: MemNode, 
                        X: OffChipArray, H: OffChipArray, Y: OffChipArray, 
                        XD1: CombNode, XD2: CombNode, XD3: CombNode,
                        HD1: CombNode, HD2: CombNode, HD3: CombNode,
                        YD1: CombNode, YD2: CombNode, YD3: CombNode ) = {
    // calculate two addresses: 
    // 1. address of Y[m,n,o] in offchipArray Y. This is used to calculate the convolution product correctly by using: 
    // y[m,n,o] = \sum_{i=-\infty}^\infty \sum_{j=-\infty}^\infty \sum_{k=-\infty}^\infty x[i,j,k] * h[m-i,n-j,o-k]

    // 2. address of Y[m,n,o] in BRAM. This is used to store the element to the correct address once an output data point
    // is calculated
    val bramCtrCol = Ctr((Const(yTileSize), 1))
    val bramCtr = bramCtrCol(0)
    val addrY = bramCtr // address for storing in BRAM of Y 
    val yAbsAddr = Add(bramCtr, offset)
    val d1Step = Mul(YD2, YD3)
    val d2Step = YD3

    val m = Div(yAbsAddr, d1Step) // m = yAbsAddr / (YD2 * YD3)

    // val n = Div(Sub(yAbsAddr, Mul(d1Step, m)), d2Step) // n = (yAbsAddr - m * (YD2*YD3)) / YD3
    val mMultiple = Mul(d1Step, m)
    val mOffset = Sub(yAbsAddr, mMultiple)
    val n = Div(mOffset, d2Step)

    // val o = Sub(yAbsAddr, Add(Mul(d1Step, m), Mul(d2Step, n))) // o = (yAbsAddr - m * (YD2*YD3) - n * YD3)
    val nMultiple = Mul(d2Step, n)
    val nOffset = Add(mMultiple, nMultiple)
    val o = Sub(yAbsAddr, nOffset)

    // accumulator
    val convSum = Reg()

    // get the bound of the cube for convolution
    // x1LrBd = bigger(0, m-h1D+1) if 0 > m-h1D+1 <=> h1D > m+1
    // x1UpBd = smaller(x1D-1, m)
    // h1LrBd = bigger(0, m-x1D+1)
    // h1UpBd = smaller(h1D-1, m)

    // val x1LrBd = Mux(Gt(HD1, Add(m, Const(1))), Const(0), Add(Sub(m, HD1), Const(1)))
    val add_m_1 = Add(m, Const(1))
    val gt_hd1_add_m_1 = Gt(HD1, add_m_1)
    val sub_m_hd1 = Sub(m, HD1)
    val add_sub_m_hd1_1 = Add(sub_m_hd1, Const(1))
    val x1LrBd = Mux(gt_hd1_add_m_1, Const(0), add_sub_m_hd1_1)

    // val x1UpBd = Mux(Lt(Sub(XD1, Const(1)), m), Sub(XD1, Const(1)), m)
    val sub_xd1_1 = Sub(XD1, Const(1))
    val lt_sub_xd1_1_m = Lt(sub_xd1_1, m)
    val x1UpBd = Mux(lt_sub_xd1_1_m, sub_xd1_1, m)

    // val h1LrBd = Mux(Gt(XD1, Add(m, Const(1))), Const(0), Add(Sub(m, XD1), Const(1)))
    val gt_xd1_add_m_1 = Gt(XD1, add_m_1)
    val sub_m_xd1 = Sub(m, XD1)
    val add_sub_m_xd1_1 = Add(sub_m_xd1, Const(1))
    val h1LrBd = Mux(gt_xd1_add_m_1, Const(0), add_sub_m_xd1_1)

    // val h1UpBd = Mux(Lt(Sub(HD1, Const(1)), m), Sub(HD1, Const(1)), m)
    val sub_hd1_1 = Sub(HD1, Const(1))
    val lt_sub_hd1_1_m = Lt(sub_hd1_1, m)
    val h1UpBd = Mux(lt_sub_hd1_1_m, sub_hd1_1, m)

    // x2LrBd = bigger(0, n-h2D+1)
    // x2UpBd = smaller(x2D-1, n)
    // h2LrBd = bigger(0, n-x2D+1)
    // h2UpBd = smaller(h2D-1, n)
    // val x2LrBd = Mux(Gt(HD2, Add(n, Const(1))), Const(0), Add(Sub(n, HD2), Const(1)))
    val add_n_1 = Add(n, Const(1))
    val gt_hd2_add_n_1 = Gt(HD2, add_n_1)
    val sub_n_hd2 = Sub(n, HD2)
    val add_sub_n_hd2_1 = Add(sub_n_hd2, Const(1))
    val x2LrBd = Mux(gt_hd2_add_n_1, Const(0), add_sub_n_hd2_1)

    // val x2UpBd = Mux(Lt(Sub(XD2, Const(1)), n), Sub(XD2, Const(1)), n)
    val sub_xd2_1 = Sub(XD2, Const(1))
    val lt_sub_xd2_1_n = Lt(sub_xd2_1, n)
    val x2UpBd = Mux(lt_sub_xd2_1_n, sub_xd2_1, n)

    // val h2LrBd = Mux(Gt(XD2, Add(n, Const(1))), Const(0), Add(Sub(n, XD2), Const(1)))
    val gt_xd2_add_n_1 = Gt(XD2, add_n_1)
    val sub_n_xd2 = Sub(n, XD2)
    val add_sub_n_xd2_1 = Add(sub_n_xd2, Const(1))
    val h2LrBd = Mux(gt_xd2_add_n_1, Const(0), add_sub_n_xd2_1)

    // val h2UpBd = Mux(Lt(Sub(HD2, Const(1)), n), Sub(HD2, Const(1)), n)
    val sub_hd2_1 = Sub(HD2, Const(1))
    val lt_sub_hd2_1_n = Lt(sub_hd2_1, n)
    val h2UpBd = Mux(lt_sub_hd2_1_n, sub_hd2_1, n)

    // x3LrBd = bigger(0, o-h3D+1)
    // x3UpBd = smaller(x3D-1, o)
    // h3LrBd = bigger(0, o-x3D+1)
    // h3UpBd = smaller(h3D-1, o)
    // val x3LrBd = Mux(Gt(HD3, Add(o, Const(1))), Const(0), Add(Sub(o, HD3), Const(1)))
    val add_o_1 = Add(o, Const(1))
    val gt_hd3_add_o_1 = Gt(HD3, add_o_1)
    val sub_o_hd3 = Sub(o, HD3)
    val add_sub_o_hd3_1 = Add(sub_o_hd3, Const(1))
    val x3LrBd = Mux(gt_hd3_add_o_1, Const(0), add_sub_o_hd3_1)

    // val x3UpBd = Mux(Lt(Sub(XD3, Const(1)), o), Sub(XD3, Const(1)), o)
    val sub_xd3_1 = Sub(XD3, Const(1))
    val lt_sub_xd3_1_o = Lt(sub_xd3_1, o)
    val x3UpBd = Mux(lt_sub_xd3_1_o, sub_xd3_1, o)

    // val h3LrBd = Mux(Gt(XD3, Add(o, Const(1))), Const(0), Add(Sub(o, XD3), Const(1)))
    val gt_xd3_add_o_1 = Gt(XD3, add_o_1)
    val sub_o_xd3 = Sub(o, XD3)
    val add_sub_o_xd3_1 = Add(sub_o_xd3, Const(1))
    val h3LrBd = Mux(gt_xd3_add_o_1, Const(0), add_sub_o_xd3_1)

    // val h3UpBd = Mux(Lt(Sub(HD3, Const(1)), o), Sub(HD3, Const(1)), o)
    val sub_hd3_1 = Sub(HD3, Const(1))
    val lt_sub_hd3_1_o = Lt(sub_hd3_1, o)
    val h3UpBd = Mux(lt_sub_hd3_1_o, sub_hd3_1, o)

    val hd1RangeTmp = Sub(h1UpBd, h1LrBd)
    val hd1Range = Add(hd1RangeTmp, Const(1))
    val hd2RangeTmp = Sub(h2UpBd, h2LrBd)
    val hd2Range = Add(hd2RangeTmp, Const(1))
    val hd3RangeTmp = Sub(h3UpBd, h3LrBd)
    val hd3Range = Add(hd3RangeTmp, Const(1))

    val addrRangePipe = Pipe(Ctr((Const(1), 1))(0), addrY, yAbsAddr, d1Step, d2Step, mMultiple, mOffset, 
                                                    nMultiple, nOffset,  m, n, o, 
                                                    x1LrBd, add_m_1, gt_hd1_add_m_1, sub_m_hd1, add_sub_m_hd1_1,
                                                    x1UpBd, sub_xd1_1, lt_sub_xd1_1_m,
                                                    h1LrBd, gt_xd1_add_m_1, sub_m_xd1, add_sub_m_xd1_1,
                                                    h1UpBd, sub_hd1_1, lt_sub_hd1_1_m, 
                                                    x2LrBd, add_n_1, gt_hd2_add_n_1, sub_n_hd2, add_sub_n_hd2_1,
                                                    x2UpBd, sub_xd2_1, lt_sub_xd2_1_n,
                                                    h2LrBd, gt_xd2_add_n_1, sub_n_xd2, add_sub_n_xd2_1,
                                                    h2UpBd, sub_hd2_1, lt_sub_hd2_1_n, 
                                                    x3LrBd, add_o_1, gt_hd3_add_o_1, sub_o_hd3, add_sub_o_hd3_1, 
                                                    x3UpBd, sub_xd3_1, lt_sub_xd3_1_o,
                                                    h3LrBd, gt_xd3_add_o_1, sub_o_xd3, add_sub_o_xd3_1, 
                                                    h3UpBd, sub_hd3_1, lt_sub_hd3_1_o,
                                                    hd1RangeTmp, hd1Range, hd2RangeTmp, hd2Range, hd3RangeTmp, hd3Range)


    // ijk ctr for convolution
    val ijkCtr = Ctr((hd1Range, 1), (hd2Range, 1), (hd3Range, 1))
    val i = Add(ijkCtr(0), h1LrBd) 
    val j = Add(ijkCtr(1), h2LrBd)
    val k = ijkCtr(2)

    val loadSize = Add(Sub(h3UpBd,h3LrBd), Const(1))
    val hd1Step = Mul(HD2, HD3)
    val hLoadAddr1D = Mul(hd1Step, i)
    val hLoadAddr2D = Mul(j, HD3)
    val hloadAddrTmp = Add(hLoadAddr1D, hLoadAddr2D)
    val hloadAddr = Add(hloadAddrTmp, h3LrBd)

    val hLoadAddrPipe = Pipe(Ctr((Const(1), 1))(0), loadSize, hd1Step, hLoadAddr1D, hLoadAddr2D, hloadAddrTmp, hloadAddr)

    val hValLd = TileMemLd(H, Mul(Mul(XD1, XD2), XD3), Const(0), hloadAddr, tileH, 1, tileSize)

    val xd1Step = Mul(XD2, XD3)
    val xd1Offset = Sub(m,j)
    val xLoadAddr1D = Mul(xd1Step, xd1Offset)
    val xd2Offset = Sub(n,j)
    val xLoadAddr2D = Mul(XD3, xd2Offset)
    val xLoadAddrTmp = Add(xLoadAddr1D, xLoadAddr2D)
    val xLoadAddr = Add(xLoadAddrTmp, x3LrBd)

    val xLoadAddrPipe = Pipe(Ctr((Const(1), 1))(0), xd1Step, xd1Offset, xLoadAddr1D, xd2Offset, xLoadAddr2D, xLoadAddrTmp, xLoadAddr)

    val xValLd = TileMemLd(X, Mul(Mul(XD1,XD2), XD3), Const(0), xLoadAddr, tileX, 1, tileSize)
    
    // val hVal = Ld(tileH, Sub(Sub(loadSize, k), Const(1)))
    val loadHReverseAddrTmp = Sub(loadSize, k)
    val loadHRerverse = Sub(loadHReverseAddrTmp, Const(1))
    val hVal = Ld(tileH, loadHRerverse)
    val xVal = Ld(tileX, k)
    val convProduct = Mul(hVal, xVal)
    val sum = Add(convSum, convProduct)
    convSum.write(sum)

    val convAddPipe = Pipe(Ctr((Const(1), 1))(0), loadHReverseAddrTmp, loadHRerverse, hVal, xVal, convProduct, sum)

    val st = St(tileY, addrY, convSum)
    val storePipe = Pipe(Ctr((Const(1), 1))(0), st)

    // reset the i,j,k accum counter when i,j,k are saturated
    val convSequential = Sequential(ijkCtr, hLoadAddrPipe, xLoadAddrPipe, Parallel(xValLd, hValLd), convAddPipe)

    // reset the convolutional sum when ijkCtr reaches 0
    convSum.setParent(convSequential)
    
    val convolution = Sequential(bramCtrCol, addrRangePipe, convSequential, storePipe)
    convolution
  }

  // Algo: 
  // 1. Use three BRAMs to store temporary results of convolution: tileX, tileH, tileY
  // 2. The size of tileH is specified by hd3. It should be large enough to hold the smallest dimension of kernel, H. 
  // 3. To calculate one output data point, the 3D convolution app does dotproduct over the third dimension of H, X, 
  //  and iterate through the first two dimensions. Then the app accumulates the convolutional sum and store it back
  // to BRAM Y
  // 4. Once BRAM Y is filled out, we store it back to the offchip memory
  def threeDConvolve(hd3: Int, yTileSize: Int) = {
    val tileSize = hd3

    // Get sizes of matrices from host
    val XD1 = ArgIn("XD1")
    val XD2 = ArgIn("XD2")
    val XD3 = ArgIn("XD3")
    val HD1 = ArgIn("HD1")
    val HD2 = ArgIn("HD2")
    val HD3 = ArgIn("HD3")
    val YD1 = ArgIn("YD1")
    val YD2 = ArgIn("YD2")
    val YD3 = ArgIn("YD3")

    // Declare off-chip matrix arrays that are the targets for convolution. Y = X * H
    val X = OffChipArray("X", XD1, XD2, XD3) // get input
    val H = OffChipArray("H", HD1, HD2, HD3) // get kernel
    val Y = OffChipArray("Y", YD1, YD2, YD3) // store convolution output

    // BRAMS on the tile for temporary storage
    val tileH = BRAM(hd3)
    val tileX = BRAM(hd3)
    val tileY = BRAM(yTileSize)

    // assume that the size of Offchiparray of Y is a multiple of BRAM(yTileSize)
    val tileYCtr = Ctr((Mul(Mul(YD1,YD2), YD3), yTileSize))

    // we assume that the offchip memory is linear
    val yTileLd = TileMemLd(Y, Mul(Mul(YD1,YD2), YD3), Const(0), tileYCtr(0), tileY, 1, yTileSize)

    // We provide three BRAMS for doing convolutions. 
    val tileConvolver = tileConvolverTile (
                        tileSize = tileSize,
                        yTileSize = yTileSize,
                        offset = tileYCtr(0),
                        tileX = tileX,
                        tileH = tileH,
                        tileY = tileY,
                        X = X,
                        H = H, 
                        Y = Y, 
                        XD1 = XD1,
                        XD2 = XD2,
                        XD3 = XD3,
                        HD1 = HD1,
                        HD2 = HD2,
                        HD3 = HD3,
                        YD1 = YD1,
                        YD2 = YD2,
                        YD3 = YD3
                        )
    val maxYOffchipSizeTmp = Mul(YD1, YD2)
    val maxYOffchipSize = Mul(maxYOffchipSizeTmp, YD3)
    val tileSt = TileMemSt(Y, maxYOffchipSize, Const(0), tileYCtr(0), tileY, 1, yTileSize)
    val top = Sequential(tileYCtr, yTileLd, tileConvolver, tileSt)
    top
  }
    
  // Assumption: assuming that kernel (H) always have no more points than X. 
  def main(args: String*) = {
    if (args.size != 2) {
      println("Usage: ThreeDConv <hTileSize> <yTileSize>.")
      println("Assume that hd3 is also the length of the tile to simplify computation")
      sys.exit(0)
    }

    val hd3 = args(0).toInt
    val yTileSize = args(1).toInt
    threeDConvolve(hd3, yTileSize)
  }

  def test(args: String*) {
    val hd3 = args(0).toInt
    val yTileSize = args(1).toInt
    val XD1 = 3
    val XD2 = 3
    val XD3 = 3
    val HD1 = 2
    val HD2 = 2
    val HD3 = 2
    val YD1 = 4
    val YD2 = 4
    val YD3 = 4

    setArg("XD1", XD1)
    setArg("XD2", XD2)
    setArg("XD3", XD3)
    setArg("HD1", HD1)
    setArg("HD2", HD2)
    setArg("HD3", HD3)
    setArg("YD1", YD1)
    setArg("YD2", YD2)
    setArg("YD3", YD3)

    val xVec = Array(5,1,2,
                    7,0,0,     
                    8,4,5,
                    0,3,3,
                    1,5,1,
                    9,6,7,
                    4,3,1,
                    3,8,1,
                    6,7,2)

    val hVec = Array(2,3,
                     3,5,
                     4,7,
                     7,6)

    val yVecResult = Array(10, 17, 7, 6, 
                        29, 49, 11, 10, 
                        37, 67, 22, 15, 
                        24, 52, 35, 25, 
                        20, 45, 30, 23, 
                        65, 108, 61, 30, 
                        102, 173, 108, 61, 
                        83, 139, 110, 65, 
                        8, 30, 44, 24, 
                        22, 102, 122, 33, 
                        64, 199, 175, 66, 
                        81, 147, 126, 52, 
                        16, 40, 25, 7,
                        40, 98, 85, 13, 
                        45, 144, 112, 20,
                        42, 85, 56, 12)
    setMem("X", xVec)
    setMem("H", hVec)

    run()
    val yConvResult = getMem[Int]("Y", 0 until YD1 * YD2 * YD3)

    compare(yConvResult, yVecResult)
  }
}
