import dhdl.graph._
import dhdl.codegen._
import dhdl.Design

object SimpleCtr extends Design {

  def main(args: String*) = {
    if (args.size != 1) {
      println("\nUsage: SimpleCtr <#par>")
      sys.exit(0)
    }
    val par = args(0).toInt

    // Fix an arbitrary tile size
    val tileSize = 24 * 96
    val max = ArgIn("max")

    // Off-chip memory output
    val arrayOut = OffChipArray("arrayOut", max)

    // Stage 2: Do something to the tile
    val outRAM = BRAM(tileSize)
    val ctr = Ctr(List(par))((max, 1))
    val value = Add(Mul(ctr(0), Const(2)), Const(1))
    val st = St(outRAM, ctr(0), value)
    val pipe = Pipe(ctr, GraphNode(st))

    // Stage 3: Store tile back
    val tileSt = TileMemSt(arrayOut, max, Const(0), Const(0), outRAM, 1, tileSize)

    // Wire up mpCtr and the stages as a MetaPipeline
    val top = Sequential(pipe, tileSt)
    top
  }
}


