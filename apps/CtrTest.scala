import dhdl.graph._
import dhdl.codegen._
import dhdl.DesignTest

object CtrTest extends DesignTest {

  def main(args: String*) = {
    if (args.size < 1) {
      println(s"Usage: CtrTest <i|j>")
      sys.exit(-1)
    }

    val resetAfterJ = args(0) == "j"

    val resOff = OffChipArray(UInt())("res", Const(96))
    val res = BRAM(96)
    val a = ArgIn(UInt())("a")
    val b = ArgIn(UInt())("b")
    val ctr = Ctr((a,1), (b,1))
    val i = ctr(0)
    val j = ctr(1)
    val reg = Reg(UInt())
    val add = Add(j, reg)
    reg.write(add)
    val stage1 = Stage(GraphNode(j), ReduceTree(j, reg, GraphNode(add)))
    val stage2 = Stage(St(res, i, reg))
    val pipe = Pipe(ctr, stage1, stage2)
    reg.setParent(pipe)


    if (resetAfterJ) { // Reset register when j reaches max
      println(s"""Resetting register with j""")
      reg.reset = Block {
        Eq(j, Sub(b, Const(1)))
      }
    }
    val tilest = TileMemSt(resOff, Const(96), Const(0), Const(0), res, 1, 96)
    tilest.force = Const(true)
    val top = Sequential(pipe, tilest)
   	top
  }

  def test(args: String*) {
    setArg("a", 10)
    run()
  }
}


