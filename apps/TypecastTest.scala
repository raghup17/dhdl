import dhdl.graph._
import dhdl.codegen._
import dhdl.DesignTest

object TypecastTest extends DesignTest {

  def main(args: String*) = {

    // Read two arguments in (ArgIn) - one integer, one float
    val aInt = ArgIn("aInt")
    val bFloat = ArgIn(FloatPt())("bFloat")

    val aFloat = Fix2Float(aInt, FloatPt())
    val bInt = Float2Fix(bFloat, UInt())

    //Wire everything up as a Pipeline
    val top = Pipe(ArgOut("aFloat", aFloat), ArgOut("bInt", bInt))
   	top
  }

  def test(args: String*) {
    setArg("aInt", 5)
    setArg("bFloat", 3.14)

    run()

    val aFloat = getArg[Double]("aFloat")
    val bInt = getArg[Int]("bInt")

    compare(aFloat, 5.0)
    compare(bInt, 3)
  }
}


