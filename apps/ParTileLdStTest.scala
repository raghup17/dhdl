import dhdl.graph._
import dhdl.codegen._
import dhdl.DesignTest

object ParTileLdStTest extends DesignTest {

  def main(args: String*) = {
    if (args.size != 1) {
      println("\nUsage: ParTileLdStTest <vecTileSize>")
      sys.exit(0)
    }
    val vecSize = ArgIn("vecSize")
    val tileSize = args(0).toInt

    // Off-chip memory input
    val vec1 = OffChipArray("vecIn1", vecSize)
    val vec2 = OffChipArray("vecIn2", vecSize)
    val vec3 = OffChipArray("vecIn3", vecSize)

    // Off-chip memory output
    val vecOut = OffChipArray("vecOut", vecSize)

//    val seqCtr = Ctr((vecSize,tileSize))
    val tileRAM1 = BRAM(tileSize)
    val tileRAM2 = BRAM(tileSize)
    val tileRAM3 = BRAM(tileSize)
//    val tileLd = TileMemLd(vec, vecSize, Const(0), seqCtr(0), tileRAM, 1, tileSize)
    val tileLd1 = TileMemLd(vec1, vecSize, Const(0), Const(0), tileRAM1, 1, tileSize)
    val tileLd2 = TileMemLd(vec2, vecSize, Const(0), Const(0), tileRAM2, 1, tileSize)
    val tileLd3 = TileMemLd(vec3, vecSize, Const(0), Const(0), tileRAM3, 1, tileSize)

    val prodRAM = BRAM(tileSize)
    val ctr = Ctr((Const(tileSize),1))
    val data1 = Ld(tileRAM1, ctr(0))
    val data2 = Ld(tileRAM2, ctr(0))
    val data3 = Ld(tileRAM3, ctr(0))
    val add1 = Add(data1, data2)
    val add2 = Add(add1, data3)
    val st = St(prodRAM, ctr(0), add2)
    val pipe = Pipe(ctr, GraphNode(data1, data2, data3, add1, add2, st))

//    val tileSt = TileMemSt(vecOut, vecSize, Const(0), seqCtr(0), prodRAM, 1, tileSize)
    val tileSt = TileMemSt(vecOut, vecSize, Const(0), Const(0), prodRAM, 1, tileSize)

//    val top = Sequential(seqCtr, tileLd, pipe, tileSt)
    val top = Sequential(Parallel(tileLd1, tileLd2, tileLd3), pipe, tileSt)
    top
  }

  def test(args: String*) {
    val tileSize = args(0.toInt)
    setArg("vecSize", 192)
    val data = Array.tabulate(192){i => i}
    setMem("vecIn", data)

    run()

    val out = getMem[Int]("vecIn", 0 until 192)
    compare(out, data)
  }
}


