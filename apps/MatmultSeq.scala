import dhdl.graph._
import dhdl.codegen._
import dhdl.DesignTest

object MatmultSeq extends DesignTest {

  def matmultTile(tileA: MemNode, tileB: MemNode, tileC: MemNode, bm: Int, bn: Int, bp: Int) = {
    val ijCtr = Ctr((Const(bm),1), (Const(bn),1))
    val i = ijCtr(0)
    val j = ijCtr(1)

    // Dot product
    val accum = Reg()
    val kCtr = Ctr(List(4))((Const(bp),1))
    val k = kCtr(0)
    val addra = Add(Mul(i, Const(bp)), k)
    val addrb = Add(Mul(k, Const(bn)), j)
    val addrc = Add(Mul(i, Const(bn)), j)
    val e1 = Ld(tileA, addra)
    val e2 = Ld(tileB, addrb)
    val prod = Mul(e1, e2)
    val sum = Add(accum, prod)
    accum.write(sum)
    val st = St(tileC, addrc, accum)
    val dotprodPipe = Pipe(kCtr, GraphNode(addra, addrb, e1, e2, prod), ReduceTree(prod, accum, sum))
    accum.setParent(dotprodPipe) // Accum is reset after dotprodPipe finishes execution

    val storePipe = Pipe(addrc, st)
    val matmult = Sequential(ijCtr, dotprodPipe, storePipe)
    matmult
  }

  def tileAdderPipe(sumTile: MemNode, accumTile: MemNode, multTile: MemNode, sourceSel: CombNode, bm: Int, bn: Int) = {
    val bound = Mul(Const(bm), Const(bn))
    val ctr = Ctr(List(4))((bound,1))
    val i = ctr(0)
    val accumTileVal = Ld(accumTile, i)
    val sumTileVal = Ld(sumTile, i)
    val multTileVal = Ld(multTile, i)
    val sum = Add(Mux(sourceSel, accumTileVal, sumTileVal), multTileVal)
    val st = St(sumTile, i, sum)
    val tileAdder = Pipe(ctr, GraphNode(accumTileVal, sumTileVal, multTileVal, sum, st))
    tileAdder
  }

  def matmult(bm: Int, bp: Int, bn: Int) = {
    // Get sizes of matrices from host
    val M = ArgIn("M")
    val N = ArgIn("N")
    val P = ArgIn("P")

    // Declare off-chip matrix arrays
    val M1 = OffChipArray("M1", M, P)
    val M2 = OffChipArray("M2", P, N)
    val M3 = OffChipArray("M3", M, N)

    val seqCtr = Ctr((M,bm), (N,bn), (P,bp))

    val i = seqCtr(0)
    val j = seqCtr(1)
    val k = seqCtr(2)

    // Load M1(i,k)
    val m1Tile = BRAM(bm*bp)
    val m1TileLd = TileMemLd(M1, P, i, k, m1Tile, bm, bp)

    // Load M2(k,j)
    val m2Tile = BRAM(bp*bn)
    val m2TileLd = TileMemLd(M2, N, k, j, m2Tile, bp, bn)

    // multTile = m1Tile x m2Tile
    val multTile = BRAM(bm*bn)
    val tileMultiplier = matmultTile (
                  tileA = m1Tile,
                  tileB = m2Tile,
                  tileC = multTile,
                  bm = bm,
                  bp = bp,
                  bn = bn
                 )

    // Load M3(i,j)
    val accumTile = BRAM(bm*bn)
    val tileAccumLd = TileMemLd(M3, N, i, j, accumTile, bm, bn)

    // sumTile = multTile + (tileAccumLd.isLoading ? accumTile : sumTile)
    val sumTile = BRAM(bm*bn)
    val dblreg = Reg(Bool(), true)
    dblreg.write(tileAccumLd.isLoading)
    dblreg.setWriter(tileAccumLd)
    val tileAdder = tileAdderPipe(sumTile, accumTile, multTile, dblreg, bm, bn)
    dblreg.addReader(tileAdder)
    dblreg.setParent(tileAdder)

    // Store sumTile to M3(i,j)
    val tileSt = TileMemSt(M3, N, i, j, sumTile, bm, bn)

    val top = Sequential(seqCtr, Parallel(m1TileLd, m2TileLd), Parallel(tileMultiplier, tileAccumLd), tileAdder, tileSt)
    top
  }


  def main(args: String*) = {
    if (args.size != 3) {
      println("Usage: MatmultSeq <bm> <bn> <bp>")
      sys.exit(0)
    }
    val bm = args(0).toInt
    val bn = args(1).toInt
    val bp = args(2).toInt
    matmult(bm, bn, bp)
//    DummyCtrlNode()
  }
  def test(args: String*) {
    val bm = args(0).toInt
    val bn = args(1).toInt
    val bp = args(2).toInt

    val M = 5*bm; val N = 5*bn; val P = 5*bp
    setArg("M", M)
    setArg("N", N)
    setArg("P", P)

    val m1 = Array.tabulate(M){i => Array.tabulate(N){j => i + j}}
    val m2 = Array.tabulate(N){i => Array.tabulate(P){j => i + j + 5}}

    setMem("M1", m1.flatten)
    setMem("M2", m2.flatten)

    run()

    val m2t = Array.tabulate(P){j => Array.tabulate(N){i => i + j + 5}}
    val m3Gold = Array.tabulate(M){i =>
      Array.tabulate(P){j =>
        m1(i).zip(m2t(j)).map{case (a,b) => a*b}.reduce(_+_)
      }
    }.flatten

    val m3 = getMem[Int]("M3", 0 until M*P)
    compare(m3, m3Gold)
  }
}
