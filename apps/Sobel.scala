
import dhdl.graph._
import dhdl.codegen._
import dhdl.Design

object Sobel extends Design {

  def main(args: String*) = {

    if (args.size != 1) {
      println("\nUsage: Sobel <#tileSize>")
      sys.exit(0)
    }
    val tileSize = args(0).toInt

    val imgW = ArgIn("imgW")
    val imgL = ArgIn("imgL")

		//TODO: Add support for constant size OFFChipArray
		val kernelW = ArgIn("kernelW")
		val kernelL = ArgIn("kernelL")
		//val dxOffChipMem = OffChipArray("dx",Const(3),Const(3))
		val dxOffChipMem = OffChipArray("dx",kernelW,kernelL)
		//val dyOffChipMem = OffChipArray("dy",Const(3),Const(3))
		val dyOffChipMem = OffChipArray("dy",kernelW,kernelL)
		val dx = BRAM(9, false)
		val dy = BRAM(9, false)
    val dxLd = TileMemLd(dyOffChipMem, Const(3), Const(0), Const(0), dy, 3, 3)
    val dyLd = TileMemLd(dxOffChipMem, Const(3), Const(0), Const(0), dx, 3, 3)
		val para_KernelsLd = Parallel(dxLd, dyLd)

		/** MetaPipeline - Ld, Sobel, St **/
		val tileCtrs = Ctr((imgL, tileSize), (imgW, tileSize))

		val img = OffChipArray("img", imgW, imgL)
    val tdImg = BRAM(tileSize*tileSize, false)
    val imgLd = TileMemLd(img, imgW, tileCtrs(0), tileCtrs(1), tdImg, tileSize, tileSize)

		/** MetaPipeline - Sobel **/
		val imgCtrs = Ctr((imgL, 1), (imgW, 1))

		val imgIdxMul = Mul(imgCtrs(0), Const(tileSize))
		val imgIdx = Add(imgIdxMul, imgCtrs(1))
		val pixel = Ld(tdImg, imgIdx)
		val pipe_idx_ld = Pipe(imgIdxMul, imgIdx, pixel)

		/** Pipe - Apply Kernel **/
		val kernelCtrs = Ctr((Const(3),1), (Const(3),1))

		/* Map */
		val kernelIdxMul = Mul(kernelCtrs(0),Const(3))
		val kernelIdx = Add(kernelIdxMul, kernelCtrs(1))
		val kernelX = Ld(dx, kernelIdx)
		val kernelY = Ld(dy, kernelIdx)
		val applyKernelX = Mul(pixel, kernelX)
		val applyKernelY = Mul(pixel, kernelY)

		/* Reduce */
		val accumX = Reg()
		val accumY = Reg()
		val accumXSum = Add(accumX, applyKernelX)
		val accumYSum = Add(accumY, applyKernelY)
		accumX.write(accumXSum)
		accumY.write(accumYSum)

		val pipe_appKernelXY = Pipe(kernelCtrs,
			GraphNode(kernelIdxMul,kernelIdx,kernelX,kernelY,applyKernelX,applyKernelY),
			ReduceTree(applyKernelX, accumX, accumXSum))
//			GraphNode(accumXSum, accumYSum))  <-- TODO: Need to have support in Pipe for multiple ReduceTrees
		/** END Pipe - Apply Kernel **/

		//TODO: Add Abs node
		//val absAccumX = Abs(accumX)
		val absAccumX = Not(accumX)
		//val absAccumY = Abs(accumY)
		val absAccumY = Not(accumY)
		val sumXY = Add(absAccumX, absAccumY)
		val gt255 = Gt(sumXY, Const(255))
		val pixelSobel = Mux(gt255, Const(255), sumXY)
		val tdImgSobel = BRAM(tileSize*tileSize, false)
		val tdImgSobelSt = St(tdImgSobel, imgIdx, pixelSobel)
		val pipe_sum_st = Pipe(absAccumX, absAccumY, sumXY, gt255, pixelSobel, tdImgSobelSt)

		val meta_sobel = MetaPipeline(imgCtrs, pipe_idx_ld, pipe_appKernelXY, pipe_sum_st)
		/** END MetaPipeline - Sobel **/

		val imgSobel = OffChipArray("imgSobel", imgW, imgL)
    val imgSobelSt = TileMemLd(imgSobel, imgW, tileCtrs(0), tileCtrs(1), tdImgSobel, tileSize, tileSize)

		val meta_ld_sobel_st = MetaPipeline(tileCtrs, imgLd, meta_sobel, imgSobelSt)
		/** END MetaPipeline - Ld, Sobel, St **/

		val top = Sequential(para_KernelsLd, meta_ld_sobel_st)
		top
  }
}
