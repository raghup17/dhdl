import dhdl.graph._
import dhdl.codegen._
import dhdl.Design

object PQTest extends Design {

  def main(args: String*) = {
    // Fix an arbitrary PQ size
    val pqSize = 96

    val max = ArgIn("max")

    // Off-chip memory output
    val arrayOut1 = OffChipArray("pqData", max)
    val arrayOut2 = OffChipArray("pqMetadata", max)
    val arrayOut3 = OffChipArray("bramOut", max)

    // Stage 2: Store numbers in descending order
    val outPQ = PQ(pqSize)
    val outRAM = BRAM(pqSize)
    val ctr = Ctr((max, 1))
    val value = Sub(max, ctr(0))
    val valueBundle = Bundle(value, ctr(0))
    val st1 = St(outPQ, ctr(0), valueBundle)
    val st2 = St(outRAM, ctr(0), value)
    val pipe = Pipe(ctr, GraphNode(value, valueBundle, st1, st2))

    // Stage 3: Wait for writes to settle in
    val waitCtr = Ctr((Const(pqSize-1), 1))
    val waitPipe = Pipe(waitCtr, GraphNode())

    // Stage 4: Split the data and metadata in the PQ into separate BRAMs
    val dataRAM = BRAM(pqSize)
    val metadataRAM = BRAM(pqSize)
    val splitCtr = Ctr((max,1))
    val ldBundle = Ld(outPQ, splitCtr(0))
    val data = ldBundle.get(0)
    val metadata = ldBundle.get(1)
    val st3 = St(dataRAM, splitCtr(0), data)
    val st4 = St(metadataRAM, splitCtr(0), metadata)
    val splitPipe = Pipe(splitCtr, GraphNode(ldBundle, data, metadata, st3, st4))

    // Stage 5: Store tile back
    val tileSt1 = TileMemSt(arrayOut1, max, Const(0), Const(0), dataRAM, 1, math.ceil(96.toDouble/pqSize).toInt * 96)
    val tileSt2 = TileMemSt(arrayOut2, max, Const(0), Const(0), metadataRAM, 1, math.ceil(96.toDouble/pqSize).toInt * 96)
    val tileSt3 = TileMemSt(arrayOut3, max, Const(0), Const(0), outRAM, 1, math.ceil(96.toDouble/pqSize).toInt * 96)

    // Wire up mpCtr and the stages as a MetaPipeline
    val top = Sequential(pipe, waitPipe, splitPipe, Parallel(tileSt1, tileSt2, tileSt3))
    top
  }
}


