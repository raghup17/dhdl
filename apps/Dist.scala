import dhdl.graph._
import dhdl.codegen._
import dhdl.DesignTest
import scala.math._

object Dist extends DesignTest {

	/* This class test ditance calculation of kmeans. The app takes two lists of points pAs and pBs
	 * Given indexes of pA and pB in pAs and pBs, two points are loaded and distance is computed. */

	def v_numA = 4
	def v_numB = 5
	def v_dim = 1

  def main(args: String*) = {
	  /* For all dimension i_dim */
	 	val pAIdx = ArgIn("pAIdx")
	 	val pBIdx = ArgIn("pBIdx")
		val numA = ArgIn("numA")
		val numB = ArgIn("numB")
		val dim = ArgIn("dim")
    val pAs = OffChipArray(FloatPt())("pAs", numA, dim)
    val pBs = OffChipArray(FloatPt())("pBs", numB, dim)
		val pAs_td = BRAM(FloatPt(),v_numA*v_dim,false)
		val pBs_td = BRAM(FloatPt(),v_numB*v_dim,false)
    val pALd = TileMemLd(pAs, dim, Const(0), Const(0), pAs_td, v_numA, v_dim)
    val pBLd = TileMemLd(pBs, dim, Const(0), Const(0), pBs_td, v_numB, v_dim)

		/* Following section should be separated into a function to reuse in Kmeans. */
	 	val ctrs_compDist = Ctr((dim, 1))         // for(i <- 0 until dim)
    val i_dim = ctrs_compDist(0)
		val pAdimIdxMul = Mul(pAIdx,dim)          // paIdx * N  ( = 0 )
    val pAdimIdx = Add(pAdimIdxMul,i_dim)     // i + paIdx * N
		val pBdimIdxMul = Mul(pBIdx,dim)          // paBIdx * N  ( = 0 )
    val pBdimIdx = Add(pBdimIdxMul,i_dim)     // i + pbIdx * N
    val distAccum = Reg(FloatPt())
    val pA_1d = Ld(pAs_td, pAdimIdx)          // a = A(i + pAIdx*N)
    val pB_1d = Ld(pBs_td, pBdimIdx)          // b = B(i + pBIdx*N)
    val diff = Sub(pA_1d, pB_1d)              // diff = a - b
    val sqrDiff = Mul(diff, diff)             // sqrDiff = diff * diff
    val sumSqrDiff = Add(distAccum, sqrDiff)  // accum += sqrDiff
    distAccum.write(sumSqrDiff)

		val pipe = Pipe(ctrs_compDist,
			GraphNode(pAdimIdxMul, pBdimIdxMul, pAdimIdx, pBdimIdx, pA_1d, pB_1d, diff, sqrDiff),
			ReduceTree(sqrDiff, distAccum, sumSqrDiff))
		distAccum.setParent(pipe)                 // accum resets every time pipe resets
		val top = Sequential(pALd, pBLd, pipe)

		val dist = ArgOut("dist", distAccum)
		dist.setParent(top)
		top
	}

  def test(args: String*) {
  	setArg("pAIdx", 0)
  	setArg("pBIdx", 0)
  	setArg("dim", v_dim)
		setArg("numA",v_numA)
		setArg("numB",v_numB)

  	val pAs = Array.tabulate(v_numA){i => Array.tabulate(v_dim){j => util.Random.nextDouble()*1000.0}}
  	val pBs = Array.tabulate(v_numB){i => Array.tabulate(v_dim){j => util.Random.nextDouble()*1000.0}}

  	setMem("pAs", pAs.flatten)
  	setMem("pBs", pBs.flatten)

		println("pAs:")
		pAs.foreach{point => println(point.mkString(", ")) }
		println("pBs:")
		pBs.foreach{cent => println(cent.mkString(", ")) }

  	run()

		val result = getReg[Double]("dist")
		println("Result: " + result)
  	//println(s"$result =?= $gold")
  	//compare(result, gold)
  }
}
