import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest}

object ArgBase extends DesignTest {

  def main(args: String*) = {
    assert(args.length == 1, "Usage ArgBase <#args>")
    val N = args(0).toInt

    val blk = Block {
      val argsIn  = List.tabulate(N){i => ArgIn(s"in$i") }
      val argsOut = List.tabulate(N){i => ArgOut(s"out$i", argsIn(i)) }
    }
    Pipe(blk,None)
  }

  def test(args: String*) {
    val N = args(0).toInt

    for (i <- 0 until N) { setArg(s"in$i", i) }
    run()
    for (i <- 0 until N) { compare(getArg[Int](s"out$i"), i) }
  }
}

// Creates N/2 ops using N inputs and N outputs (replicates outputs of each op 2x)
trait Characterization extends Design {
  val name: String
  val tp: Type
  def op(x: ArgIn, y: ArgIn): CombNode

  def main(args: String*) = {
    assert(args.length == 1, s"Usage name <#args>")
    val N = args(0).toInt

    val blk = Block {
      val argsIn  = List.tabulate(N+1){i => ArgIn(tp)(s"in$i") }
      val ops = List.tabulate(N){i => op(argsIn(i),argsIn(i + 1)) }

      val argsOut = List.tabulate(N){i => ArgOut(s"out$i", ops(i)) }
    }
    Pipe(blk, None)
  }

}

object AddSInt extends Characterization with DesignTest {
  val name = "AddSInt"
  val tp = SInt()
  def op(x: ArgIn, y: ArgIn) = Add(x,y)

  def test(args: String*) {
    val N = args(0).toInt
    for (i <- 0 until N) { setArg(s"in$i", i) }
    run()
    for (i <- 0 until N) { compare(getArg[Int](s"out$i"), 2*i + 1) }
  }
}

object MulSInt extends Characterization {
  val name = "MulSInt"
  val tp = SInt()
  def op(x: ArgIn, y: ArgIn) = Mul(x,y)
}
object SubSInt extends Characterization {
  val name = "SubSInt"
  val tp = SInt()
  def op(x: ArgIn, y: ArgIn) = Sub(x,y)
}
object DivSInt extends Characterization {
  val name = "DivSInt"
  val tp = SInt()
  def op(x: ArgIn, y: ArgIn) = Div(x,y)
}


object AddUInt extends Characterization {
  val name = "AddUInt"
  val tp = UInt()
  def op(x: ArgIn, y: ArgIn) = Add(x,y)
}
object MulUInt extends Characterization {
  val name = "MulUInt"
  val tp = UInt()
  def op(x: ArgIn, y: ArgIn) = Mul(x,y)
}
object SubUInt extends Characterization {
  val name = "SubUInt"
  val tp = UInt()
  def op(x: ArgIn, y: ArgIn) = Sub(x,y)
}
object DivUInt extends Characterization {
  val name = "DivUInt"
  val tp = UInt()
  def op(x: ArgIn, y: ArgIn) = Div(x,y)
}
object LtUInt extends Characterization {
  val name = "LtUInt"
  val tp = UInt()
  def op(x: ArgIn, y: ArgIn) = Lt(x,y)
}
object LeUInt extends Characterization {
  val name = "LeUInt"
  val tp = UInt()
  def op(x: ArgIn, y: ArgIn) = Le(x,y)
}

object AddFlt extends Characterization {
  val name = "AddFlt"
  val tp = Flt()
  def op(x: ArgIn, y: ArgIn) = Add(x,y)
}
object MulFlt extends Characterization {
  val name = "MulFlt"
  val tp = Flt()
  def op(x: ArgIn, y: ArgIn) = Mul(x,y)
}
object DivFlt extends Characterization {
  val name = "DivFlt"
  val tp = Flt()
  def op(x: ArgIn, y: ArgIn) = Div(x,y)
}
object LtFlt extends Characterization {
  val name = "LtFlt"
  val tp = Flt()
  def op(x: ArgIn, y: ArgIn) = Lt(x,y)
}
object EqFlt extends Characterization {
  val name = "EqFlt"
  val tp = Flt()
  def op(x: ArgIn, y: ArgIn) = Eq(x,y)
}
object LeFlt extends Characterization {
  val name = "LeFlt"
  val tp = Flt()
  def op(x: ArgIn, y: ArgIn) = Le(x,y)
}



object BitArgBase extends Design {
  def main(args: String*) = {
    assert(args.length == 1, "Usage BitArgBase <#args>")
    val N = args(0).toInt

    val blk = Block {
      val argsIn  = List.tabulate(N){i => ArgIn(Bool())(s"in$i") }
      val argsOut = List.tabulate(N){i => ArgOut(s"out$i", argsIn(i)) }
    }
    Pipe(blk, None)
  }
}

object NotBool extends Design {
  def main(args: String*) = {
    assert(args.length == 1, "Usage NotBool <#args>")
    val N = args(0).toInt

    val blk = Block {
      val argsIn  = List.tabulate(N){i => ArgIn(Bool())(s"in$i") }
      val nots    = argsIn.map{x => Not(x)}
      val argsOut = List.tabulate(N){i => ArgOut(s"out$i", nots(i)) }
    }
    Pipe(blk,None)
  }
}
object AndBool extends Characterization {
  val name = "AndBool"
  val tp = Bool()
  def op(x: ArgIn, y: ArgIn) = And(x,y)
}
object OrBool extends Characterization {
  val name = "OrBool"
  val tp = Bool()
  def op(x: ArgIn, y: ArgIn) = Or(x,y)
}

object LtSInt extends Characterization {
  val name = "LtSInt"
  val tp = SInt()
  def op(x: ArgIn, y: ArgIn) = Lt(x,y)
}
object LeSInt extends Characterization {
  val name = "LeSInt"
  val tp = SInt()
  def op(x: ArgIn, y: ArgIn) = Le(x,y)
}
object Eq32 extends Characterization {
  val name = "Eq32"
  val tp = SInt()
  def op(x: ArgIn, y: ArgIn) = Eq(x,y)
}

object MuxSInt extends DesignTest {
  def main(args: String*) = {
    assert(args.length == 1, "Usage MuxSInt <#args>")
    val N = args(0).toInt

    val blk = Block {
      val selsIn  = List.tabulate(N){i => ArgIn(Bool())(s"sel$i") }
      val argsIn  = List.tabulate(N+1){i => ArgIn(s"in$i") }
      val ops = List.tabulate(N){i => Mux(selsIn(i), argsIn(i),argsIn(i + 1)) }

      val argsOut = List.tabulate(N){i => ArgOut(s"out$i", ops(i)) }
    }
    Pipe(blk, None)
  }

  def test(args: String*) {
    val N = args(0).toInt

    val sel = List.tabulate(N){i => util.Random.nextBoolean() }
    val ins = List.tabulate(N+1){i => i}

    for (i <- 0 until N) { setArg(s"sel$i", sel(i)) }
    for (i <- 0 until N+1) { setArg(s"in$i", ins(i)) }

    run()

    for (i <- 0 until N) {
      vcompare(getArg[Int](s"out$i"), (if (sel(i)) i else i + 1))
    }
  }
}

object FltUIntDiv extends Design {
  def main(args: String*) = {
    assert(args.length == 1, "Usage FltUIntDiv <#args>")
    val N = args(0).toInt

    val blk = Block {
      val fIn = List.tabulate(N){i => ArgIn(Flt())(s"f$i") }
      val uIn = List.tabulate(N){i => ArgIn(UInt())(s"u$i") }
      val ops = List.tabulate(N){i => Div(fIn(i),uIn(i)) }
      val out = List.tabulate(N){i => ArgOut(s"out$i", ops(i)) }
    }

    Pipe(blk, None)
  }
}

object FltCastUInt extends Design {
  def main(args: String*) = {
    assert(args.length == 1, "Usage FltCastUInt <#args>")
    val N = args(0).toInt

    val blk = Block {
      val fIn = List.tabulate(N){i => ArgIn(Flt())(s"in$i") }
      val ops = List.tabulate(N){i => Float2Fix(fIn(i), UInt()) }
      val out = List.tabulate(N){i => ArgOut(s"out$i", ops(i)) }
    }
    Pipe(blk,None)
  }
}

object UIntCastFloat extends Design {
  def main(args: String*) = {
    assert(args.length == 1, "Usage UIntCastFloat <#args>")
    val N = args(0).toInt

    val blk = Block {
      val fIn = List.tabulate(N){i => ArgIn(UInt())(s"in$i") }
      val ops = List.tabulate(N){i => Fix2Float(fIn(i), Flt()) }
      val out = List.tabulate(N){i => ArgOut(s"out$i", ops(i)) }
    }
    Pipe(blk,None)
  }
}

trait UnaryCharacterization extends Design {
  val name: String
  def op(x: ArgIn): CombNode
  def main(args: String*) = {
    assert(args.length == 1, s"Usage $name <#args>")
    val N = args(0).toInt

    val blk = Block {
      val ins = List.tabulate(N){i => ArgIn(Flt())(s"in$i") }
      val ops = List.tabulate(N){i => op(ins(i)) }
      val out = List.tabulate(N){i => ArgOut(s"out$i", ops(i)) }
    }
    Pipe(blk,None)
  }
}

object SqrtFlt extends UnaryCharacterization {
  val name = "SqrtFlt"
  def op(x: ArgIn) = Sqrt(x) // Change to Sqrt
}
object ExpFlt extends UnaryCharacterization {
  val name = "SqrtFlt"
  def op(x: ArgIn) = Exp(x) // Change to Exp
}
object LogFlt extends UnaryCharacterization {
  val name = "LogFlt"
  def op(x: ArgIn) = Log(x) // Change to Log
}
object AbsFlt extends UnaryCharacterization {
  val name = "AbsFlt"
  def op(x: ArgIn) = Abs(x) // Change to Abs
}

object PQChar extends Design {
  def main(args: String*) = {
    assert(args.length == 1, "Usage PQChar <#args>")
    val N = args(0).toInt
    val arg = ArgIn("a")

    val queue = PQ(N, false)
    val inCtr = Ctr((Const(100),1))
    val outCtr = Ctr((Const(100),1))
    val addrIn = inCtr(0)
    val addrOut = outCtr(0)

    val inBlk = Block {
      val argComp = Bundle(addrIn, And(arg,addrIn))
      val stores  = St(queue, addrIn, argComp)
    }

    val outBlk = Block {
      val load  = Ld(queue, addrOut)
      val argOut = ArgOut(s"out0", load.get(0))
      val argOut2 = ArgOut(s"out1", load.get(1))
    }
    Sequential(Pipe(inCtr,inBlk,None), Pipe(outCtr,outBlk,None))
  }
}

object LdChar extends Design {
  def main(args: String*) = {
    val N = ArgIn("N")
    val P = args(0).toInt

    val vec = OffChipArray("vec", Const(9600))

    val ram = BRAM(96000,false)
    val tileLd = TileMemLd(vec, Const(9600), Const(0), Const(0), ram, 1, 96000)

    val ctr = Ctr(List(P))((N,1))
    var outs: List[ArgOut] = null
    val blk = Block {
      val load = Ld(ram, ctr(0))
      outs = List.tabulate(P){i => ArgOut(s"out$i", load(i)) }
    }
    val pipe = Pipe(ctr,blk,None)

    val seq = Sequential(tileLd,pipe)
    outs.foreach(_.setParent(seq))
    seq
  }
}

object StChar extends Design {
  def main(args: String*) = {
    val N = ArgIn("N")
    val P = args(0).toInt

    val vec = OffChipArray("vec", Const(9600))

    val ram = BRAM(96000,false)
    val tileLd = TileMemLd(vec, Const(9600), Const(0), Const(0), ram, 1, 96000)

    val ctr = Ctr(List(P))((N,1))
    val blk = Block {
      St(ram, ctr(0), ctr(0))
    }
    val pipe = Pipe(ctr,blk,None)

    var outs: List[ArgOut] = Nil
    val blk2 = Block{
      val load = Ld(ram, Const(32))
      outs = List.tabulate(P){i => ArgOut(s"out$i",load(i))}
    }
    val pipe2 = Pipe(blk2,None)

    val seq = Sequential(tileLd,pipe,pipe2)
    outs.foreach(_.setParent(seq))
    seq
  }
}