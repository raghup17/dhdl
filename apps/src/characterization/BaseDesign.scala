import dhdl.graph._
import dhdl.codegen._
import dhdl.Design

object BaseDesign extends Design {

  def main(args: String*) = {
    val a = ArgIn("a")

    val inv = Not(a)

    val top = Pipe(inv)
    ArgOut("not", inv)
    top
  }
}


