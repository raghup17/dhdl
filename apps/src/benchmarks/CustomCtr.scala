import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}
import java.io._
import scala.io.Source

object CustomCtr extends CustomCtrDesign
trait CustomCtrDesign extends DesignTest {

  /*
  This app tests the custom parallelizable counter SM.

  It creates a 2-level counter, each of which can be parallelized,
  and writes level 1 to offchip1 (overwriting at each count on level 2)
  and writes level 2 to offchip2
  */

  val T = FixPt(32, 0, false)
  var innerMax = 96
  var outerMax = 96
  var innerPar = 1
  var outerPar = 1
  var innerStride = 1
  var outerStride = 1


  def main(args: String*) = {
    if (!(args.size == 6)) {
      println(args.size)
      println("\nUsage: CustomCtr <innerMax> <outerMax> <innerPar> <outerPar> <innerStride> <outerStride>")
      sys.exit(0)
    }
    innerMax = args(0).toInt
    outerMax = args(1).toInt
    innerPar = args(2).toInt
    outerPar = args(3).toInt
    innerStride = args(4).toInt
    outerStride = args(5).toInt

    // Declare off-chip data arrays
    val result1 = OffChipArray(T)("result1", Const(innerMax))
    val result2 = OffChipArray(T)("result2", Const(outerMax))

    val counter1 = Ctr(List(outerPar,innerPar))((Const(outerMax), outerStride), (Const(innerMax), innerStride))

    val bram1 = BRAM(T, "bram1", innerMax, false)
    val bram2 = BRAM(T, "bram2", outerMax, false)

    val st1 = St(bram1, counter1(1), counter1(1)) // 1st level
    val st2 = St(bram2, counter1(0), counter1(0)) // 2nd level

    val p1 = Pipe(counter1, GraphNode(st1, st2), None)

    val tileSt1 = TileMemSt(innerPar, result1, Const(innerMax), Const(0), Const(0), bram1, 1, innerMax)
    tileSt1.withForce(Const(true))
    val tileSt2 = TileMemSt(outerPar, result2, Const(outerMax), Const(0), Const(0), bram2, 1, outerMax)
    tileSt2.withForce(Const(true))

    val body = List(p1) ++ List(tileSt1) ++ List(tileSt2) 
    val all = Sequential(body:_*)
      
    all
  }


  def test(args: String*) {

  }

}


object CustomCtrDSE extends DSE {
  val name = "CustomCtr"
  val makeDesign = () => new CustomCtrDesign{}

  val modelDim = Discrete("modelDim", 96 to 1152 by 96)
  val dotMapPar = Discrete("minibatchDim", 96 to 1152 by 96)
  val dotReducePar = Discrete("numBatches", 1 to 6)
  val Metapipe = Category("Metapipe", true, false)

  (dotReducePar <= modelDim) and (dotReducePar divides modelDim)
}
