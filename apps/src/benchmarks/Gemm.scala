import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design,DesignTest,DSE}

object Gemm extends GemmDesign
object GemmTest extends GemmDesign with DesignTest {
  def test(args: String*) {
    val M = 5*bm; val N = 5*bn; val P = 5*bp
    setArg("M", M)
    setArg("N", N)
    setArg("P", P)

    val m1 = Array.tabulate(M){i => Array.tabulate(N){j => i + j}}
    val m2 = Array.tabulate(N){i => Array.tabulate(P){j => i + j + 5}}

    setMem("M1", m1.flatten)
    setMem("M2", m2.flatten)

    run()

    val m2t = Array.tabulate(P){j => Array.tabulate(N){i => i + j + 5}}
    val m3Gold = Array.tabulate(M){i =>
      Array.tabulate(P){j =>
        m1(i).zip(m2t(j)).map{case (a,b) => a*b}.reduce(_+_)
      }
    }.flatten

    val m3 = getMem[Int]("M3", 0 until M*P)
    compare(m3, m3Gold)
  }
}

trait GemmDesign extends Design {
  var shouldMetaPipeline = true
  var bm = 96
  var bn = 96
  var bp = 96
  var opar = 1
  var mpar = 1
  var ipar = 1
  val T = Flt()

  def matmultTile(tileA: MemNode, tileB: MemNode, tileC: MemNode) = {

    // Matrix multiply
    val ijkCtr = Ctr(List(1,1,ipar))((Const(bm),1), (Const(bn),1), (Const(bp),1))
    val i = ijkCtr(0)
    val j = ijkCtr(1)
    val k = ijkCtr(2)

    val accum = Reg(T)
    val mula = Mul(i, Const(bp))
    val addra = Add(mula, k)
    val mulb = Mul(k, Const(bn))
    val addrb = Add(mulb, j)
    val e1 = Ld(tileA, addra)
    val e2 = Ld(tileB, addrb, bn)
    val prod = Mul(e1, e2)
    val sum = Add(accum, prod)
    accum.write(sum)
    val mulc = Mul(i, Const(bn))
    val addrc = Add(mulc, j)
    val st = St(tileC, addrc, accum)
    val stage1 = Stage(GraphNode(mula, addra, mulb, addrb, e1, e2, prod), ReduceTree(prod, accum, sum))
    val stage2 = Stage(GraphNode(mulc, addrc, st), None)
    val multPipe = Pipe(ijkCtr, List(stage1, stage2))
    accum.setParent(multPipe)
    // Reset accum when k == bp-1
    accum.reset = Block {
      Eq(k(ipar-1), Const(bp-1))
    }
    multPipe
  }

//  def tileAdderPipe(sumTile: MemNode, accumTile: MemNode, multTile: MemNode, sourceSel: CombNode, bm: Int, bn: Int) = {
//    val bound = Mul(Const(bm), Const(bn))
//    val ctr = Ctr(List(4))((bound,1))
//    val i = ctr(0)
//    val accumTileVal = Ld(accumTile, i)
//    val sumTileVal = Ld(sumTile, i)
//    val multTileVal = Ld(multTile, i)
//    val sum = Add(Mux(sourceSel, accumTileVal, sumTileVal), multTileVal)
//    val st = St(sumTile, i, sum)
//    val tileAdder = Pipe(ctr, GraphNode(accumTileVal, sumTileVal, multTileVal, sum, st))
//    tileAdder
//  }

  def matmult() = {
    // Get sizes of matrices from host
    val M = ArgIn("M"); bound(M, 1920) //3072
    val N = ArgIn("N"); bound(N, 1920)
    val P = ArgIn("P"); bound(P, 1920)

    // Declare off-chip matrix arrays
    val M1 = OffChipArray(T)("M1", M, P)
    val M2 = OffChipArray(T)("M2", P, N)
    val M3 = OffChipArray(T)("M3", M, N)

    val seqCtr = Ctr(List(1,1,opar))((M,bm), (N,bn), (P,bp))
    val i = seqCtr(0)
    val j = seqCtr(1)
    val k = seqCtr(2)

    val s1 = replicate(opar) { id =>
      val m1Tile = BRAM(T, bm*bp, shouldMetaPipeline)
      val m2Tile = BRAM(T, bp*bn, shouldMetaPipeline)
      val m1TileLd = TileMemLd(ipar)(M1, P, i, k(id), m1Tile, bm, bp)
      val m2TileLd = TileMemLd(ipar)(M2, N, k(id), j, m2Tile, bp, bn)
      m1TileLd.force = Const(true)
      m2TileLd.force = Const(true)
      (Parallel(m1TileLd, m2TileLd), List(m1Tile, m2Tile))
    }

    // multTile = m1Tile x m2Tile
    val s2Mults = replicate(opar) { i =>
      val multTile = BRAM(T, bm*bn, shouldMetaPipeline)
      val m1Tile = s1._2(i)(0)
      val m2Tile = s1._2(i)(1)
      val tileMultiplier = matmultTile(tileA = m1Tile, tileB = m2Tile, tileC = multTile)
      (tileMultiplier, List(multTile))
    }

    // Load M3(i,j)
//    val accumTile = BRAM(T, bm*bn, shouldMetaPipeline)
//    val tileAccumLd = TileMemLd(M3, N, i, j, accumTile, bm, bn)
//    val s2 = Parallel(s2Mults._1, tileAccumLd)

    val s2 = s2Mults._1

    // sumTile = multTile + (tileAccumLd.isLoading ? accumTile : sumTile)
    val sumTile = BRAM(T, bm*bn, shouldMetaPipeline)
//    val dblreg = Reg(Bool(), true)
//    dblreg.write(tileAccumLd.isLoading)
//    dblreg.setWriter(tileAccumLd)

    val multRAMList = s2Mults._2.flatten
    val s3Ctr = Ctr(List(1))((Const(bm*bn),1))
    var s3_mapOut: CombNode = null
    val s3Map = Block {
      val mapNodes = (0 until opar).map{i => Ld(multRAMList(i), s3Ctr(0))}
      s3_mapOut = treeReduce(mapNodes.toList, (a,b) => Add(a,b))
    }
    val s3_oldval = Ld(sumTile, s3Ctr(0))
//    val s3_accumval = Ld(accumTile, s3Ctr(0))
    val s3_accumval = Const(0.0f)
    val muxCond = Eq(k(0), Const(0))
    val s3_otherval = Mux(muxCond, s3_accumval, s3_oldval)
//    val s3_otherval = Mux(dblreg, s3_accumval, s3_oldval)
    val s3_newval = Add(s3_mapOut, s3_otherval)
    val writerNode = St(sumTile, s3Ctr(0), s3_newval)
    s3Map.add(s3_oldval, s3_accumval, s3_otherval, s3_newval, writerNode)
    val s3 = (Pipe(s3Ctr, s3Map), sumTile)

//    dblreg.addReader(s3._1)
//    dblreg.setParent(s3._1)

//    val s3 = reduceAll(s2Mults._2.flatten, sumTile) { (a,b,last) =>
//      val res = if (last) sumTile else BRAM(T, bm*bn, shouldMetaPipeline)
//      val bound = Const(bm*bn)
//      val ctr = Ctr(List(1))((bound,1))
//      val i = ctr(0)
//      val tileAdder = if (last) {
//        val in1 = Ld(a, i)
//        val in2_0 = Ld(b, i)
//        val in2_1 = Ld(accumTile, i)
//        val in2 = Mux(dblreg, in2_1, in2_0)
//        val sum = Add(in1, in2)
//        val st = St(res, i, sum)
//        val pipe = Pipe(ctr, GraphNode(in1, in2_0, in2_1, in2, sum, st))
//        dblreg.addReader(pipe)
//        dblreg.setParent(pipe)
//        pipe
//      } else {
//        val in1 = Ld(a, i)
//        val in2 = Ld(b, i)
//        val sum = Add(in1, in2)
//        val st = St(res, i, sum)
//        Pipe(ctr, GraphNode(in1, in2, sum, st))
//      }
//      (tileAdder, res)
//    }

    // Store sumTile to M3(i,j)
    val tileSt = TileMemSt(M3, N, i, j, sumTile, bm, bn)
    tileSt.force = Const(true)
    val body = List(s1._1) ++ List(s2) ++ List(s3._1) ++ List(tileSt)

    if (shouldMetaPipeline) MetaPipeline(seqCtr, body:_*)
    else Sequential(seqCtr, body:_*)
  }


  def main(args: String*) = {
    if (args.size != 7) {
      println("Usage: Gemm <bm> <bn> <bp> <opar> <mpar> <ipar> <mp>")
      sys.exit(0)
    }
    bm = args(0).toInt
    bn = args(1).toInt
    bp = args(2).toInt
    opar = args(3).toInt
    mpar = args(4).toInt
    ipar = args(5).toInt
    shouldMetaPipeline = args(6) == "true"
    matmult()
  }
}

object GemmDSE extends DSE {
  val name = "Gemm"
  val makeDesign = () => new GemmDesign{}
  //override val verbose = true
  val BM = Discrete("BM", (10 to 100 by 10).toList :+ 1)    // 10
  val BN = Discrete("BN", 96 to 960 by 96)                // 10
  val BP = Discrete("BP", 96 to 960 by 96)                // 10
  val OuterPar = Discrete("OuterPar", 1 to 6)             // 6
  val MiddlePar = Discrete("MiddlePar", 1 to 3)           // 160
  val InnerPar = Discrete("InnerPar", 1 to 96)    // 160
  val Metapipe = Category("Metapipe", true, false)        // 2

  (MiddlePar <= BN) and (MiddlePar divides BN)
  (InnerPar <= BP) and (InnerPar divides BP)

  override def isLegal(args: Point): Boolean = {
    // HACK: Floating point only
    super.isLegal(args) && ( (args(3).toInt * args(4).toInt * args(5).toInt) < 350)
  }

  /*space += "BM" -> Discrete(96 to 960 by 96)
  space += "BN" -> Discrete(96 to 960 by 96)
  space += "BP" -> Discrete(96 to 960 by 96)
  space += "OuterPar" -> Discrete(1 to 10)
  space += "MiddlePar" -> Discrete(1 until 1000)
  space += "InnerPar" -> Discrete(1 until 1000)
  space += "Metapipe" -> Category(true, false)

  restrict += LessThan("InnerPar", "BN")

  def isLegal(args: Point) = {
    val bn   = args(1).toInt
    val mpar = args(4).toInt
    (bn % mpar == 0) && (mpar < bn)

    val bp = args(2).toInt
    val ipar = args(5).toInt
    (bp % ipar == 0) && (ipar < bp)
  }*/
}
