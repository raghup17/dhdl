import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}

object LogReg extends LogRegDesign
trait LogRegDesign extends DesignTest {

  val T = Flt()
  var tileSize = 96
  var innerPar = 1
  var innerMpPar = 1
  var outerMpPar = 1
  var shouldMetapipeline = false
  var D = 16

  def main(args: String*) = {
    if (args.size != 6) {
      println("\nUsage: LogReg <#tileN> <#D> <#ipar> <#innerMpPar> <#outerMpPar> <metapipeline?>")
      sys.exit(0)
    }

    tileSize = args(0).toInt
    D = args(1).toInt
    innerPar = args(2).toInt
    innerMpPar = args(3).toInt
    outerMpPar = args(4).toInt
    shouldMetapipeline = args(5) == "true"

    // Off-chip memory input
    val N = ArgIn("N"); bound(N, 9993600)

    val x = OffChipArray(T)("x", N, Const(D))
    val y = OffChipArray(T)("y", N)
    val theta = OffChipArray(T)("theta", Const(D))


    val rCtr = Ctr(List(outerMpPar))((N, tileSize))
    val r = rCtr(0)

    // Gradient Metapipe: Process all rows {
      // 1. Load tiles from off-chip memory {
      val (offChipLoadPar, bramList) = replicate(outerMpPar) { oidx =>
        val bx = BRAM(T, tileSize*D, shouldMetapipeline)
        val by = BRAM(T, tileSize, shouldMetapipeline)
        val bxLd = TileMemLd(x, Const(D), r, Const(0), bx, tileSize, D)
        val byLd = TileMemLd(y, Const(D), Const(0), r, by, 1, tileSize)
        val offChipLoad = Parallel(bxLd, byLd)
        (offChipLoad, List(bx, by))
      }
      // Load theta in parallel. This should not scale with outerMpPar,
      // and should be done only once
      val btheta = BRAM(T, D, shouldMetapipeline)
      val thetaLd = TileMemLd(theta, Const(D), Const(0), Const(0), btheta, 1, D)
      val ldStage = Parallel(offChipLoadPar, thetaLd)
      // End off-chip load }
      val bx = bramList.map { _(0)}
      val by = bramList.map { _(1)}

      // 2. Row processing MetaPipe: Process one row at a time {
      val (rowPipePar, gradientList) = replicate(outerMpPar) { oidx =>
        val rrCtr = Ctr(List(innerMpPar))((Const(tileSize), 1))
        val rr = rrCtr(0)

          // Pipe 1: Dot product pipe: Row rr, theta
          val (dotPipePar, dotAccum) = replicates(innerMpPar) { idx =>
            val jCtr = Ctr(List(innerPar))((Const(D), 1))
            val j = jCtr(0)
            val rowaddr = Mul(rr(idx), Const(D))
            val addr = Add(rowaddr, j)
            val rval = Ld(bx(oidx), addr)
            val thetaVal = Ld(btheta, j)
            val mul = Mul(rval, thetaVal)
            val dotAccum = Reg(T, shouldMetapipeline)
            val dotSum = Add(mul, dotAccum)
            dotAccum.write(dotSum)
            val dotPipe = Pipe(jCtr,
              GraphNode(rowaddr, addr, rval, thetaVal, mul),
              ReduceTree(mul, dotAccum, dotSum)
            )
            dotAccum.setParent(dotPipe)
            (dotPipe, List(dotAccum))
          }


          // Pipe 2: Unit pipe: pipe2Res = y(rr) - sigmoid(dotAccum)
          val (pipe2Par, pipe2Res) = replicates(innerMpPar) { idx =>
            val yLd = Ld(by(oidx), rr(idx))
            val negativeT = Sub(Const(0.0f), dotAccum(idx)(0))
            val exp = Exp(negativeT)
            val denom = Add(Const(1.0f), exp)
            val sigmoid = Div(Const(1.0f), denom)
            val diff = Sub(yLd, sigmoid)
            val pipe2Res = Reg(T, shouldMetapipeline)
            pipe2Res.write(diff)
            val pipe2 = Pipe(yLd, negativeT, exp, denom, sigmoid, diff, pipe2Res)
            pipe2Res.setParent(pipe2)
            (pipe2, List(pipe2Res))
          }


          // Pipe 3: row rr * (y(rr) - sigmoid)
          val (pipe3Par, subRAM) = replicate(innerMpPar) { idx =>
            val subRAM = BRAM(T, D, shouldMetapipeline)
            val kCtr = Ctr(List(innerPar))((Const(D), 1))
            val k = kCtr(0)
            val rowaddr2 = Mul(rr(idx), Const(D))
            val addr2 = Add(rowaddr2, k)
            val rval2 = Ld(bx(oidx), addr2)
            val subval = Mul(rval2, pipe2Res(idx)(0))
            val stval = St(subRAM, k, subval)
            val pipe3 = Pipe(kCtr, GraphNode(rowaddr2, addr2, rval2, subval, stval))
            (pipe3, List(subRAM))
          }


          // Pipe 4: Accumulator reduction
          val k2Ctr = Ctr(List(innerPar))((Const(D), 1))
          val gradient = BRAM(T, D, shouldMetapipeline)
          gradient.setAccum()
          val pipe4Par = reducePipeAccum(innerMpPar, k2Ctr, gradient) { idx =>
            val k2 = k2Ctr(0)
            Ld(subRAM(idx)(0), k2)
          } { (a,b) => Add(a,b) }

        // Construct pipeline to process one row
        val rowPipe = if (shouldMetapipeline) MetaPipeline(rrCtr, dotPipePar, pipe2Par, pipe3Par, pipe4Par) else Sequential(rrCtr, dotPipePar, pipe2Par, pipe3Par, pipe4Par)
        (rowPipe, List(gradient))
      }
      // End }

      // Reduce all the gradients into one
      val gradientRedCtr = Ctr(List(innerPar))((Const(D), 1))
      val gradient = BRAM(T, D, shouldMetapipeline)
      gradient.setAccum()
      val gradientRedPipe = reducePipeAccum(outerMpPar, gradientRedCtr, gradient) { idx =>
        val c = gradientRedCtr(0)
        Ld(gradientList(idx)(0), c)
      } { (a,b) => Add(a,b) }

      val gradientPipe = if (shouldMetapipeline) MetaPipeline(rCtr, ldStage, rowPipePar, gradientRedPipe) else Sequential(rCtr, ldStage, rowPipePar, gradientRedPipe)
    // End gradient }

    // Compute theta = theta + alpha * gradient {
    val alpha = ArgIn(T)("alpha")
    val pCtr = Ctr((Const(D), 1))
    val p = pCtr(0)
    val thetaVal2 = Ld(theta, p)
    val gradientVal = Ld(gradient, p)
    val times = Mul(gradientVal, alpha)
    val add = Add(thetaVal2, times)
    val stTheta = St(theta, p, add)
    val updatePipe = Pipe(pCtr, GraphNode(thetaVal2, gradientVal, times, add, stTheta))
    // End theta compute }

    // Store theta back
    val thetaSt = TileMemSt(theta, Const(D), Const(0), Const(0), btheta, 1, D)

    val top = Sequential(gradientPipe, updatePipe, thetaSt)
    top
  }

  def test(args: String*) {
//    val tileSize = args(0).toInt
//    val C = 960
//
//    setArg("C",C)
//    val vec1 = Array.tabulate(C){i => util.Random.nextDouble * 100}
//    val vec2 = Array.tabulate(C){i => util.Random.nextDouble * 100}
//
//    setMem("vec1", vec1)
//    setMem("vec2", vec2)
//
//    run()
//
//    val outGold = vec1.zip(vec2).map{case (a,b) => a*b}.sum
//    val out = getArg[Double]("out")
//
//    compare(out, outGold)
  }
}


//object LogRegDSE extends DSE {
//  val name = "LogReg"
//  val makeDesign = () => new LogRegDesign{}
//
//  val TileSize = Discrete("TileSize", 96 to 19200 by 96)
//  val OuterPar = Discrete("OuterPar", 1 to 6)
//  val InnerPar = Discrete("InnerPar", 1 to 19200)
//  val Metapipe = Category("Metapipe", true, false)
//
//  (InnerPar <= TileSize) and (InnerPar divides TileSize)
//}
