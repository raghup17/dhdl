import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}
import java.io._
import scala.io.Source

object SparseSGD extends SparseSGDDesign
trait SparseSGDDesign extends DesignTest {

  val T = Flt()
  val D4bit = FixPt(2,2,false)
  val M8bit = FixPt(4,4,false) // Fix2Float in Node.scala
  var modelDim = 128
  var minibatchDim = 8
  var numBatches = 1
  var sparsity = 120
  var alpha = 0.001f
  var epochs = 200
  var bPar = 1
  var dotPar = 1
  var shouldMetapipeline = true


  def main(args: String*) = {
    if (!(args.size == 9 | args.size == 10)) {
      println(args.size)
      println("\nUsage: SparseSGD <#modelDim> <#minibatchDim> <#numBatches> <sparsity(0's per dim)> <#alpha> <#epochs> <#batchPar> <#dotProductPar> <metapipeline?> <recycle>")
      sys.exit(0)
    }
    modelDim = args(0).toInt
    minibatchDim = args(1).toInt
    numBatches = args(2).toInt
    sparsity = args(3).toInt
    alpha = args(4).toFloat
    epochs = args(5).toInt
    bPar = args(6).toInt
    dotPar = args(7).toInt
    shouldMetapipeline = args(8) == "true"

    // Get sizes of data structures from host
    val M = ArgIn("M"); bound(M, modelDim)
    val B = ArgIn("B"); bound(B, minibatchDim*numBatches)

    // Declare off-chip data arrays
    val model = OffChipArray(T)("model", M)
    val train_x = OffChipArray(T)("train_x", B, Const((modelDim-sparsity)*2))
    val train_x_T = OffChipArray(T)("train_x_T", B, Const((modelDim-sparsity)*2))
    val train_y = OffChipArray(T)("train_y", B)
    val result = OffChipArray(T)("result", M)

    val epochCtr = Ctr(List(1))((Const(epochs), 1))

    val loadCtr = Ctr(List(1,bPar))((M,modelDim), (B,minibatchDim))
    val id_model = loadCtr(0)
    val id_batch = loadCtr(1)

    // 1.1. Load tiles from off-chip memory
    val comboBlock = replicate(bPar) { i =>
      // 0.1. Create bool reg for muxing off-chip clean model with on-chip updated model 
      var firstEpoch1 = And(
          Eq(epochCtr(0)(0), Const(0)), // First iteration
          Lt(id_batch(i), Const(bPar * minibatchDim)) // batch counter is on first value
        )
      var firstEpoch2 = And(
          Eq(epochCtr(0)(0), Const(0)), // First iteration
          Lt(id_batch(i), Const(bPar * minibatchDim)) // batch counter is on first value
        )

      // 0.2. Declare BRAMs
      val modelTile = BRAM(T, "modelTile_handle" + i, modelDim, shouldMetapipeline)
      val updatedModel = BRAM(T, "updatedModel_handle" + i, modelDim, shouldMetapipeline)
      val xTile = BRAM(T, "xTile_handle" + i, (modelDim-sparsity) * minibatchDim * 2, shouldMetapipeline)
      val yTile = BRAM(T, "yTile_handle" + i, minibatchDim, shouldMetapipeline)

      // 1.0 Load off-chip data
      // TODO: turn off modelTileLd if firstEpoch is not true
      val modelTileLd = TileMemLd(model, M, Const(0), Const(0), modelTile, 1, modelDim) // Assume we load entire model
      val xTileLd = TileMemLd(train_x, Const((modelDim-sparsity)*2), id_batch(i), id_model(0), xTile, minibatchDim, (modelDim-sparsity)*2)
      val yTileLd = TileMemLd(train_y, B, Const(0), id_batch(i), yTile, 1, minibatchDim)
      modelTileLd.withForce(Const(true))
      xTileLd.withForce(Const(true))
      yTileLd.withForce(Const(true))
      val offChipLoad = Parallel(modelTileLd,xTileLd,yTileLd)

      // 2. Perform dot product on each row in X and store into a column of BRAM of size minibatchDim
      val dotTile = BRAM(T, "dotTile_handle" + i, minibatchDim, shouldMetapipeline)
      val tileMultiplier = dotmultTile(xTile,modelTile,updatedModel,dotTile,firstEpoch1)

      // 3. Subtract dot product from Y
      val subtractTile = BRAM(T, "subtractTile_handle" + i, minibatchDim, shouldMetapipeline)
      val tileSubtractor = subtractionTile(yTile,dotTile,subtractTile)

      // 4. Update model 
      val tileUpdator = updateTile(xTile,subtractTile,modelTile,updatedModel,firstEpoch2)

      val body = List(offChipLoad) ++ List(tileMultiplier) ++ List(tileSubtractor) ++ List(tileUpdator)
      val returnCtrl = if (shouldMetapipeline) MetaPipeline(loadCtr, body:_*) else Sequential(loadCtr, body:_*)

      (returnCtrl, List(updatedModel))
    }

    val epochCtrl = Sequential(epochCtr, comboBlock._1)

    // 5. Write to off chip after all epochs
    val tileSt = TileMemSt(result, M, Const(0), Const(0), comboBlock._2(0)(0), 1, modelDim)
    tileSt.withForce(Const(true))
    val topCtr = Ctr((Const(1),1))
    val last = List(epochCtrl) ++ List(tileSt)
    val topCtrl = if (shouldMetapipeline) MetaPipeline(topCtr, last:_*) else Sequential(topCtr,last:_*)

    topCtrl
  }

  // Dot product between each row in minibatch and model
  def dotmultTile(matrixTile: MemNode, vectorTile: MemNode, otherVectorTile: MemNode, resultsTile: MemNode, firstEpoch: CombNode) = {

    val rowCtr = Ctr(List(1))((Const(minibatchDim),1))
    val row = rowCtr(0)

    val accum = Reg(T)
    val colCtr = Ctr(List(dotPar))((Const(modelDim - sparsity),1))
    val s2 = reducePipe(1, colCtr, accum) {i =>
      // Address where model index is stored is row * (modelDim-sparsity) + 2 * col
      val addrm_fl = Ld(matrixTile, Add(Mul(row, Mul(Const(2), Const(modelDim - sparsity))), Mul(Const(2),colCtr(0))))
      val addrm = Float2Fix(addrm_fl)
      val addrx = Add(Mul(row, Mul(Const(2), Const(modelDim-sparsity))), Add(Mul(Const(2), colCtr(0)), Const(1)))
      val m1 = Ld(vectorTile, addrm)
      val m2 = Ld(otherVectorTile, addrm)
      val v1 = Mux(firstEpoch, m1, m2)
      val v2 = Ld(matrixTile, addrx)
      Mul(v1,v2)
    }{(a,b) => Add(a,b) }
    accum.setParent(s2)

    val s3 = replicate(1) {pid =>
      val st = St(resultsTile, rowCtr(0), accum, pid, 1)
      val storePipe = Pipe(st)
      (storePipe, List())
    }

    val dotmult = Sequential(rowCtr, s2, s3._1)
    dotmult
  }

  // Subtract y computed from model from y given in training set
  def subtractionTile(TileA: MemNode, TileB: MemNode, TileC: MemNode) = {
    val rowCtr = Ctr(List(1))((Const(minibatchDim),1))
    val row = rowCtr(0)

    val s1 = replicates(1) {pid => 
      val e1 = Ld(TileA, row)
      val e2 = Ld(TileB, row)
      val sub = Sub(e1, e2)
      val st = St(TileC, row, sub, pid, 1)
      val subPipe = Pipe(rowCtr, GraphNode(e1, e2, sub, st), None) 
      (subPipe, List())
    }


    val subtract = s1._1
    subtract
  }

  // Compute a new model by adding gradient*step to old model (using linreg gradient)
  def updateTile(inputTile: MemNode, errorVector: MemNode, modelVector: MemNode, newModelVector: MemNode, firstEpoch: CombNode) = {
    val elementCtr = Ctr(List(dotPar))((Const(modelDim-sparsity),1))
    val element = elementCtr(0)

    val pointCtr = Ctr((Const(minibatchDim),1))
    val point = pointCtr(0)
    // // Address generation
    val addrm_fl = Ld(inputTile, Add(Mul(point, Mul(Const(2), Const(modelDim - sparsity))), Mul(Const(2),element)))
    val addrm = Float2Fix(addrm_fl)
    val addrx = Add(Mul(point, Mul(Const(2), Const(modelDim-sparsity))), Add(Mul(Const(2), element), Const(1)))
    // Update delta calculation
    val e1 = Ld(inputTile, addrx)
    val e2 = Ld(errorVector, point)
    val del = Div(Mul(Mul(e1, e2), Const(alpha)), Fix2Float(Const(minibatchDim)))
    // // Load correct model
    val m1 = Ld(modelVector, addrm)
    val m2 = Ld(newModelVector, addrm)
    val firstUpdate = And(firstEpoch, Eq(point(0), Const(0)))
    val e3 = Mux(firstUpdate, m1, m2)
    // Apply update
    val sum = Add(e3, del)
    val st = St(newModelVector, addrm, sum, 0, 1)

    val s1Pipe = Pipe(pointCtr, GraphNode(addrm_fl, addrm, addrx, e1, e2, del, m1, m2, firstUpdate, e3, sum, st), None)


    val totalPipe = Sequential(elementCtr,s1Pipe)

    totalPipe
  }

  // This block will resolve model write-back contention once mux'ed BRAM loading is supported
  def writeBack(suggestedModel: MemNode, masterModel: MemNode) {
    // val ctr = Ctr(List(1))((Const(modelDim),1))
    // val data = Ld(suggestedModel, ctr(0)(0))
    // val st = St(masterModel, ctr(0), data, 0, 1)
    // Pipe(ctr, GraphNode(data), st)

  }

  def test(args: String*) {
    // Read input args
    val modelDim = args(0).toInt
    val minibatchDim = args(1).toInt
    val numBatches = args(2).toInt
    val sparsity = args(3).toInt
    val alpha = args(4).toFloat
    val epochs = args(5).toInt
    val recycle = args(9) == "true"
    val maxDPcheck = 9

    // Compute number of points, assuming CPU randomly selected batches and put them all in the offchip array for us
    val numPoints = minibatchDim*numBatches

    // Use input args to make random off chip array values 
    setArg("M",modelDim)
    setArg("B",numPoints)

    val realModel = if(recycle){ReadData("realModel")} else {Array.tabulate(modelDim){i => (math rint util.Random.nextDouble * 1000) / 1000 }}
    val modelNoise = Array.tabulate(modelDim){i => (math rint util.Random.nextGaussian * 100) / 1000 }
    val model = if(recycle){ReadData("M")} else {(realModel zip modelNoise) map {case (e1: Double, e2: Double) => e1 + e2}}
    // Create dense matrix
    val train_x_matrix = if(recycle){ReadMatrix("train_x_matrix", modelDim, numPoints)} else {Array.tabulate(numPoints){i => Array.tabulate(modelDim){j => (math rint util.Random.nextDouble * 1000) / 1000 }}}
    var train_x_matrix_LIL: Array[Array[Double]] = Array.tabulate(numPoints){i => Array.tabulate(2*(modelDim - sparsity)){j => -1}}
    // Randomly sparsify it and build LIL sparse matrix
    if (!(recycle)) {
      for (row <- 0 to numPoints-1) {
        var keepers = Array.tabulate(modelDim){i => i}
        var droppers = Array.tabulate(sparsity){i => -1}
        var count = 0
        while (keepers.length > modelDim - sparsity) {
          var dropId = util.Random.nextInt(keepers.length)
          droppers(count) = keepers(dropId)
          var ab = keepers.toBuffer
          ab.remove(dropId)
          keepers = ab.toArray
          count = count+1
        }
        // Remove droppers from test matrix
        for (el <- droppers) {
          train_x_matrix(row)(el) = 0
        }
        // Add keepers to dhdl matrix
        var id = 0
        for (el <- keepers) {
          train_x_matrix_LIL(row)(2*id) = el
          train_x_matrix_LIL(row)(2*id + 1) = train_x_matrix(row)(el)
          id = id + 1
        }
      }
    }
    val train_x = if (recycle){ReadData("X")} else {train_x_matrix_LIL.flatten}
    val yNoise = Array.tabulate(numPoints){i => (math rint util.Random.nextGaussian * 100) / 1000 }
    var train_y = if(recycle){ReadData("Y")} else {train_x_matrix map {case v1: Array[Double] => 
      (v1 zip realModel).map({case (e1: Double, e2: Double) => 
        e1 * e2}).foldLeft(0.0)((m: Double, n: Double) => m + n)}}
    if (!(recycle)){train_y = (train_y zip yNoise) map {case (e1: Double, e2: Double) => e1 + e2}}

    // Set offchip data tiles
    setMem("model", model)
    setMem("train_x", train_x)
    setMem("train_y", train_y)

    // Show user what off chip array values are
    println("\nBatch:")
    if (numPoints * modelDim > 100) {
      println("<supressed>")
    }
    else {
      for (row <- 0 to numPoints-1) {
        for (col <- 0 to modelDim) {
          if (col == modelDim) {
            print("| " + train_y(row) + "\n")
          }        
          else {
            print(train_x_matrix(row)(col) + " ")
          }
        }
      }
    }
    println("\nExact Model: ")
    if (modelDim > 100) {
      println("<supressed>")
    }
    else {
      for (x <- realModel) {
        print(x + " ")
      }
    }

    if (modelDim > 100) {
      println("<supressed>")
    }
    else {
      println("\nInitial Model:")
      for (x <- model) {
        print(x + " ")
      }
    }

    // Run data through the architecture
    run()

    // Compute Gold
    val outGold = CorrectSparseSGD(model, train_x_matrix, train_y, minibatchDim, numBatches, modelDim, alpha, epochs)
    println("\nCorrect Output: ")
    outGold map {case e1: Double => print(e1 + " ")}
    // Extract DHDL results
    val out = getMem[Double]("result", 0 until modelDim)
    println("\nDHDL Output: ")
    out map {case e1: Double => print(e1 + " ")}
    println

    // Find abs(difference)
    val diff = (outGold zip out) map {case (e1: Double, e2: Double) => e1 - e2}

    // Compare
    println("Comparing outGold - outDHDL")
    // For each element in updated model
    for (i <- 0 to diff.length-1) {
      // Keep going until we find failure point
      var th = 0
      while (th < maxDPcheck+1) {
        if (diff(i) > 1/Math.pow(10,th) | diff(i) < -1/Math.pow(10,th)) {
          println(i + ": Diverge @ " + th + "dp: " + diff(i))
          th = 9999 // Break while loop
        }
        else if (th == maxDPcheck) {
          println(i + ": OK! (" + maxDPcheck + " DP)")
          th = 9999 // Break while loop
        }
        else {
          th = th + 1
        }
      }
    }
    WriteData(realModel, "realModel")
    WriteMatrix(train_x_matrix, "train_x_matrix")
    WriteData(train_x, "X")
    WriteData(train_y, "Y")
    WriteData(model, "M")
    WriteData(outGold, "O")
  }


  def WriteData(data:Array[Double], tag:String) = {
    println("Writing test data...")
    val dir = System.getProperty("user.dir") + "/"
    val f = new File(dir + "data" + tag )
    if (f.exists) {f.delete()}
    val pw = new PrintWriter(f)
    for (k <- 0 to data.length-1) {
      if (k == data.length-1) {
        pw.write(data(k).toString)
      }
      else {
        pw.write(data(k) + "\n")
      }
    }
    pw.close
  }

  // Writes matrix as flat array, which makes sparse case easier
  def WriteMatrix(data:Array[Array[Double]], tag:String) = {
    println("Writing test matrix...")
    val dir = System.getProperty("user.dir") + "/"
    val f = new File(dir + "data" + tag )
    if (f.exists) {f.delete()}
    val pw = new PrintWriter(f)
    for (k <- 0 to data.length-1) {
      for (l <- 0 to data(0).length-1){
        if (k == data.length-1 & l == data(0).length-1) {
          pw.write(data(k)(l).toString)
        }
        else {
          pw.write(data(k)(l) + "\n")
        }
      }
    }
    pw.close
  }

  def ReadData(tag:String) = {
    println("Reading test data...")
    val dir = System.getProperty("user.dir") + "/"
    val filename = dir + "data" + tag
    var data = Array.tabulate(0) {i => 0.0}
    for(line <- Source.fromFile(filename).getLines()){
      data = data :+ line.toDouble
    }
    data
  }

  def ReadMatrix(tag:String, modelDim:Int, rows:Int) = {
    println("Reading test matrix...")
    val dir = System.getProperty("user.dir") + "/"
    val filename = dir + "data" + tag
    var data = Array.tabulate(rows) {i => Array.tabulate(modelDim) {j => 0.0}}
    var row = 0
    var col = 0
    for(line <- Source.fromFile(filename).getLines()){
      data(row)(col) = line.toDouble
      col = col + 1
      if (col == modelDim) {
        row = row + 1
        col = 0
      }
    }
    data
  }

  def CorrectSparseSGD(model: Array[Double], train_x_matrix: Array[Array[Double]], train_y: Array[Double], minibatchDim: Int, numBatches: Int, modelDim: Int, alpha: Double, epochs: Int): Array[Double] = {
    // Initialize updatedModel
    var updatedModel = model map {case e: Double => e}
    var latestModel = updatedModel map {case e: Double => e} // Copy values only (not pointer)
    var y_pred = Array.tabulate(numBatches*minibatchDim){i => 0.0}
    var err = Array.tabulate(numBatches*minibatchDim){i => 0.0}

    for (i <- 0 to epochs-1) {
      for (b <- 0 to numBatches-1) {
        // Collect rows for this minibatch
        var this_x = Array.ofDim[Double](minibatchDim, modelDim)
        var this_y = Array.ofDim[Double](minibatchDim)
        for (pt <- 0 to minibatchDim-1) {
          this_x(pt) = train_x_matrix(b*minibatchDim + pt)
          this_y(pt) = train_y(b*minibatchDim + pt)
        }

        // Update model with latest results
        latestModel = updatedModel

        // 2. Get y_predicted
        y_pred = this_x map {
            case vec: Array[Double] => ((vec zip latestModel) 
              map {case (e1: Double, e2: Double) => e1 * e2}).foldLeft(0.0)((m: Double, n: Double) => m + n)}

        // 3. Subtract y_predicted and y
        err = (this_y zip y_pred) map { case (e1: Double, e2: Double) => e1 - e2}

        // 4. Update model
        var delta = latestModel map {case _ => 0.0}
        // Iterate on each column
        for (col <- 0 to model.length-1) {
          // Extract column vector from train_x_matrix
          val thisCol: Array[Double] = this_x map {_(col)}
          // Dot product this vector with err and divide by number of points to get average gradient
          var sum = 0.0
          for (el <- Array(0, minibatchDim-1)) {
            sum = sum + (thisCol(el) * err(el))
          }
          val gradient = sum / thisCol.length
          val update = gradient * alpha
          delta(col) = update
          updatedModel(col) = latestModel(col) + update
        }
      }

      // // Debugging prints
      // println("x_matrix:\n")
      // for (i <- 0 to this_x.length-1) {
      //   for (j <- 0 to this_x(i).length-1) {
      //     print(" " + this_x(i)(j))
      //   }
      //   println()
      // }
      // println("dot:\n")
      // y_pred map {case e1: Double => print(" " + e1)}
      // println("err:\n")
      // err map {case e1: Double => print(" " + e1)}
      // println("delta:\n")
      // delta map {case e1: Double => print(" " + e1)}
      // println("\nmodel @ step " + i + ":")
      // updatedModel map {case e1: Double => print(e1 + " ")}

    }
    // Return updated model
    updatedModel
  }  
}


object SparseSGDDSE extends DSE {
  val name = "SparseSGD"
  val makeDesign = () => new SparseSGDDesign{}

  val modelDim = Discrete("modelDim", 96 to 19200 by 96)
  val dotMapPar = Discrete("dotMapPar", 1 to 6)
  val dotReducePar = Discrete("dotReducePar", 1 to 19200)
  val Metapipe = Category("Metapipe", true, false)

  (dotReducePar <= modelDim) and (dotReducePar divides modelDim)
}