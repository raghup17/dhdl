import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}

object BramSplitter extends BramSplitterDesign

trait BramSplitterLib extends DesignTest {
  val T = Flt()

  def bramSplitter(K: Int, D: Int) = {
  // Off-chip memory input: K * D
    val off = OffChipArray(T)("x", Const(K), Const(D))

    // 1. Load 'off' -> 'on'
    val on = BRAM(T, K*D, false)
    val onLd = TileMemLd(off, Const(D), Const(0), Const(0), on, K, D)
    onLd.force = Const(true)

    // 2. Create K parallel pipes, where each pipe i
    // copies on(i,0)..on(i,D-1) to another BRAM
    val (copyPipe, bramList) = replicate(K) { i =>
      val b = BRAM(T, 1*D, false)
      val ctr = Ctr((Const(D), 1))
      val addr = Add(Const(i*D), ctr(0))
      val data = Ld(on, addr)
      val st = St(b, ctr(0), data)
      val p = Pipe(ctr, GraphNode(addr, data, st))
      (p, List(b))
    }
    val onList = bramList.map { _(0) }

    // Return the stages as a Sequential
    val splitter = Sequential(onLd, copyPipe)
    (splitter, onList)
  }
}

trait BramSplitterDesign extends DesignTest with BramSplitterLib {

  def main(args: String*) = {
    if (args.size != 2) {
      println("\nUsage: BramSplitter <K> <D>")
      sys.exit(0)
    }

    val K = args(0).toInt
    val D = args(1).toInt

    val (splitter, onList) = bramSplitter(K, D)

    val tilests = onList.map { on =>
      val off = OffChipArray(T)(s"x_$on", Const(1), Const(D))
      val offSt = TileMemSt(off, Const(D), Const(0), Const(0), on, K, D)
      offSt.force = Const(true)
      offSt
    }

    val storeback = Parallel(tilests:_*)

    val top = Sequential(splitter, storeback)
    top
  }

  def test(args: String*) {
  }
}


