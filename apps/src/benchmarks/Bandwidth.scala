import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}
import java.io._
import scala.io.Source

object Bandwidth extends BandwidthDesign
trait BandwidthDesign extends DesignTest {

  val T = Flt()
  val D4bit = FixPt(2,2,false)
  val M8bit = FixPt(4,4,false) // Fix2Float in Node.scala
  var modelDim = 128
  var minibatchDim = 8
  var numBatches = 1
  var alpha = 0.001f
  var epochs = 200
  var bPar = 1
  var ldPar = 1
  var shouldMetapipeline = true


  def main(args: String*) = {
    if (!(args.size == 7)) {
      println(args.size)
      println("\nUsage: Bandwidth <#modelDim> <#minibatchDim> <#numBatches> <#epochs> <#batchPar> <recycle(testing)>")
      sys.exit(0)
    }
    modelDim = args(0).toInt
    minibatchDim = args(1).toInt
    numBatches = args(2).toInt
    epochs = args(3).toInt
    bPar = args(4).toInt
    ldPar = args(5).toInt

    // Get sizes of data structures from host
    val M = ArgIn("M"); bound(M, modelDim)
    val B = ArgIn("B"); bound(B, minibatchDim*numBatches)

    // Declare off-chip data arrays
    val model = OffChipArray(T)("model", M)
    val train_x = OffChipArray(T)("train_x", B, M)
    val train_x_T = OffChipArray(T)("train_x_T", B, M)
    val train_y = OffChipArray(T)("train_y", B)

    val epochCtr = Ctr(List(1))((Const(epochs), 1))

    val loadCtr = Ctr(List(1,bPar))((M,modelDim), (B,minibatchDim))
    val id_model = loadCtr(0)
    val id_batch = loadCtr(1)

    // 1.1. Load tiles from off-chip memory
    val loadBlock = replicate(bPar) { i =>
      // 0.2. Declare BRAMs
      val modelTile = BRAM(T, "modelTile_handle" + i, modelDim, shouldMetapipeline)
      val xTile = BRAM(T, "xTile_handle" + i, modelDim * minibatchDim, shouldMetapipeline)
      val yTile = BRAM(T, "yTile_handle" + i, minibatchDim, shouldMetapipeline)
      val xTileTranspose = BRAM(T, "xTileTranspose_handle" + i, modelDim * minibatchDim, shouldMetapipeline)

      // 1.0 Load off-chip data
      // TODO: turn off modelTileLd if firstEpoch is not true
      val modelTileLd = TileMemLd(ldPar)(model, M, Const(0), Const(0), modelTile, 1, modelDim) // Assume we load entire model
      val xTileLd = TileMemLd(ldPar)(train_x, M, id_batch(i), id_model(0), xTile, minibatchDim, modelDim)
      val yTileLd = TileMemLd(ldPar)(train_y, B, Const(0), id_batch(i), yTile, 1, minibatchDim)
      val xTileTransposeLd = TileMemLd(ldPar)(train_x_T, M, id_batch(i), id_model(0), xTileTranspose, minibatchDim, modelDim)
      modelTileLd.withForce(Const(true))
      xTileLd.withForce(Const(true))
      xTileTransposeLd.withForce(Const(true))
      yTileLd.withForce(Const(true))
      val offChipLoad = Parallel(modelTileLd, xTileLd, yTileLd, xTileTransposeLd)

      (offChipLoad, List(modelTile, xTile, yTile, xTileTranspose))
    }


    val readBlock = replicate(bPar) {i =>

      val readCtr = Ctr(List(ldPar))((Const(ldPar),1))
      val readRdrM = Ld(loadBlock._2(i)(0), readCtr(0))
      val readRdrX = Ld(loadBlock._2(i)(1), readCtr(0))
      val readRdrY = Ld(loadBlock._2(i)(2), readCtr(0))
      val readRdrXT = Ld(loadBlock._2(i)(3), readCtr(0))

      val readPipe = Pipe(readCtr,GraphNode(readRdrX,readRdrY,readRdrM, readRdrXT),None)

      (readPipe, List())
    }
    
    val chain = List(loadBlock._1) ++ List(readBlock._1)
    val tieBlock = MetaPipeline(loadCtr, chain:_*)

    val epochCtrl = Sequential(epochCtr, tieBlock)

    epochCtrl
  }

  def test(args: String*) {
    // Read input args
    val modelDim = args(0).toInt
    val minibatchDim = args(1).toInt
    val numBatches = args(2).toInt
    val recycle = args(4) == "true"

    // Compute number of points, assuming CPU randomly selected batches and put them all in the offchip array for us
    val numPoints = minibatchDim*numBatches

    // Use input args to make random off chip array values 
    setArg("M",modelDim)
    setArg("B",numPoints)

    // Generate new data
    val realModel = if(recycle){ReadData("realModel")} else {Array.tabulate(modelDim){i => (math rint util.Random.nextDouble * 1000) / 1000 }}
    val modelNoise = Array.tabulate(modelDim){i => (math rint util.Random.nextGaussian * 100) / 1000 }
    val model = if(recycle){ReadData("M")} else {(realModel zip modelNoise) map {case (e1: Double, e2: Double) => e1 + e2}}
    val train_x_matrix = if(recycle){ReadMatrix("train_x_matrix", modelDim, numPoints)} else {Array.tabulate(numPoints){i => Array.tabulate(modelDim){j => (math rint util.Random.nextDouble * 1000) / 1000 }}}
    val train_x = if(recycle){ReadData("X")} else {train_x_matrix.flatten}
    val yNoise = Array.tabulate(numPoints){i => (math rint util.Random.nextGaussian * 100) / 1000 }
    var train_y = if(recycle){ReadData("Y")} else {train_x_matrix map {case v1: Array[Double] => 
      (v1 zip realModel).map({case (e1: Double, e2: Double) => 
        e1 * e2}).foldLeft(0.0)((m: Double, n: Double) => m + n)}}
    if (!(recycle)){train_y = (train_y zip yNoise) map {case (e1: Double, e2: Double) => e1 + e2}}

    // Set offchip data tiles
    setMem("model", model)
    setMem("train_x", train_x)
    setMem("train_y", train_y)

    // Show user what off chip array values are
    println("\nBatch:")
    if (numPoints * modelDim > 100) {
      println("<supressed>")
    }
    else {
      for (row <- 0 to numPoints-1) {
        for (col <- 0 to modelDim) {
          if (col == modelDim) {
            print("| " + train_y(row) + "\n")
          }        
          else {
            print(train_x(row*modelDim + col) + " ")
          }
        }
      }
    }
    println("\nExact Model: ")
    if (modelDim > 100) {
      println("<supressed>")
    }
    else {
      for (x <- realModel) {
        print(x + " ")
      }
    }

    if (modelDim > 100) {
      println("<supressed>")
    }
    else {
      println("\nInitial Model:")
      for (x <- model) {
        print(x + " ")
      }
    }

    WriteData(realModel, "realModel")
    WriteMatrix(train_x_matrix, "train_x_matrix")
    WriteData(train_x, "X")
    WriteData(train_y, "Y")
    WriteData(model, "M")

    // Run data through the architecture
    run()

    println("DONE!")

  }

  def WriteData(data:Array[Double], tag:String) = {
    println("Writing test data...")
    val dir = System.getProperty("user.dir") + "/"
    val f = new File(dir + "data" + tag )
    if (f.exists) {f.delete()}
    val pw = new PrintWriter(f)
    for (k <- 0 to data.length-1) {
      if (k == data.length-1) {
        pw.write(data(k).toString)
      }
      else {
        pw.write(data(k) + "\n")
      }
    }
    pw.close
  }

  // Writes matrix as flat array, which makes sparse case easier
  def WriteMatrix(data:Array[Array[Double]], tag:String) = {
    println("Writing test matrix...")
    val dir = System.getProperty("user.dir") + "/"
    val f = new File(dir + "data" + tag )
    if (f.exists) {f.delete()}
    val pw = new PrintWriter(f)
    for (k <- 0 to data.length-1) {
      for (l <- 0 to data(0).length-1){
        if (k == data.length-1 & l == data(0).length-1) {
          pw.write(data(k)(l).toString)
        }
        else {
          pw.write(data(k)(l) + "\n")
        }
      }
    }
    pw.close
  }

  def ReadData(tag:String) = {
    println("Reading test data...")
    val dir = System.getProperty("user.dir") + "/"
    val filename = dir + "data" + tag
    var data = Array.tabulate(0) {i => 0.0}
    for(line <- Source.fromFile(filename).getLines()){
      data = data :+ line.toDouble
    }
    data
  }

  def ReadMatrix(tag:String, modelDim:Int, rows:Int) = {
    println("Reading test matrix...")
    val dir = System.getProperty("user.dir") + "/"
    val filename = dir + "data" + tag
    var data = Array.tabulate(rows) {i => Array.tabulate(modelDim) {j => 0.0}}
    var row = 0
    var col = 0
    for(line <- Source.fromFile(filename).getLines()){
      data(row)(col) = line.toDouble
      col = col + 1
      if (col == modelDim) {
        row = row + 1
        col = 0
      }
    }
    data
  }

}


object BandwidthDSE extends DSE {
  val name = "Bandwidth"
  val makeDesign = () => new BandwidthDesign{}

  val modelDim = Discrete("modelDim", 96 to 19200 by 96)
  val dotMapPar = Discrete("dotMapPar", 1 to 6)
  val dotReducePar = Discrete("dotReducePar", 1 to 19200)
  val Metapipe = Category("Metapipe", true, false)

  (dotReducePar <= modelDim) and (dotReducePar divides modelDim)
}
