import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}
import java.io._
import scala.io.Source

object ParTest extends ParTestDesign
trait ParTestDesign extends DesignTest {

  val T = Flt()
  val D4bit = FixPt(2,2,false)
  val M8bit = FixPt(4,4,false) // Fix2Float in Node.scala
  var modelDim = 128
  var minibatchDim = 8
  var numBatches = 1
  var alpha = 0.001f
  var epochs = 200
  var bPar = 1
  var dotPar = 1
  var shouldMetapipeline = true


  def main(args: String*) = {
    if (!(args.size == 4)) {
      println(args.size)
      println("\nUsage: ParTest <#numBatches> <#batchPar> <#dotProductPar> <recycle(testing)>")
      sys.exit(0)
    }
    modelDim = 96
    minibatchDim = 96
    numBatches = args(0).toInt
    bPar = args(1).toInt
    dotPar = args(2).toInt

    // Get sizes of data structures from host
    val M = ArgIn("M"); bound(M, modelDim)
    val B = ArgIn("B"); bound(B, minibatchDim*numBatches)

    // Declare off-chip data arrays
    val model = OffChipArray(T)("model", M)
    val train_x = OffChipArray(T)("train_x", B, M)
    val train_y = OffChipArray(T)("train_y", B)
    val result = OffChipArray(T)("result", Const(minibatchDim))

    val loadCtr = Ctr(List(1,bPar))((M,modelDim), (B,minibatchDim))
    val id_model = loadCtr(0)
    val id_batch = loadCtr(1)

    // 1.1. Load tiles from off-chip memory
    val comboBlock = replicate(bPar) { i =>
      // 0.2. Declare BRAMs
      val modelTile = BRAM(T, "modelTile_handle" + i, modelDim, false)
      val updatedModel = BRAM(T, "updatedModel_handle" + i, modelDim, false)
      val xTile = BRAM(T, "xTile_handle" + i, modelDim * minibatchDim, false)
      val yTile = BRAM(T, "yTile_handle" + i, minibatchDim, false)

      // 1.0 Load off-chip data
      val modelTileLd = TileMemLd(model, M, Const(0), Const(0), modelTile, 1, modelDim) // Assume we load entire model
      val xTileLd = TileMemLd(train_x, M, id_batch(i), id_model(0), xTile, minibatchDim, modelDim)
      val yTileLd = TileMemLd(train_y, B, Const(0), id_batch(i), yTile, 1, minibatchDim)
      modelTileLd.withForce(Const(true))
      xTileLd.withForce(Const(true))
      yTileLd.withForce(Const(true))
      val offChipLoad = Parallel(modelTileLd, xTileLd, yTileLd)

      // 2. Perform dot product on each row in X and store into a column of BRAM of size minibatchDim
      val dotTile = BRAM(T, "dotTile_handle" + i, minibatchDim, false)
      val tileMultiplier = dotmultTile(xTile,modelTile,dotTile,id_batch(i))

      // 3. Subtract dot product from Y
      val subtractTile = BRAM(T, "subtractTile_handle" + i, minibatchDim, false)
      val tileSubtractor = subtractionTile(yTile,dotTile,subtractTile)

      // 4. Update model 
      val tileUpdator = updateTile(xTile,subtractTile,modelTile,updatedModel,id_batch(i))
      
      val processing_body = List(tileMultiplier) ++ List(tileSubtractor) ++ List(tileUpdator)
      val processingCtrl = Sequential(processing_body:_*)
      
      val body = List(offChipLoad) ++ List(processingCtrl)
      val returnCtrl = Sequential(loadCtr, body:_*)

      (returnCtrl, List(subtractTile))
    }

    // 5. Write to off chip after all epochs
    val tileSt = TileMemSt(result, M, Const(0), Const(0), comboBlock._2(0)(0), 1, modelDim)
    tileSt.withForce(Const(true))
    val topCtr = Ctr((Const(1),1))
    val last = List(comboBlock._1) ++ List(tileSt)
    val topCtrl = Sequential(topCtr,last:_*)

    topCtrl
  }

  // Dot product between each row in minibatch and model
  // TODO: Should rows be parallelized? I think element-wise parallelization provides same in this implementation?
  def dotmultTile(matrixTile: MemNode, vectorTile: MemNode, resultsTile: MemNode, id_batch: CombNode) = {

    val rowCtr = Ctr(List(1))((Const(minibatchDim),1))
    val row = rowCtr(0)

    val accum = Reg(T)
    val colCtr = Ctr(List(dotPar))((Const(modelDim),1))
    val s2 = reducePipe(1, colCtr, accum) {i => 
      val addrx = Add(Mul(row, Const(modelDim)), colCtr(0))
      val v1 = Ld(vectorTile, colCtr(0))
      val v2 = Ld(matrixTile, addrx)
      Mul(v1,v2)
    }{(a,b) => Add(a,b) }
    accum.setParent(s2)

    val s3 = replicate(1) {pid =>
      val st = St(resultsTile, rowCtr(0), accum, pid, 1)
      val storePipe = Pipe(st)
      (storePipe, List())
    }

    val dotmult = Sequential(rowCtr, s2, s3._1)
    dotmult
  }

  // Subtract y computed from model from y given in training set
  def subtractionTile(TileA: MemNode, TileB: MemNode, TileC: MemNode) = {
    val rowCtr = Ctr(List(1))((Const(minibatchDim),1))
    val row = rowCtr(0)

    val s1 = replicates(1) {pid => 
      val e1 = Ld(TileA, row)
      val e2 = Ld(TileB, row)
      val sub = Sub(e1, e2)
      val st = St(TileC, row, sub, pid, 1)
      val subPipe = Pipe(rowCtr, GraphNode(e1, e2, sub, st), None) 
      (subPipe, List())
    }

    val subtract = s1._1
    subtract

  }

  // Compute a new model by adding gradient*step to old model (using linreg gradient)
  def updateTile(inputTile: MemNode, errorVector: MemNode, modelVector: MemNode, newModelVector: MemNode, id_batch: CombNode) = {
    val elementCtr = Ctr(List(1))((Const(modelDim),1))
    val el = elementCtr(0)

    val s1 = replicates(1) {pid => 
      val accum = Reg(T)
      val pointCtr = Ctr(List(dotPar))((Const(minibatchDim),1))
      val point = pointCtr(0)
      val addrx = Add(Mul(point, Const(modelDim)), el)
      val e1 = Ld(inputTile, addrx, modelDim)
      val e2 = Ld(errorVector, point)
      val prod = Mul(e1, e2)
      val sum = Add(accum, prod)
      accum.write(sum)
      val s1Pipe = Pipe(pointCtr, GraphNode(addrx, e1, e2, prod), ReduceTree(prod, accum, sum))
      accum.setParent(s1Pipe)
      (s1Pipe, List(accum))
    }

    val s2 = replicate(1) { pid =>
      val m1 = Ld(modelVector, el)
      val m2 = Ld(newModelVector, el)
      val logicBit = Eq(id_batch, Const(0))
      val e1 = Mux(logicBit, m1, m2)
      val temp = Div(Mul(s1._2(pid)(0), Const(alpha)), Fix2Float(Const(minibatchDim)))
      val sum = Add(e1, temp)
      val st = St(newModelVector, el, sum, pid, 1)
      val s2Pipe = Pipe(GraphNode(m1, m2, e1, temp, sum, st, logicBit), None)
      (s2Pipe, List())
    }

    val update = Sequential(elementCtr, s1._1, s2._1)
    update
  }

  def test(args: String*) {
    // Read input args
    val modelDim = 96
    val batchDim = 96
    val numBatches = args(0).toInt
    val recycle = args(3) == "true"
    val maxDPcheck = 9

    // Compute number of points, assuming CPU randomly selected batches and put them all in the offchip array for us
    val numPoints = minibatchDim*numBatches

    // Use input args to make random off chip array values 
    setArg("M",modelDim)
    setArg("B",numPoints)

    // Generate new data
    val realModel = if(recycle){ReadData("realModel")} else {Array.tabulate(modelDim){i => (math rint util.Random.nextDouble * 1000) / 1000 }}
    val modelNoise = Array.tabulate(modelDim){i => (math rint util.Random.nextGaussian * 100) / 1000 }
    val model = if(recycle){ReadData("M")} else {(realModel zip modelNoise) map {case (e1: Double, e2: Double) => e1 + e2}}
    val train_x_matrix = if(recycle){ReadMatrix("train_x_matrix", modelDim, numPoints)} else {Array.tabulate(numPoints){i => Array.tabulate(modelDim){j => (math rint util.Random.nextDouble * 1000) / 1000 }}}
    val train_x = if(recycle){ReadData("X")} else {train_x_matrix.flatten}
    val yNoise = Array.tabulate(numPoints){i => (math rint util.Random.nextGaussian * 100) / 1000 }
    var train_y = if(recycle){ReadData("Y")} else {train_x_matrix map {case v1: Array[Double] => 
      (v1 zip realModel).map({case (e1: Double, e2: Double) => 
        e1 * e2}).foldLeft(0.0)((m: Double, n: Double) => m + n)}}
    if (!(recycle)){train_y = (train_y zip yNoise) map {case (e1: Double, e2: Double) => e1 + e2}}

    // Set offchip data tiles
    setMem("model", model)
    setMem("train_x", train_x)
    setMem("train_y", train_y)

    // Show user what off chip array values are
    println("\nBatch:")
    if (numPoints * modelDim > 100) {
      println("<supressed>")
    }
    else {
      for (row <- 0 to numPoints-1) {
        for (col <- 0 to modelDim) {
          print(train_x(row*modelDim + col) + " ")
        }
      }
    }
    println("\nExact Model: ")
    if (modelDim > 100) {
      println("<supressed>")
    }
    else {
      for (x <- realModel) {
        print(x + " ")
      }
    }

    if (modelDim > 100) {
      println("<supressed>")
    }
    else {
      println("\nInitial Model:")
      for (x <- model) {
        print(x + " ")
      }
    }

    // Compute Gold
    val outGold = CorrectParTest(model, train_x_matrix, train_y, minibatchDim, numBatches, modelDim)
    println("\nCorrect Output: ")
    outGold map {case e1: Double => print(e1 + " ")}
    WriteData(realModel, "realModel")
    WriteMatrix(train_x_matrix, "train_x_matrix")
    WriteData(train_x, "X")
    WriteData(train_y, "Y")
    WriteData(model, "M")
    WriteData(outGold, "O")

    // Run data through the architecture
    run()

    // Extract DHDL results
    val out = getMem[Double]("result", 0 until modelDim)
    println("\nDHDL Output: ")
    out map {case e1: Double => print(e1 + " ")}
    println

    // Find abs(difference)
    val diff = (outGold zip out) map {case (e1: Double, e2: Double) => e1 - e2}

    // Compare
    println("Comparing outGold - outDHDL")
    // For each element in updated model
    for (i <- 0 to diff.length-1) {
      // Keep going until we find failure point
      var th = 0
      while (th < maxDPcheck+1) {
        if (diff(i) > 1/Math.pow(10,th) | diff(i) < -1/Math.pow(10,th)) {
          println(i + ": Diverge @ " + th + "dp: " + diff(i))
          th = 9999 // Break while loop
        }
        else if (th == maxDPcheck) {
          println(i + ": OK! (" + maxDPcheck + " DP)")
          th = 9999 // Break while loop
        }
        else {
          th = th + 1
        }
      }
    }
  }

  def WriteData(data:Array[Double], tag:String) = {
    println("Writing test data...")
    val dir = System.getProperty("user.dir") + "/"
    val f = new File(dir + "data" + tag )
    if (f.exists) {f.delete()}
    val pw = new PrintWriter(f)
    for (k <- 0 to data.length-1) {
      if (k == data.length-1) {
        pw.write(data(k).toString)
      }
      else {
        pw.write(data(k) + "\n")
      }
    }
    pw.close
  }

  // Writes matrix as flat array, which makes sparse case easier
  def WriteMatrix(data:Array[Array[Double]], tag:String) = {
    println("Writing test matrix...")
    val dir = System.getProperty("user.dir") + "/"
    val f = new File(dir + "data" + tag )
    if (f.exists) {f.delete()}
    val pw = new PrintWriter(f)
    for (k <- 0 to data.length-1) {
      for (l <- 0 to data(0).length-1){
        if (k == data.length-1 & l == data(0).length-1) {
          pw.write(data(k)(l).toString)
        }
        else {
          pw.write(data(k)(l) + "\n")
        }
      }
    }
    pw.close
  }

  def ReadData(tag:String) = {
    println("Reading test data...")
    val dir = System.getProperty("user.dir") + "/"
    val filename = dir + "data" + tag
    var data = Array.tabulate(0) {i => 0.0}
    for(line <- Source.fromFile(filename).getLines()){
      data = data :+ line.toDouble
    }
    data
  }

  def ReadMatrix(tag:String, modelDim:Int, rows:Int) = {
    println("Reading test matrix...")
    val dir = System.getProperty("user.dir") + "/"
    val filename = dir + "data" + tag
    var data = Array.tabulate(rows) {i => Array.tabulate(modelDim) {j => 0.0}}
    var row = 0
    var col = 0
    for(line <- Source.fromFile(filename).getLines()){
      data(row)(col) = line.toDouble
      col = col + 1
      if (col == modelDim) {
        row = row + 1
        col = 0
      }
    }
    data
  }

  def CorrectParTest(model: Array[Double], train_x_matrix: Array[Array[Double]], train_y: Array[Double], minibatchDim: Int, numBatches: Int, modelDim: Int): Array[Double] = {
    // Initialize updatedModel
    var updatedModel = model map {case e: Double => e}
    var latestModel = updatedModel map {case e: Double => e} // Copy values only (not pointer)
    var y_pred = Array.tabulate(numBatches*minibatchDim){i => 0.0}
    var err = y_pred map {case _ => 0.0}

    for (b <- 0 to numBatches - 1) {
      // Collect rows for this minibatch
      var this_x = Array.ofDim[Double](minibatchDim, modelDim)
      var this_y = Array.ofDim[Double](minibatchDim)
      for (pt <- 0 to minibatchDim-1) {
        this_x(pt) = train_x_matrix(b*minibatchDim + pt)
        this_y(pt) = train_y(b*minibatchDim + pt)
      }

      // Update model with latest results
      latestModel = updatedModel

      // 2. Get y_predicted
      // val y_pred = this_x map {
      y_pred = this_x map {
          case vec: Array[Double] => ((vec zip model) 
            map {case (e1: Double, e2: Double) => e1 * e2}).foldLeft(0.0)((m: Double, n: Double) => m + n)}
      // 3. Subtract y_predicted and y
      err = (this_y zip y_pred) map { case (e1: Double, e2: Double) => e1 - e2}

      // 4. Update model
      var delta = latestModel map {case _ => 0.0}
      // Iterate on each column
      for (col <- 0 to model.length-1) {
        // Extract column vector from train_x_matrix
        val thisCol: Array[Double] = this_x map {_(col)}
        // Dot product this vector with err and divide by number of points to get average gradient
        var sum = 0.0
        for (el <- 0 to thisCol.length - 1) {
          sum = sum + (thisCol(el) * err(el))
        }
        val gradient = sum / thisCol.length
        val update = gradient * alpha
        delta(col) = update
        updatedModel(col) = latestModel(col) + update
      }

    }

    // // Debugging prints
    // println("x_matrix:\n")
    // for (i <- 0 to this_x.length-1) {
    //   for (j <- 0 to this_x(i).length-1) {
    //     print(" " + this_x(i)(j))
    //   }
    //   println()
    // }
    // println("dot:\n")
    // y_pred map {case e1: Double => print(" " + e1)}
    // println("err:\n")
    // err map {case e1: Double => print(" " + e1)}
    // println("delta:\n")
    // delta map {case e1: Double => print(" " + e1)}
    // println("\nmodel @ step " + i + ":")
    // updatedModel map {case e1: Double => print(e1 + " ")}

    // Return updated model
    err
  }  
}


object ParTestDSE extends DSE {
  val name = "ParTest"
  val makeDesign = () => new ParTestDesign{}

  val modelDim = Discrete("modelDim", 96 to 19200 by 96)
  val dotMapPar = Discrete("dotMapPar", 1 to 6)
  val dotReducePar = Discrete("dotReducePar", 1 to 19200)
  val Metapipe = Category("Metapipe", true, false)

  (dotReducePar <= modelDim) and (dotReducePar divides modelDim)
}
