import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design,DesignTest,DSE}
import scala.math._

object kNN extends kNNDesign
object kNNTest extends kNNDesign with DesignTest {
  def test(args: String*) {
    val ptTileSize = args(0).toInt
    val dimTileSize = args(1).toInt
    println("Do nothing!")
  }
}
trait kNNDesign extends Design {

  // Tuning parameters
  var shouldMpOuter = true
  var shouldMpInner = true
  var shouldMpInnermost = true
  var tileLoopPar = 2
  var testLoopPar = 2
  var distLoopPar = 8
  val k = 64
  val numDistinctLabels = k

  var train: OffChipArray = null
  var test: OffChipArray = null
  var label: OffChipArray = null
  var newLabel: OffChipArray = null

  def kNearestDist(testBlk: BRAM, newLabelBlk: BRAM, D: Int, N: CombNode, trainTileSize: Int, testTileSize: Int) = {
    val iCtr = Ctr(List(testLoopPar))((Const(testTileSize), 1))

    val body = replicate(testLoopPar) { pid =>
      // For each point i in test block,
      // Load all training blocks,
      // calculate distances to all points in training block
      // and store minimum K in a priority queue

      val i = iCtr(0)(pid)  // Point i in test block
      val jCtr = Ctr((N, trainTileSize))
      val j = jCtr(0)  // Point j in training block

      // Load training set tile and label tile into on-chip memory
      val trainRAM = BRAM(FloatPt(), trainTileSize*D, shouldMpOuter)
      val labelRAM = BRAM(trainTileSize, shouldMpOuter)
      val trainTileLd = TileMemLd(train, Const(D), j, Const(0), trainRAM, trainTileSize, D)
      val labelTileLd = TileMemLd(label, Const(D), Const(0), j, labelRAM, 1, trainTileSize)
      val trainLabelLd = Parallel(trainTileLd, labelTileLd)

      // Distance calculation loop for the entire tile
      val ptsCtr = Ctr((Const(trainTileSize), 1))
      val jj = ptsCtr(0)  // jj == index of training point within tile

      val dimCtr = Ctr(List(distLoopPar))((Const(D), 1))
      val d = dimCtr(0)
      val trainBase = Mul(jj,Const(D))
      val trainAddr = Add(trainBase, d)
      val testBase = Mul(i,Const(D))
      val testAddr = Add(testBase, d)
      val trainPtDim = Ld(trainRAM, trainAddr)
      val testPtDim = Ld(testBlk, testAddr)
      val diff = Sub(trainPtDim, testPtDim)
      val sqrDiff = Mul(diff, diff)
      val distAccum = Reg(FloatPt(), shouldMpInner)
      val sumSqrDiff = Add(distAccum, sqrDiff)
      distAccum.write(sumSqrDiff)
      val mapNode = GraphNode(trainBase, trainAddr, testBase, testAddr, trainPtDim, testPtDim, diff, sqrDiff)
      val redNode = ReduceTree(sqrDiff, distAccum, sumSqrDiff)
      val distPipe = Pipe(dimCtr, mapNode, redNode)
      distAccum.setParent(distPipe)
      distAccum.setWriter(distPipe)

      // Store distance into priority queue
      val pq = PQ(k, false)
      val labelLd = Ld(labelRAM, jj)
      val storeMul = Mul(j, Const(D))
      val storeAddr = Add(storeMul, jj)
      val distAsInt = Float2Fix(distAccum) // Required currently due to MaxJ limitations
      val bundleToStore = Bundle(distAsInt, labelLd)
      val st = St(pq, storeAddr, bundleToStore)
      val storePipe = Pipe(labelLd, storeMul, storeAddr, distAsInt, bundleToStore, st)
      distAccum.addReader(storePipe)

      val distmp = if (shouldMpInnermost) MetaPipeline(ptsCtr, distPipe, storePipe) else Sequential(ptsCtr, distPipe, storePipe)
      val s1 = if (shouldMpInner) MetaPipeline(jCtr, trainLabelLd, distmp) else Sequential(jCtr, trainLabelLd, distmp)

      // Wait for writes to propagate, reset accumulator
      val waitCtr = Ctr((Const(k-1), 1))
      val waitPipe = Pipe(waitCtr, GraphNode())

      val accumRAM = BRAM(numDistinctLabels)
  //    val resetCtr = Ctr((Const(numDistinctLabels), 1))
  //    val resetBlk = St(accumRAM, resetCtr(0), Const(0))
  //    val resetPipe = Pipe(resetCtr, GraphNode(resetBlk))
  //    val waitStage = Parallel(waitPipe, resetPipe)

      // Accum pipe, get label for point i
      val maxr = Reg(CompositeType(List(UInt(), UInt())), false)
      val accumCtr = Ctr((Const(k), 1))
      val labelBundle = Ld(pq, accumCtr(0))
      val labelIdx = labelBundle.get(1)
      val labelAccum = Ld(accumRAM, labelIdx)
      val inc = Add(labelAccum, Const(1))
      val st2 = St(accumRAM, labelIdx, inc)
      val labelIncBundle = Bundle(inc, labelIdx)
      val newMax = MaxWithMetadata(labelIncBundle, maxr)
      maxr.write(newMax)
      val accumPipe = Pipe(accumCtr,
          GraphNode(labelBundle, labelIdx, labelAccum, inc, st2, labelIncBundle),
          ReduceTree(inc, maxr, newMax))
      maxr.setParent(accumPipe)

      // Stage 4: Store label into newLabelBlk(i)
      val stLabel = St(newLabelBlk, i, maxr.get(1), pid, testLoopPar)
      val storeNewLabel = Pipe(stLabel)
      (Sequential(s1, accumPipe, waitPipe, storeNewLabel), List())
    }

    // Connect as a Sequential
//    val processTestPt = Sequential(iCtr, s1, accumPipe, waitPipe, storeNewLabel)
    val processTestPt = Sequential(iCtr, body._1)
    processTestPt
  }

  def main(args: String*) = {
    if (args.size != 9) {
      println("\nUsage: kNN <#testTileSize> <#trainTileSize> <tileLoopPar> <testLoopPar> <distLoopPar> <mpOuter> <mpMiddle> <mpInner> <#dims>")
      sys.exit(0)
    }

    val testTileSize = args(0).toInt
    val trainTileSize = args(1).toInt
    val D = 96 //args(2).toInt

    tileLoopPar = args(2).toInt
    testLoopPar = args(3).toInt
    distLoopPar = args(4).toInt
    shouldMpOuter = args(5) == "true"
    shouldMpInner = args(6) == "true"
    shouldMpInnermost = args(7) == "true"

    val N = ArgIn("numTrain"); bound(N, 960)
    val R = ArgIn("numTest"); bound(R, 720)

    // Offchip arrays - training and test set
    train = OffChipArray(FloatPt())("train", N, Const(D))
    test = OffChipArray(FloatPt())("test", R, Const(D))
    label = OffChipArray("label", N)
    newLabel = OffChipArray("newLabel", R)

    // Ctr over test points, stride of testTileSize
    val iCtr = Ctr(List(tileLoopPar))((R, testTileSize))

    // 1. Load test set tile into on-chip memory
    val (testLdStage, testRAMList) = replicate(tileLoopPar) { pid =>
      val i = iCtr(0)(pid)
      val testRAM = BRAM(FloatPt(), testTileSize*D, shouldMpOuter)
      val testLd = TileMemLd(test, Const(D), i, Const(0), testRAM, testTileSize, D)
      (testLd, List(testRAM))
    }

    // 2. Process testRAM, get labels in newLabelRAM
    val (processStage, newLabelRAMList) = replicate(tileLoopPar) { pid =>
      val newLabelRAM = BRAM(testTileSize, shouldMpOuter)
      val kmp = kNearestDist(testRAMList(pid)(0).asInstanceOf[BRAM], newLabelRAM, D, N, trainTileSize, testTileSize)
      (kmp, List(newLabelRAM))
    }

      // 3. Store newLabelRAM out
    val (stStage, emptyList) = replicate(tileLoopPar) { pid =>
      val i = iCtr(0)(pid)
      (TileMemSt(newLabel, R, Const(0), i, newLabelRAMList(pid)(0), 1, testTileSize), List())
    }

    val top = MetaPipeline(iCtr, testLdStage, processStage, stStage)
    top
  }
}

object kNNDSE extends DSE {
  val name = "kNN"
  val makeDesign = () => new kNNDesign{}
  val TestTileSize = Discrete("TestTileSize", (6 to 960 by 6).toList :+ 1)      // 96
  val TrainTileSize = Discrete("TrainTileSize", (6 to 960 by 6).toList :+ 1)    // 96
  val TileLoopPar = Discrete("TileLoopPar", 1 to 3)                             // 3
  val TestLoopPar = Discrete("TestLoopPar", 1 to 6)                             // 6
  val DistLoopPar = Discrete("DistLoopPar", 1 to 96)                            // 12
  val MetaOuter = Category("MetaOuter", true, false)                            // 2
  val MetaMiddle = Category("MetaMiddle", true, false)                          // 2
  val MetaInner = Category("MetaInner", true, false)                            // 2

  (TileLoopPar <= TestTileSize) and (TileLoopPar divides TestTileSize)
  (TestLoopPar <= TestTileSize) and (TestLoopPar divides TestTileSize)
  val Dims = Discrete("Dims", List(96))
  (DistLoopPar <= Dims) and (DistLoopPar divides Dims)

}



//    val numPoints = 2*ptTileSize
//    val dim = dimTileSize
//    val numCents = ptTileSize
//
//    setArg("numPoints", numPoints)
//    setArg("numCents", numCents)
//    setArg("dimension", dim)
//
//    val points = Array.tabulate(numPoints){i =>
//      Array.tabulate(dim){j => util.Random.nextDouble()*1000.0}}
//
//    setMem("points", points.flatten)
//
//    val cents = points.slice(0,numCents)
//
//    //println("Points:")
//    //points.map(point => println(point.mkString(" ") + ", "))
//    //println("Cents:")
//    //cents.map(cent => println(cent.mkString(" ") + ", "))
//
//    /*
//    // GroupBy reorder the centroids, which makes it hard to compare.
//
//    val groups = points.groupBy(point=>
//      cents.map(cent=>
//        sqrt(point.zip(cent).map{
//          case(p1,p2)=>pow(p1-p2,2)}.reduceLeft((x,y)=>x+y))
//      ).zip(cents).minBy(_._1)._2
//    )
//
//    val newCents = groups.valuesIterator.toList
//      .map(group => group.transpose.map(_.sum/group.size))
//
//    */
//
//    var centsAccum = Array.fill(numCents, dim) {0.0}
//    var centsCount = Array.fill(numCents) {0}
//
//    points.map(point => {
//        var currMin = 1000000000000.0
//        var minIdx = 0
//        cents.zipWithIndex.map{case (cent,idx) =>
//            def dist(p1:Array[Double], p2:Array[Double]) =
//              sqrt(p1.zip(p2).map{case (d1,d2)=>pow(d1-d2,2)}.reduceLeft((x,y)=>x+y))
//            val currDist = dist(point,cent)
//            if (currDist < currMin) {
//              currMin = currDist
//              minIdx = idx
//            }
//        }
//        centsAccum(minIdx) = centsAccum(minIdx).zip(point).map{case (d1,d2) => d1+d2}
//        centsCount(minIdx) += 1
//    }
//    )
//
//    //println("centsAccum:")
//    //centsAccum.map(centAccum => println(centAccum.mkString(" ") + ", "))
//    //println("centsCount:")
//    //println(centsCount.mkString(", "))
//
//    val newCents = centsAccum.zipWithIndex.map{case (centAccum,idx) =>
//      centAccum.map(d => d/centsCount(idx))}
//
//    println("newCents:")
//    newCents.map(newCent => println(newCent.mkString(" ") + ", "))
//
//    run()
//
//    val result = getMem[Double]("newCents", 0 until numCents*dim)
//    println("Result: ")
//    println(result.mkString(", "))
//    compare(result, newCents.flatten)

//object KmeansTest extends DHDLTest {
//  def main(args: String*) = {
//    if (args.size != 2) {
//      println("\nUsage: Kmeans <#pointTileSize> <#dimTileSize>")
//      sys.exit(0)
//    }
//    Kmeans.ptTileSize = args(0).toInt
//    Kmeans.dimTileSize = args(1).toInt
//    val ptTileSize = Kmeans.ptTileSize
//    val dimTileSize = Kmeans.dimTileSize
//    /* Algorithm Inputs */
//    val numPoints = ArgIn("numPoints")
//    val numCents = ArgIn("numCents")
//    val dim = ArgIn("dimension")
//    val points = OffChipArray(FloatPt())("points", numPoints, dim)
//    val cents = BRAM(FloatPt(),ptTileSize*dimTileSize, false)
//    val centsLd = TileMemLd(points, dim, Const(0), Const(0), cents, ptTileSize, dimTileSize)
//    val (meta_accumCents, newCentsAccumB, newCentsCountB) =
//      Kmeans.accumCloestCents(numPoints, points, dim, numCents, cents)
//    val centsAccum = OffChipArray(FloatPt())("centsAccum", numCents, dim)
//    val centsCount = OffChipArray(UInt())("centsCount", numCents, ArgIn("1"))
//    val newCentsAccumSt = TileMemSt(centsAccum, dim, Const(0), Const(0), newCentsAccumB, ptTileSize,
//      dimTileSize)
//    val newCentsCountSt = TileMemSt(centsCount, Const(1), Const(0), Const(0), newCentsCountB,
//      ptTileSize, 1)
//
//    val top = Sequential(centsLd, meta_accumCents, newCentsAccumSt, newCentsCountSt)
//    top
//  }
//  def test(args: String*) {
//    val ptTileSize = args(0).toInt
//    val dimTileSize = args(1).toInt
//    val numPoints = 2*ptTileSize
//    val dim = dimTileSize
//    val numCents = ptTileSize
//
//    setArg("numPoints", numPoints)
//    setArg("numCents", numCents)
//    setArg("dimension", dim)
//    setArg("1",1)
//
//    val points = Array.tabulate(numPoints){i =>
//      Array.tabulate(dim){j => util.Random.nextDouble()*1000.0}}
//
//    setMem("points", points.flatten)
//
//    val cents = points.slice(0,numCents)
//
//    println("Points:")
//    points.map(point => println(point.mkString(" ") + ", "))
//    println("Cents:")
//    cents.map(cent => println(cent.mkString(" ") + ", "))
//    var centsAccum = Array.fill(numCents, dim) {0.0}
//    var centsCount = Array.fill(numCents) {0}
//
//    points.map(point => {
//        var currMin = 1000000000000.0
//        var minIdx = 0
//        cents.zipWithIndex.map{case (cent,idx) =>
//            def dist(p1:Array[Double], p2:Array[Double]) =
//              sqrt(p1.zip(p2).map{case (d1,d2)=>pow(d1-d2,2)}.reduceLeft((x,y)=>x+y))
//            val currDist = dist(point,cent)
//            if (currDist < currMin) {
//              currMin = currDist
//              minIdx = idx
//            }
//        }
//        centsAccum(minIdx) = centsAccum(minIdx).zip(point).map{case (d1,d2) => d1+d2}
//        centsCount(minIdx) += 1
//    }
//    )
//
//    println("centsAccum:")
//    centsAccum.map(centAccum => println(centAccum.mkString(" ") + ", "))
//    println("centsCount:")
//    println(centsCount.mkString(", "))
//
//    val newCents = centsAccum.zipWithIndex.map{case (centAccum,idx) =>
//      centAccum.map(d => d/centsCount(idx))}
//
//    println("newCents:")
//    newCents.map(newCent => println(newCent.mkString(" ") + ", "))
//
//    run()
//
//    val centsAccumResult = getMem[Double]("centsAccum", 0 until numCents*dim)
//    println("centsAccumResult: ")
//    println(centsAccumResult.mkString(", "))
//    val centsCountResult = getMem[Int]("centsCount", 0 until numCents)
//    println("centsCountResult: ")
//    println(centsCountResult.mkString(", "))
//    //println(s"$result =?= $gold")
//    //compare(result, gold)
//  }
//}

/*object kNNDSE extends DSE {
  val name = "kNN"
  val makeDesign = () => new kNNDesign{}

  val TileSize = Discrete("TileSize", 96 to 960 by 96)
  val PointPar = Discrete("PointPar", 1 to 1000)
  val DistPar  = Discrete("DistPar", 1 to 1000)
  val OuterMeta = Category("OuterMetapipe", true, false)
  val InnerMeta = Category("InnerMetapipe", true, false)
}*/
