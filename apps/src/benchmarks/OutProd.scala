import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}

object OutProd extends OutProdDesign
object OutProdTest extends OutProdDesign with DesignTest {
  def test(args: String*) {
    val tileSize = args(0).toInt
    val vecLength = 4*tileSize
    setArg("vecLength", vecLength)

    val vec1 = Array.tabulate(vecLength){i => util.Random.nextInt(1000)}
    val vec2 = Array.tabulate(vecLength){i => util.Random.nextInt(1000)}

    setMem("vec1", vec1)
    setMem("vec2", vec2)

    run()

    val gold = Array.tabulate(vecLength){i =>
      Array.tabulate(vecLength){j =>
        vec1(i)*vec2(j)
      }
    }

    val result = getMem[Int]("result", 0 until vecLength*vecLength)
    compare(result,gold.flatten)
  }
}
trait OutProdDesign extends Design {

  var tileSize = 96
  var innerLoopPar = 1
  var outerLoopPar = 4
  var shouldMetapipeline = false

  def tiledOutProd(vec1B: BRAM, vec2B:BRAM, tileSize:Int, outputDblBuf:Boolean, par: Int = 1, tp:Type = UInt()) =  {
      if (vec1B.t!=tp || vec2B.t!=tp)
        throw new Exception("vectors to perform outer product must have the same type as output vector!")
      val resultB = BRAM(tp, tileSize*tileSize, outputDblBuf)

      val opCtrs = Ctr(List(1, par))((Const(tileSize), 1), (Const(tileSize), 1))
      val mapNode = Block({
        val v1xv2 = Mul(Ld(vec1B, opCtrs(0)), Ld(vec2B, opCtrs(1)))
        val resRowBase = Mul(Const(tileSize), opCtrs(0))
        val resAddr = Add(resRowBase, opCtrs(1))
        val resultBSt = St(resultB, resAddr, v1xv2)
      })
      val pipe = Pipe(opCtrs, mapNode)
      (pipe, resultB)
  }

  def main(args: String*) = {
    if (args.size != 4) {
      println("Usage: OutProd <tileSize> <outerLoopPar> <innerLoopPar> <shouldMetapipeline>")
      sys.exit(0)
    }
    tileSize = args(0).toInt
    outerLoopPar = args(1).toInt
    innerLoopPar = args(2).toInt
    shouldMetapipeline = args(3) == "true"

    val vecLength = ArgIn("vecLength");   bound(vecLength, 3840)
    val vec1 = OffChipArray("vec1", vecLength)
    val vec2 = OffChipArray("vec2", vecLength)
    val result = OffChipArray("result", vecLength, vecLength)

    val tileCtrs = Ctr(List(1, outerLoopPar))((vecLength, tileSize), (vecLength, tileSize))

    val vec1B = BRAM(tileSize, shouldMetapipeline)
    val vec1Ld = TileMemLd(innerLoopPar)(vec1, vecLength, Const(0), tileCtrs(0), vec1B, 1, tileSize)
    val (vec2Lds, bramList) = replicate(outerLoopPar) { pid =>
      val vec2B = BRAM(tileSize, shouldMetapipeline)
      val vec2Ld = TileMemLd(innerLoopPar)(vec2, vecLength, Const(0), tileCtrs(1)(pid), vec2B, 1, tileSize)
      (vec2Ld, List(vec1B, vec2B))
    }
    val ldStage = Parallel(vec1Ld, vec2Lds)

    val (pipe_dotProd, resultB) = replicate(outerLoopPar) { pid =>
      val vec1 = bramList(pid)(0).asInstanceOf[BRAM]
      val vec2 = bramList(pid)(1).asInstanceOf[BRAM]
      val (pipe, res) = tiledOutProd(vec1, vec2, tileSize, shouldMetapipeline, innerLoopPar)
      (pipe, List(res))
    }

    val (resultSt, emptyList) = replicate(outerLoopPar) { pid =>
      (TileMemSt(innerLoopPar, result, vecLength, tileCtrs(0), tileCtrs(1)(pid), resultB(pid)(0), tileSize, tileSize), List())
    }

    val top = if (shouldMetapipeline) {
      MetaPipeline(tileCtrs, ldStage, pipe_dotProd, resultSt)
    } else {
      Sequential(tileCtrs, ldStage, pipe_dotProd, resultSt)
    }
    top
  }
}

object OutProdDSE extends DSE {
  val name = "OutProd"
  val makeDesign = () => new OutProdDesign{}

  val TileSize = Discrete("TileSize", 96 to 19200 by 96)
  val OuterPar = Discrete("OuterPar", 6 to 12)
  val InnerPar = Discrete("InnerPar", 1 to 19200)
  val Metapipe = Category("Metapipe", true, false)

  (InnerPar <= TileSize) and (InnerPar divides TileSize)
  (OuterPar <= TileSize) and (OuterPar divides TileSize)
}

