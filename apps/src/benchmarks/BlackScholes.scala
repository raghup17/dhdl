import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}

/**
 *  Implementation of BlackScholes option pricing benchmark
 *  in DHDL. This implementation is closely based on the
 *  implementation of the 'blackscholes' benchmark in the
 *  PARSEC benchmark suite.
 */
object BlackScholes extends BlackScholesDesign
object BlackScholesTest extends BlackScholesDesign with DesignTest {
  def test(args: String*) {
    val tileSize = args(0).toInt
    println("Nothing tested yet!")
//    setArg("C",C)
//    val vec1 = Array.tabulate(C){i => util.Random.nextInt(100)}
//    val vec2 = Array.tabulate(C){i => util.Random.nextInt(100)}
//
//    setMem("vec1", vec1)
//    setMem("vec2", vec2)
//
//    run()
//
//    val outGold = vec1.zip(vec2).map{case (a,b) => a*b}.sum
//    val out = getArg[Int]("out")
//
//    compare(out, outGold)
  }
}
trait BlackScholesDesign extends Design {
  var shouldMetapipeline = true
  var outerPar = 1
  var innerPar = 1
  var tileSize = 96

//#define inv_sqrt_2xPI 0.39894228040143270286
  val inv_sqrt_2xPI = Const(0.39894228040143270286f)

  def CNDF(InputX: CombNode) = {
    // Check for negative value of InputX
    val lt_0 = Lt(InputX, Const(0.0f))
    val sign = Mux(lt_0, Const(true), Const(false))
    val xInput = Abs(InputX)

    // Compute NPrimeX term common to both four & six decimal accuracy calcs
//    expValues = exp(-0.5f * InputX * InputX);
    val inputSqr = Mul(xInput, xInput)
    val mul = Mul(Const(-0.5f), inputSqr)
    val expValues = Exp(mul)

//    xNPrimeofX = expValues;
//    xNPrimeofX = xNPrimeofX * inv_sqrt_2xPI;
    val xNPrimeofX = Mul(expValues, inv_sqrt_2xPI)

//    xK2 = 0.2316419 * xInput;
//    xK2 = 1.0 + xK2;
//    xK2 = 1.0 / xK2;
    val xK20 = Mul(Const(0.2316419f), xInput)
    val xK21 = Add(Const(1.0f), xK20)
    val xK2 = Div(Const(1.0f), xK21)

//    xK2_2 = xK2 * xK2;
    val xK2_2 = Mul(xK2, xK2)

//    xK2_3 = xK2_2 * xK2;
    val xK2_3 = Mul(xK2_2, xK2)

//    xK2_4 = xK2_3 * xK2;
    val xK2_4 = Mul(xK2_3, xK2)

//    xK2_5 = xK2_4 * xK2;
    val xK2_5 = Mul(xK2_4, xK2)

//    xLocal_1 = xK2 * 0.319381530;
    val xLocal_10 = Mul(xK2, Const(0.319381530f))

//    xLocal_2 = xK2_2 * (-0.356563782);
    val xLocal_20 = Mul(xK2_2, Const(-0.356563782f))

//    xLocal_3 = xK2_3 * 1.781477937;
    val xLocal_30 = Mul(xK2_3, Const(1.781477937f))

//    xLocal_2 = xLocal_2 + xLocal_3;
    val xLocal_21 = Add(xLocal_20, xLocal_30)

//    xLocal_3 = xK2_4 * (-1.821255978);
    val xLocal_31 = Mul(xK2_4, Const(-1.821255978f))

//    xLocal_2 = xLocal_2 + xLocal_3;
    val xLocal_22 = Add(xLocal_21, xLocal_31)

//    xLocal_3 = xK2_5 * 1.330274429;
    val xLocal_32 = Mul(xK2_5, Const(1.330274429f))

//    xLocal_2 = xLocal_2 + xLocal_3;
    val xLocal_23 = Add(xLocal_22, xLocal_32)

//    xLocal_1 = xLocal_2 + xLocal_1;
    val xLocal_11 = Add(xLocal_23, xLocal_10)

//    xLocal   = xLocal_1 * xNPrimeofX;
//    xLocal   = 1.0 - xLocal;
    val xLocal0   = Mul(xLocal_11, xNPrimeofX)
    val xLocal   = Sub(Const(1.0f), xLocal0)

//    OutputX  = xLocal;
//    if (sign) {
//        OutputX = 1.0 - OutputX;
//    }
    val subFromOne = Sub(Const(1.0f), xLocal)
    val OutputX  = Mux(sign, subFromOne, xLocal)
    OutputX
  }

//fptype BlkSchlsEqEuroNoDiv( fptype sptprice,
//                            fptype strike, fptype rate, fptype volatility,
//                            fptype time, int otype, float timet )
  def BlkSchlsEqEuroNoDiv(sptprice: CombNode, strike: CombNode, rate: CombNode, volatility: CombNode, time: CombNode, otype: CombNode) = {

//    xStockPrice = sptprice;
    val xStockPrice = sptprice

//    xStrikePrice = strike;
    val xStrikePrice = strike

//    xRiskFreeRate = rate;
    val xRiskFreeRate = rate

//    xVolatility = volatility;
    val xVolatility = volatility

//    xTime = time;
    val xTime = time;

//    xSqrtTime = sqrt(xTime);
    val xSqrtTime = Sqrt(xTime)

//    logValues = log( sptprice / strike );
    val div = Div(sptprice, strike)
    val logValues = Log(div)

//    xLogTerm = logValues;
    val xLogTerm = logValues;

//    xPowerTerm = xVolatility * xVolatility;
//    xPowerTerm = xPowerTerm * 0.5;
      val volSqr = Mul(xVolatility, xVolatility)
      val xPowerTerm = Mul(volSqr, Const(0.5f))

//    xD1 = xRiskFreeRate + xPowerTerm;
//    xD1 = xD1 * xTime;
//    xD1 = xD1 + xLogTerm;
      val xD10 = Add(xRiskFreeRate, xPowerTerm)
      val xD11 = Mul(xD10, xTime)
      val xD12 = Add(xD11, xLogTerm)

//    xDen = xVolatility * xSqrtTime;
      val xDen = Mul(xVolatility, xSqrtTime)

//    xD1 = xD1 / xDen;
//    xD1 = xD1 / xDen;
      val xD13 = Div(xD12, xDen)
      val xD14 = Div(xD13, xDen)

//    xD2 = xD1 -  xDen;
      val xD2 = Sub(xD14, xDen)

//    d1 = xD1;
      val d1 = xD14

//    d2 = xD2;
      val d2 = xD2

//    NofXd1 = CNDF( d1 );
      val NofXd1 = CNDF(d1)

//    NofXd2 = CNDF( d2 );
      val NofXd2 = CNDF(d2)

//    FutureValueX = strike * ( exp( -(rate)*(time) ) );
      val rateTime = Mul(rate, time)
      val negRateTime = Sub(Const(0.0f), rateTime)
      val expRateTime = Exp(negRateTime)
      val FutureValueX = Mul(strike, expRateTime)

//    if (otype == 0) {
//        OptionPrice = (sptprice * NofXd1) - (FutureValueX * NofXd2);
//    } else {
//        NegNofXd1 = (1.0 - NofXd1);
//        NegNofXd2 = (1.0 - NofXd2);
//        OptionPrice = (FutureValueX * NegNofXd2) - (sptprice * NegNofXd1);
//    }
      val mul1t = Mul(sptprice, NofXd1)
      val mul2t = Mul(FutureValueX, NofXd2)
      val OptionPrice1 = Sub(mul1t, mul2t)

      val NegNofXd1 = Sub(Const(1.0f), NofXd1)
      val NegNofXd2 = Sub(Const(1.0f), NofXd2)
      val mul3t = Mul(FutureValueX, NegNofXd2)
      val mul4t = Mul(sptprice, NegNofXd1)
      val OptionPrice2 = Sub(mul3t, mul4t)
      val cmp = Eq(otype, Const(0))
      val OptionPrice = Mux(cmp, OptionPrice2, OptionPrice1)

//    return OptionPrice;
      OptionPrice
  }


  def main(args: String*) = {
    if (args.size != 4) {
      println("\nUsage: BlackScholes <#tileSize> <#opar> <#ipar> <metapipe>")
      sys.exit(0)
    }
    tileSize = args(0).toInt
    outerPar = args(1).toInt
    innerPar = args(2).toInt
    shouldMetapipeline = args(3) == "true"

    val numOptions = ArgIn("numOptions"); bound(numOptions, 10000032)

    // Inputs
    val otype = OffChipArray(FixPt())("otype", numOptions)
    val sptprice = OffChipArray(FloatPt())("sptprice", numOptions)
    val strike = OffChipArray(FloatPt())("strike", numOptions)
    val rate = OffChipArray(FloatPt())("rate", numOptions)
    val volatility = OffChipArray(FloatPt())("volatility", numOptions)
    val otime = OffChipArray(FloatPt())("otime", numOptions)
    val optprice = OffChipArray(FloatPt())("optprice", numOptions)

    val seqCtr = Ctr(List(outerPar))((numOptions,tileSize))

    val s1 = replicate(outerPar){ i =>
      val otypeRAM = BRAM(FixPt(), tileSize, shouldMetapipeline)
      val sptpriceRAM = BRAM(FloatPt(), tileSize, shouldMetapipeline)
      val strikeRAM = BRAM(FloatPt(), tileSize, shouldMetapipeline)
      val rateRAM = BRAM(FloatPt(), tileSize, shouldMetapipeline)
      val volatilityRAM = BRAM(FloatPt(), tileSize, shouldMetapipeline)
      val otimeRAM = BRAM(FloatPt(), tileSize, shouldMetapipeline)

      val otypeLd = TileMemLd(innerPar)(otype, numOptions, Const(0), seqCtr(0)(i), otypeRAM, 1, tileSize)
      val sptpriceLd = TileMemLd(innerPar)(sptprice, numOptions, Const(0), seqCtr(0)(i), sptpriceRAM, 1, tileSize)
      val strikeLd = TileMemLd(innerPar)(strike, numOptions, Const(0), seqCtr(0)(i), strikeRAM, 1, tileSize)
      val rateLd = TileMemLd(innerPar)(rate, numOptions, Const(0), seqCtr(0)(i), rateRAM, 1, tileSize)
      val volatilityLd = TileMemLd(innerPar)(volatility, numOptions, Const(0), seqCtr(0)(i), volatilityRAM, 1, tileSize)
      val otimeLd = TileMemLd(innerPar)(otime, numOptions, Const(0), seqCtr(0)(i), otimeRAM, 1, tileSize)

      val dataLd = Parallel(otypeLd, sptpriceLd, strikeLd, rateLd, volatilityLd, otimeLd)

      (dataLd, List(otypeRAM,sptpriceRAM,strikeRAM,rateRAM,volatilityRAM,otimeRAM))
    }

    val otypeRAM = s1._2.map{l => l(0) }
    val sptpriceRAM = s1._2.map{l => l(1) }
    val strikeRAM = s1._2.map{l => l(2) }
    val rateRAM = s1._2.map{l => l(3) }
    val volatilityRAM = s1._2.map{l => l(4) }
    val otimeRAM = s1._2.map{l => l(5) }

    val s2 = replicate(outerPar){j =>
      // 2. Call Blackscholes
      val optpriceRAM = BRAM(FloatPt(), tileSize, shouldMetapipeline)
      val bsCtr = Ctr(List(innerPar))((Const(tileSize), 1))
      val bsPipe = Pipe(bsCtr,
        Block {
          val i = bsCtr(0)
          val sptprice_d = Ld(sptpriceRAM(j), i)
          val strike_d = Ld(strikeRAM(j), i)
          val rate_d = Ld(rateRAM(j), i)
          val volatility_d = Ld(volatilityRAM(j), i)
          val otime_d = Ld(otimeRAM(j), i)
          val otype_d = Ld(otypeRAM(j), i)
          val optPrice = BlkSchlsEqEuroNoDiv(sptprice_d, strike_d, rate_d, volatility_d, otime_d, otype_d)
          val st = St(optpriceRAM, i, optPrice)
        }
      )
      (bsPipe, List(optpriceRAM))
    }

    val optpriceRAM = s2._2.map{l => l(0) }

    // 3. Store optpriceRAM out
    val s3 = replicate(outerPar){i =>
      val optpriceSt = TileMemSt(innerPar, optprice, numOptions, Const(0), seqCtr(0)(i), optpriceRAM(i), 1, tileSize)
      (optpriceSt, Nil)
    }

    // Wire (1) and (2) in a Sequential controller
    if (shouldMetapipeline) MetaPipeline(seqCtr, s1._1, s2._1, s3._1)
    else                      Sequential(seqCtr, s1._1, s2._1, s3._1)
  }
}

object BlackScholesDSE extends DSE {
  val name = "BlackScholes"
  val makeDesign = () => new BlackScholesDesign{}
  val TileSize = Discrete("TileSize", 96 to 19200 by 96)
  val OuterPar = Discrete("OuterPar", 1 to 1)
  val InnerPar = Discrete("InnerPar", 1 to 10)
  val Metapipe = Category("Metapipe", true, false)
}


