import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}
import java.io._
import scala.io.Source


object MultiDotProduct extends MultiDotProductDesign
trait MultiDotProductDesign extends DesignTest {

  val T = Flt()
  var tileSize = 96
  var outerPar = 1
  var innerPar = 1
  var numVecs = 1
  var shouldMetapipeline = true

  def main(args: String*) = {
    if (args.size != 5) {
      println("\nUsage: MultiDotProduct <#tileSize> <#opar> <#ipar> <#vecs> <metapipeline?>")
      sys.exit(0)
    }
    tileSize = args(0).toInt
    outerPar = args(1).toInt
    innerPar = args(2).toInt
    numVecs = args(3).toInt
    shouldMetapipeline = args(4) == "true"

    // Off-chip memory input
    val C = ArgIn("C"); bound(C, 9993600)

    val vec1 = OffChipArray(T)("vec1", C)
    val vec2 = OffChipArray(T)("vec2", C)
    val result = OffChipArray(T)("result", Const(numVecs))

    val seqCtr = Ctr(List(outerPar))((Const(1),1))
    val resultsTile = BRAM(T, numVecs, shouldMetapipeline)
    // 1. Load tiles from off-chip memory
    val s1 = replicate(outerPar) { i =>
      val tile1 = BRAM(T, tileSize*numVecs, shouldMetapipeline)
      val tile2 = BRAM(T, tileSize*numVecs, shouldMetapipeline)

      val tile1Ld = TileMemLd(innerPar)(vec1, Const(tileSize), Const(0), seqCtr(0)(i), tile1, 1, tileSize*numVecs)
      val tile2Ld = TileMemLd(innerPar)(vec2, Const(tileSize), Const(0), seqCtr(0)(i), tile2, 1, tileSize*numVecs)
      tile1Ld.withForce(Const(true))
      tile2Ld.withForce(Const(true))
      val offChipLoad = Parallel(tile1Ld,tile2Ld)
      (offChipLoad, List(tile1,tile2))
    }

    val vecCtr = Ctr(List(1))((Const(numVecs),1))
    val s2 = replicates(1) {pid =>
      val accum = Reg(T)
      val innerCtr = Ctr(List(innerPar))((Const(tileSize),1))
      val addr = Add(Mul(vecCtr(0), Const(tileSize)), innerCtr(0))
      val m1 = Ld(s1._2(0)(0), addr)
      val m2 = Ld(s1._2(0)(1), addr)
      val prod = Mul(m1,m2)
      val sum = Add(accum, prod)
      accum.write(sum)
      val s2Pipe = Pipe(innerCtr, GraphNode(addr, m1, m2, prod), ReduceTree(prod, accum, sum))
      accum.setParent(s2Pipe)

      (s2Pipe, List(accum))
    }

    val s3 = replicate(1) {pid =>
      val st = St(resultsTile, vecCtr(0), s2._2(0)(0), 1, 1)
      val storePipe = Pipe(st)
      (storePipe, List())
    }

    val dot = Sequential(vecCtr, s2._1, s3._1)


    // Wire (1) and (2) in a Sequential controller
    val top = if (shouldMetapipeline) MetaPipeline("awesome", seqCtr, s1._1, dot) else Sequential(seqCtr, s1._1, dot)
    val tileSt = TileMemSt(result, Const(numVecs), Const(0), Const(0), resultsTile, 1, numVecs)
    tileSt.withForce(Const(true))
    val last = List(top) ++ List(tileSt)
    val topCtr = Ctr((Const(1),1))
    val topCtrl = Sequential(topCtr, last:_*)

    topCtrl
  }

  def test(args: String*) {
    val tileSize = args(0).toInt
    val numVecs = args(3).toInt
    val C = tileSize*numVecs

    setArg("C",C)
    val vec1 = Array.tabulate(C){i => (util.Random.nextDouble * 1000) / 1000}
    val vec2 = Array.tabulate(C){i => (util.Random.nextDouble * 1000) / 1000}

    setMem("vec1", vec1)
    setMem("vec2", vec2)

    WriteData(vec1, "vec1")
    WriteData(vec2, "vec2")

    run()

    val outGold = vec1.zip(vec2).map{case (a,b) => a*b}.sum
    val out = getMem[Double]("result", 0 until numVecs)

    // compare(out, outGold)
  }

  def WriteData(data:Array[Double], tag:String) = {
    val dir = System.getProperty("user.dir") + "/"
    val f = new File(dir + "data" + tag )
    println("Writing test data to " + dir + "data" + tag)
    if (f.exists) {f.delete()}
    val pw = new PrintWriter(f)
    for (k <- 0 to data.length-1) {
      if (k == data.length-1) {
        pw.write(data(k).toString)
      }
      else {
        pw.write(data(k) + "\n")
      }
    }
    pw.close
  }

}



object MultiDotProductDSE extends DSE {
  val name = "MultiDotProduct"
  val makeDesign = () => new MultiDotProductDesign{}

  val TileSize = Discrete("TileSize", 96 to 19200 by 96)
  val OuterPar = Discrete("OuterPar", 12 to 24)
  val InnerPar = Discrete("InnerPar", 1 to 19200)
  val Metapipe = Category("Metapipe", true, false)

  (InnerPar <= TileSize) and (InnerPar divides TileSize)
}