import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}

object DotProduct extends DotProductDesign
trait DotProductDesign extends DesignTest {

  val T = Flt()
  var tileSize = 96
  var outerPar = 1
  var innerPar = 1
  var shouldMetapipeline = true

  def main(args: String*) = {
    if (args.size != 4) {
      println("\nUsage: DotProduct <#tileSize> <#opar> <#ipar> <metapipeline?>")
      sys.exit(0)
    }
    tileSize = args(0).toInt
    outerPar = args(1).toInt
    innerPar = args(2).toInt
    shouldMetapipeline = args(3) == "true"

    // Off-chip memory input
    val C = ArgIn("C"); bound(C, 9993600)

    val vec1 = OffChipArray(T)("vec1", C)
    val vec2 = OffChipArray(T)("vec2", C)

    val seqCtr = Ctr(List(outerPar))((C,tileSize))

    // 1. Load tiles from off-chip memory
    val s1 = replicate(outerPar) { i =>
      val tile1 = BRAM(T, tileSize, shouldMetapipeline)
      val tile2 = BRAM(T, tileSize, shouldMetapipeline)

      val tile1Ld = TileMemLd(innerPar)(vec1, C, Const(0), seqCtr(0)(i), tile1, 1, tileSize)
      val tile2Ld = TileMemLd(innerPar)(vec2, C, Const(0), seqCtr(0)(i), tile2, 1, tileSize)
      val offChipLoad = Parallel(tile1Ld,tile2Ld)
      (offChipLoad, List(tile1,tile2))
    }

    val x = s1._2.map {l => l(0) }
    val y = s1._2.map {l => l(1) }

    // 2. Perform dot product and collect result into a Register, assign register output to ArgOut
    val accum = Reg(T)
    val ctr = Ctr(List(innerPar))((Const(tileSize), 1))
    val s2 = reducePipe(outerPar, ctr, accum) { i =>
      val v1 = Ld(x(i), ctr(0))
      val v2 = Ld(y(i), ctr(0))
      Mul(v1, v2)
    }{(a,b) => Add(a,b) }

    val out = ArgOut("out", accum)

    // Wire (1) and (2) in a Sequential controller
    val top = if (shouldMetapipeline) MetaPipeline("SuperAwesomeMetaPipelineTeamGo", seqCtr, s1._1, s2) else Sequential(seqCtr, s1._1, s2)
    accum.setParent(top) // Reset accum when top is done
    out.setParent(top)   // Capture value of out when top is done
    top
  }

  def test(args: String*) {
    val tileSize = args(0).toInt
    val C = 960

    setArg("C",C)
    val vec1 = Array.tabulate(C){i => util.Random.nextDouble * 100}
    val vec2 = Array.tabulate(C){i => util.Random.nextDouble * 100}

    setMem("vec1", vec1)
    setMem("vec2", vec2)

    run()

    val outGold = vec1.zip(vec2).map{case (a,b) => a*b}.sum
    val out = getArg[Double]("out")

    compare(out, outGold)
  }
}


object DotProductDSE extends DSE {
  val name = "DotProduct"
  val makeDesign = () => new DotProductDesign{}

  val TileSize = Discrete("TileSize", 96 to 19200 by 96)
  val OuterPar = Discrete("OuterPar", 12 to 24)
  val InnerPar = Discrete("InnerPar", 1 to 19200)
  val Metapipe = Category("Metapipe", true, false)

  (InnerPar <= TileSize) and (InnerPar divides TileSize)
}
