
import dhdl.graph._
import dhdl.codegen._
import dhdl.DesignTest
import dhdl.Design
import scala.math._

object CycleTest extends DesignTest {
  def main(args: String*) = {
    val x = ArgIn("x")
    val i = ArgIn("i")

    val iMin = Reg(UInt(), false)
    val xMin = Reg(UInt(), false, Some(Const(1000)))

    val less = Lt(x, xMin)
    val xMux = Mux(less, x, xMin)
    xMin.write(xMux)
    val iMux = Mux(less, i, iMin)
    iMin.write(iMux)
    val pipe = Pipe(less, xMux, iMux)
    iMin.setParent(pipe)
    xMin.setParent(pipe)
    pipe
  }

  def test(args: String*) {
    setArg("x", 10)
    setArg("i", 5)
    run()
  }
}