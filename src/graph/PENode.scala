package dhdl.graph

import scala.collection.mutable.Set
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import scala.math.max
import dhdl.transform.MPRewriteTransformer
import dhdl.analysis.MPDblBufAnalysis
import dhdl.Design


/** Base class for all PE nodes. This class contains
  * implementations of common methods that every DHDL node
  * can use and override
  */
abstract class PENode(implicit val design: Design) extends Node

case class PE(inputs: PENode*)(implicit design: Design) extends PENode {
  override val name = s"pe_${design.nextId}"
}
