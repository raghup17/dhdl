package dhdl
import dhdl.graph.ArgIn
import sys.process._
import java.io._
import java.nio.file.{Files,Paths};
import codegen.dot._
import codegen.maxj._
import codegen.scalagen._
import scala.collection.immutable.StringOps
import scala.collection.immutable.Range
import scala.reflect.ClassTag
import scala.io.Source

trait Parsable[T] {
  def parse(x: String): T
}

trait DesignTest extends Design {

  // To be implemented by user (can do nothing if desired)
  def test(args: String*): Unit

  private var testIn: Option[InputStream] = None
  private var testErr: Option[InputStream] = None
  private var testOut: Option[OutputStream] = None

  private lazy val reader: BufferedReader = new BufferedReader(new InputStreamReader(testIn.get))
  private lazy val writer: BufferedWriter = new BufferedWriter(new OutputStreamWriter(testOut.get))
  private lazy val logger: BufferedReader = new BufferedReader(new InputStreamReader(testErr.get))

  object CMD extends Enumeration { val NONE, GET, SET, ACK, ERR = Value }

  /*
    Valid Output Commands:
    get register contents:                GET REG_ID
    get memory contents at INDEX:         GET MEM_ID INDEX
    set register contents to DATA:        SET REG_ID DATA
    set memory contents at INDEX to DATA: SET MEM_ID INDEX DATA
    begin next phase:                     ACK
    terminate early:                      ERR
    no action:                            NONE

    Tokens are separated by newline characters
  */

  // Implicit conversions from string to numeric types
  implicit object ParseDbl extends Parsable[Double]{ def parse(x:String) = x.toDouble }
  implicit object ParseInt extends Parsable[Int]{ def parse(x:String) = x.toInt }
  implicit object ParseLng extends Parsable[Long]{ def parse(x:String) = x.toLong }
  implicit object ParseBln extends Parsable[Boolean]{ def parse(x:String) = x.toBoolean }

  private def openTest() = {
    val make = Process("make", new File("./out/scala")).run()
    val makeResult = make.exitValue
    if (makeResult != 0) {
      println("Failed to compile Scala testbench code")
      sys.exit(makeResult)
    }
    val processBuilder = Process("sbt run", new File("./out/scala"))
    val pio = new ProcessIO(in => testOut = Some(in), out => testIn = Some(out), err => testErr = Some(err))
    val process = processBuilder.run(pio)
    var waited = 0
    print("[DHDL] Waiting for subprocess streams to open...")
    while (testIn.isEmpty || testOut.isEmpty || testErr.isEmpty) {
      Thread.sleep(100)
      if (waited > 30 && waited % 10 == 0) { print(".") }
      waited += 1
    }
    println("Ready!")
    println("[DHDL] Waiting for ACK from subprocess...")
    // HACK: sbt run outputs some stuff to stdout upon startup that we need to ignore
    var response = CMD.NONE
    while (response != CMD.ACK) {
      val resp = readln()
      try { response = CMD(resp.toInt) }
      catch {case _: Throwable => response = CMD.NONE }
      if (response != CMD.ACK && resp != null)
        println(resp)
      else if (resp == null)
        sys.error("Subprocess terminated unexpectedly")
    }
    //reader.skip(Long.MaxValue)
    println("Ready!")

    println("[DHDL] Starting test")
    process
  }

  var unsetArgs: Set[String] = Set.empty
  var wasRun = false

  def run() {
    if (!unsetArgs.isEmpty) {
      println("[DHDL] Warning: Some input arguments were not set: " + unsetArgs.mkString("(",", ",")"))
    }
    sendCmd(CMD.ACK)
    wasRun = true
  }

  override def main(args: Array[String]) = {
    var top = main(args:_*)

    val transformedTop = processTop(top)

    if (Config.genDot) {
      val dot = new GraphvizCodegen()
      dot.run(transformedTop)
    }
    if (Config.genScala) {
      val scal = new ScalaCodegen()
      scal.run(transformedTop)
    }
    if (Config.genMaxJ) {
      val maxj = new MaxJCodegen()
      maxj.run(transformedTop)
    }
    if (Config.test) {
      unsetArgs ++= allNodes.flatMap{case a: ArgIn => Some(a.id); case _ => None}

      val process = openTest()
      test(args:_*)
      sendCmd(CMD.ACK)
      if (!wasRun)     println("[DHDL] Warning: Architecture was not run. Use command run() in the test function after setting memories.")
      else if (passed) println("Test passed!")
      else             println("Test failed.")

      process.destroy()
    }
  }

  private def readln() = { reader.readLine }

  private def writeln(str: String) {
    writer.write(str)
    writer.newLine
    writer.flush
  }
  private def sendCmd(cmd: CMD.Value) { writeln(cmd.id.toString) }

  private def flushError(): Nothing = {
    Thread.sleep(100) // Hack: Wait for rest of error from subprocess, if any
    while (reader.ready) { println(readln) }
    while (logger.ready) { println(logger.readLine) }
    sys.exit(-1)
  }

  private def parse[T:Parsable](line: String): T = {
    try {
      implicitly[Parsable[T]].parse(line)
    }
    catch { case _: Throwable =>
      println(line)
      flushError
    }
  }

  private def getCmdResponse() = {
    val resp = readln()
    try { CMD(resp.toInt) }
    catch { case _: Throwable => // This is almost definitely an exception in the subprocess
      println(resp)
      flushError
    }
  }

  ////////////
  // Test API

  // --- Memory communication

  /**
   * Get an element of type T from BRAM or off-chip memory at index i
   * @param T  - Type of data element
   * @param id - Name used to create memory in DHDL specification
   * @param i  - Memory address (note that it is always one dimensional)
   *
   * Example:
   *    DHDL: val x = OffChipArray("vec0", M, N)  // Create "vec0" as MxN off-chip memory
   *    ...
   *    Test: val data = getMem("vec0", 0)        // Get data at flat address 0
   */
  def getMem[T:Manifest:Parsable](id: String, i: Int): T = {
    sendCmd(CMD.GET)
    writeln(id.toString)
    writeln(i.toString)
    val result = getCmdResponse()
    result match {
      case CMD.NONE => throw new Exception(s"ERROR: No response for mem get $id at address $i")
      case CMD.ERR => throw new Exception(readln)
      case CMD.ACK => parse[T](readln)
    }
  }

  /**
   * Get array of data over a given range from a BRAM or off-chip memory
   * @param T   - Type of data elements
   * @param id  - Name used to create memory in DHDL specification
   * @param rng - Flat index range over which to collect elements
   *
   * Example:
   *    DHDL: val x = OffChipArray("vec0", M, N)
   *    ...
   *    Test: val xData = getMem("vec0", 0 until 100) // Get data elements at addresses [0,99]
   */
  def getMem[T:ClassTag:Manifest:Parsable](id: String, rng: Range): Array[T] = {
    val arr = new Array[T](rng.length)
    var indx = 0
    for (i <- rng) {
      arr(indx) = getMem(id, i)
      indx += 1
    }
    (arr)
  }

  /**
   * Set a single element in a BRAM or off-chip memory at address i to a given value
   * @param id   - Name used to create memory in DHDL specification
   * @param i    - Flat address of memory element to set
   * @param data - Value to put into the memory
   */
  def setMem[T:Manifest](id: String, i: Int, data: T) {
    if (!unsetArgs.isEmpty) sys.error("Cannot set memory before all arguments have been set.")
    sendCmd(CMD.SET)
    writeln(id.toString)
    writeln(i.toString)
    writeln(data.toString)
    getCmdResponse() match {
      case CMD.NONE => throw new Exception(s"ERROR: No response for mem set $id at address $i")
      case CMD.ERR => throw new Exception(readln)
      case CMD.ACK => // Success!
    }
  }

  /**
   * Set elements in a BRAM or off-chip memory over an optional given range from an array
   * @param id   - Name used to create memory in DHDL specification
   * @param data - Array of elements to copy into memory
   * @param rng  - Optional set of flat addresses into memory (uses 0 until data.length otherwise)
   */
  def setMem[T:Manifest](id: String, data: Array[T], rng: Option[Range] = None): Unit = {
    val range = rng.getOrElse(0 until data.length)
    var indx = 0
    for (i <- range) {
      setMem(id, i, data(indx))
      indx += 1
    }
  }
  def setMem[T:Manifest](id: String, data: Array[T], rng: Range): Unit = setMem(id,data,Some(rng))

  // --- Register communication

  /**
   * Set register to a given value
   * @param id   - Name used to create register in DHDL specification
   * @param data - Value to set register to
   */
  def setReg[T:Manifest](id: String, data: T) {
    if (unsetArgs.contains(id)) unsetArgs -= id
    sendCmd(CMD.SET)
    writeln(id.toString)
    writeln(data.toString)
    getCmdResponse() match {
      case CMD.NONE => throw new Exception(s"ERROR: No response for reg set $id")
      case CMD.ERR => throw new Exception(readln)
      case CMD.ACK => // Success!
    }
  }

  /**
   * Set an input argument to a given value
   * @param arg  - Argument number in DHDL specification
   * @param data - Value to set argument to
   */
  def setArg[T:Manifest](arg: String, data: T) { setReg(arg, data) }


  /**
   * Get the value of a register
   * @param T  - Type of value expected
   * @param id - Name used to create register in DHDL specification
   */
  def getReg[T:Manifest:Parsable](id: String): T = {
    sendCmd(CMD.GET)
    writeln(id.toString)
    getCmdResponse() match {
      case CMD.NONE => throw new Exception(s"ERROR: No response for reg get $id")
      case CMD.ERR => throw new Exception(readln)
      case CMD.ACK => parse[T](readln)
    }
  }

  /**
   * Get the value of an output argument
   * @param T   - Type of value expected
   * @param arg - Argument identifier in DHDL specification
   */
  def getArg[T:Manifest:Parsable](arg: String): T = getReg[T](arg)

  /**
   * Load a given file into a flat array
   * Multiple lines in the input file are flattened into a one dimensional array
   * @param path - Path to file (absolute or relative from run directory)
   * @param sv   - Optional separator (default is comma)
   */
  def load[T:ClassTag:Parsable](path: String, sv: String = ","): Array[T] = {
    if (!Files.exists(Paths.get(path)))
      throw new Exception(s"File $path not found")
    else {
      Source.fromFile(path).getLines.map(_.split(sv)).flatMap{i => i}.map{x=>implicitly[Parsable[T]].parse(x.trim)}.toArray
    }
  }

  /**
   * Save a flat array to a file
   * @param data - Flat array to save
   * @param path - Path to file (absolute or relative from run directory)
   * @param sv   - Optional separator (default is comma)
   */
  def save[T](data: Array[T], path: String, sv: String = ",") {
    val lf = new PrintWriter(path)
    lf.println(data.mkString(sv))
    lf.close
  }

  var passed = true

  def check[T](cond: Boolean): Boolean = { passed = passed && cond; cond }

  def compare[T](data: Array[T], gold: Array[T]) {
    assert(data.length >= gold.length, s"Output data is shorter than expected (${data.length} < ${gold.length})")
    for (i <- 0 until gold.length) {
      if (!check(data(i) == gold(i))) println(s"Output did not match reference at index $i (${data(i)} != ${gold(i)})")
    }
  }
  def compare[T:Parsable](data: T, gold: T) {
    if (!check(data == gold)) println(s"Output did not match reference ($data != $gold)")
  }
  def vcompare[T:Parsable](data: T, gold: T) {
    if (!check(data == gold)) println(s"Fail: Output did not match reference ($data != $gold)")
    else println(s"Pass ($data == $gold)")
  }

}