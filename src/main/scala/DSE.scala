package dhdl
import graph._
import analysis._

import scala.collection.mutable.ArrayBuffer
import java.io.{Console => _, _}
import sys.process._

trait DSE {
  val name: String
  val makeDesign: () => Design
  val verbose: Boolean = false

  abstract class Domain[T] {
    def apply(i: Int): Param[T]
    def length: Int
  }
  class Discrete(val rng: List[Int]) extends Domain[Int] {
    def apply(i: Int) = Nat(rng(i))
    def length = rng.length
  }
  class Category[T](val options: List[T]) extends Domain[T] {
    def apply(i: Int) = Cat(options(i))
    def length = options.length
  }

  object Discrete {
    def apply(id: String, lst: List[Int]) = {
      val disc = new Discrete(lst)
      space += id -> disc
      params ::= id
      Key(id)
    }
    def apply(id: String, rng: Range) = {
      val disc = new Discrete(rng.toList)
      space += id -> disc
      params ::= id
      Key(id)
    }
    def unapply(x: Key): Option[List[Int]] = space.get(x.id) match {
      case Some(x: Discrete) => Some(x.rng)
      case _ => None
    }
  }
  object Category {
    def apply[T](id: String, options: T*) = {
      val cat = new Category(options.toList)
      space += id -> cat
      params ::= id
      Key(id).asCategory
    }
    def unapply(x: Key): Option[List[_]] = space.get(x.id) match {
      case Some(x: Category[_]) => Some(x.options)
      case _ => None
    }
  }

  case class Key(id: String) {
    def < (that: Key) = LessThan(this, that)
    def <= (that: Key) = LessThanEqual(this, that)
    def divides (that: Key) = Divides(this, that)
    var numeric = true
    def asCategory = {numeric = false; this}
  }

  abstract class Param[T]
  case class Nat(x: Int) extends Param[Int] {
    def >(that: Nat) = this.x > that.x
    def <(that: Nat) = this.x < that.x
    def <=(that: Nat) = this.x <= that.x
    def divides(that: Nat) = that.x % this.x == 0
    override def toString = x.toString
  }
  case class Cat[T](x: T) extends Param[T] {
    override def toString = x.toString
  }

  case class Point(args: Array[Param[_]]) {
    def apply(id: String) = args(params.indexOf(id))
    def apply(i: Int) = args(i).toString
    def >(that: Point) = args.zip(that.args).forall {
      case (x: Nat, y: Nat) => x > y
      case (Cat(x),Cat(y)) => x == y
    }
    def toArray = args.map(_.toString)
    def mkString(del: String) = toArray.mkString(del)
    override def toString = "Point(" + toArray.mkString(", ") + ")"
  }
  object Point {
    def apply(n: Int)(func: Int => Param[_]) = new Point(Array.tabulate(n){i => func(i)})
  }

  abstract class Restrict { def and(that: Restrict) {} }
  case class LessThan(x: Key, y: Key) extends Restrict {
    assert(x.numeric && y.numeric, s"${x.id} and ${y.id} are not both numeric")
    restrict ::= this
  }
  case class LessThanEqual(x: Key, y: Key) extends Restrict {
    assert(x.numeric && y.numeric, s"${x.id} and ${y.id} are not both numeric")
    restrict ::= this
  }
  case class Divides(x: Key, y: Key) extends Restrict {
    assert(x.numeric && y.numeric, s"${x.id} and ${y.id} are not both numeric")
    restrict ::= this
  }

  var params: List[String] = Nil
  var space:  Map[String,Domain[_]] = Map.empty
  var restrict: List[Restrict] = Nil

  def compressSpace() {
    restrict.foreach{
      case r@LessThan(Discrete(x),Discrete(y)) =>
        val filt = x.filterNot(x => y.forall(y => x >= y))
        space += r.x.id -> new Discrete(filt)

      case r@LessThanEqual(Discrete(x),Discrete(y)) =>
        val filt = x.filterNot(x => y.forall(y => x > y))
        space += r.x.id -> new Discrete(filt)

      case r@Divides(Discrete(x),Discrete(y)) =>
        val filt = x.filterNot(x => y.forall(y => y % x != 0))
        space += r.x.id -> new Discrete(filt)
    }
  }
  def isLegal(args: Point): Boolean = restrict forall {
    case LessThan(Key(a),Key(b)) => args(a).asInstanceOf[Nat] < args(b).asInstanceOf[Nat]
    case LessThanEqual(Key(a),Key(b)) => args(a).asInstanceOf[Nat] <= args(b).asInstanceOf[Nat]
    case Divides(Key(a),Key(b)) => args(a).asInstanceOf[Nat] divides args(b).asInstanceOf[Nat]
  }

  def main(args: Array[String]) {
    println(s"Prior to initial pruning, space size is: ${params.map(p => space(p).length.toLong).fold(1L)(_*_)}")
    val pwd = new File(".").getAbsolutePath().dropRight(2)
    params = params.reverse
    val N = params.length
    compressSpace()

    val dims = params.map(p => space(p).length)
    val spaceSize = dims.map(_.toLong).fold(1L)(_*_)
    val prods = List.tabulate(N){i => dims.slice(i+1,N).fold(1)(_*_)}

    Config.genDot = false
    Config.genScala = false
    Config.genMaxJ = false
    Config.dse = true
    Config.quick = true

    //var legals = 0L
    println(s"Total space size is ${spaceSize}. Counting valid parameters...")
    //var i = 0L
    /*while (i < spaceSize) {

      if (isLegal(arg)) {
        legals += 1
        args_count.append(arg)
      }
      i += 1
    }*/
    val P = 8
    val size = if (spaceSize < Integer.MAX_VALUE) spaceSize.toInt else throw new Exception("TODO: Support big spaces...")
    var step = ((P + size)/P)

    var argsBuffer = Array.fill(P){ ArrayBuffer[Int]() }
    (0 until P).par.foreach{x =>
      val start = x * step
      val end = Math.min(start + step, size)
      val work = end - start

      for (i <- start until end) {
        val arg = Point(N){d => space(params(d)).apply( (i / prods(d)).toInt % dims(d) ) }
        if (isLegal(arg)) argsBuffer(x).append(i)
      }
    }
    val legalArgs = argsBuffer.map(_.length).sum
    println(s"Legal space size is ${legalArgs}.")

    //val args = args_count.toList
    //val argShuffle = util.Random.shuffle(args)

    //val alms = new Array[Int](legalCombs)
    //val regs = new Array[Int](legalCombs)
    //val dsps = new Array[Int](legalCombs)
    //val bram = new Array[Int](legalCombs)
    //val cycl = new Array[Double](legalCombs)
    //val valid = Array.tabulate(legalCombs){i => true}
    //val rout = new Array[Int](legalCombs)

    val args = util.Random.shuffle(argsBuffer.flatten.toList).take(200000)
    argsBuffer = null
    val legalSize = args.length
    val valids = Array.fill(P)(0)
    //val designs = new Array[Design](P)

    println("And aaaawaaaay we go!")

    val startTime = System.currentTimeMillis
    // Suppress output if not verbose
    step = ((P + legalSize)/P)

    (0 until P).par.foreach{x =>
      val start = x * step
      val end = Math.min(start + step, legalSize)
      val work = end - start

      val pw = new PrintStream(s"${pwd}/csv/${name}-${x}.csv")
      if (x == 0) pw.println(params.mkString(", ") + ", ALMs, Regs, DSPs, BRAM, Cycles, Valid")
      //val design = makeDesign()
      val lutRoutingModel = new LUTRoutingModel()
      val regFanoutModel = new RegFanoutModel()
      val unavailALMModel = new UnavailALMModel()
      //val bramModel = new BRAMDupModel()
      val design = makeDesign()
      design.lutRoutingModel = lutRoutingModel
      design.regFanoutModel = regFanoutModel
      design.unavailALMModel = unavailALMModel
      //design.bramModel = bramModel
      design.areaAnalysis = new AreaAnalysis(lutRoutingModel, regFanoutModel, unavailALMModel)(design)

      val cache = new Array[String](10000)
      var cache_counter = 0

      var nextNotify = 0.0; val notifyStep = 200
      for (i <- start until end) {
        val arg = Point(N){d => space(params(d)).apply( (args(i) / prods(d)).toInt % dims(d) ) }

        //val arg = argShuffle(i)
        design.main(arg.toArray)

        /*cycl(i) = design.cycles
        alms(i) = design.area.alms
        regs(i) = design.area.regs
        dsps(i) = design.area.dsps
        bram(i) = design.area.brams*/
        val valid = design.area.alms <= 262400 && design.cycles > 0 && design.area.dsps <= 1963 && design.area.brams <= 2567

        //pw.println(arg.mkString(", ") + s", ${design.area.alms}, ${design.area.regs}, ${design.area.dsps}, ${design.area.brams}, ${design.cycles}, ${valid}")
        cache(cache_counter) = arg.mkString(", ") + s", ${design.area.alms}, ${design.area.regs}, ${design.area.dsps}, ${design.area.brams}, ${design.cycles}, ${valid}"
        design.reset()
        if (valid) valids(x) = valids(x) + 1

        // Space pruning: mark all points stricter "larger" than this one as invalid as well
        // (Assumes area can only increase with increasing numeric parameters)
        /*if (!valid(i)) {
          for (j <- i+1 until end) {
            if (valid(j) && argShuffle(j) > arg) valid(j) = false
          }
        }*/

        cache_counter += 1
        if (cache_counter >= 10000 || i == end - 1) {
          for (c <- 0 until cache_counter) { pw.println(cache(c)) }
          cache_counter = 0
          pw.flush()
        }

        if ( (i - start) > nextNotify) {
          val time = System.currentTimeMillis - startTime
          System.out.println(s"#$x: " + "%.4f".format(100*(i-start)/work.toDouble) + s"% (${i-start} / $work) Complete after ${time/1000} seconds")
          nextNotify += notifyStep
          //pw.flush()
        }
      }
      pw.close()
    }
    val nValid = valids.sum

    val time = (System.currentTimeMillis - startTime)/1000.0
    println(s"Completed all trials ($nValid / $legalSize valid) in $time seconds")

    /*println(s"Finding pareto points...")
    //println("Bounds:    %.2f".format(designs.map(_.tBounds).sum /(10*time)))
    //println("Cycles:    %.2f".format(designs.map(_.tCycles).sum /(10*time)))
    //println("Area:      %.2f".format(designs.map(_.tArea).sum / (10*time)))

    // Detect Pareto optimal points
    val pareto = new Array[Boolean](legalCombs)
    for (i <- 0 until legalCombs) {
      var isPareto = valid(i)
      var j = if (i == 0) 1 else 0

      // Must have at least one column less for each other trial
      // More formally: "A point X* is said to be a Pareto optimal one
      // if there is no X such that Fi(X)<=Fi(X*) for all i = 1...n,
      // with at least one strict inequality."
      while (j < legalCombs && isPareto) {
        isPareto = !valid(j) || alms(i) < alms(j) || cycl(i) < cycl(j) ||
                                (alms(i) <= alms(j) && cycl(i) < cycl(j)) ||
                                (alms(i) < alms(j) && cycl(i) <= cycl(j)) //|| regs(i) < regs(j) || dsps(i) < dsps(j) || bram(i) < bram(j)
        j += 1
        if (j == i) j += 1
      }
      pareto(i) = isPareto
    }*/


    // Write to file
    //println("Done. Writing to file...")
    /*val pw = new PrintWriter(s"csv/$name.csv")

    for (i <- 0 until legalCombs)
      pw.println(argShuffle(i).mkString(", ") + s", ${alms(i)}, ${regs(i)}, ${dsps(i)}, ${bram(i)}, ${cycl(i)}, ${pareto(i)}, ${valid(i)}")

    pw.close()
    println("Complete.")*/

  }
}