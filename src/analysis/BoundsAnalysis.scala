package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.Design

class BoundsAnalysis(implicit design: Design) extends Traversal {

  object Bound {
    def unapply(x: Any): Option[Double] = x match {
      case Const(x: Int) => Some(x.toDouble)
      case Const(x: Double) => Some(x)
      case Const(x: Float) => Some(x.toDouble)
      case Const(x: Long) => Some(x.toDouble)
      case n: CombNode => design.nodeBounds.get(n)
      case _ => None
    }
  }

  def analyze(top: Node) { run(top) }

  def setBound(node: CombNode, bound: Double) { design.nodeBounds += node -> bound }

  override def visitNode(node: Node) : Unit = if (!visited.contains(node)) {
    visited += node
    node match {
      case n: Parallel =>
        n.nodes.foreach(visitNode)
      case n: MetaPipeline =>
        n.numIter.topSort.foreach(visitNode)
        n.nodes.foreach(visitNode)
      case n: Sequential =>
        n.numIter.topSort.foreach(visitNode)
        n.nodes.foreach(visitNode)
      case n: Pipe =>
        n.numIter.topSort.foreach(visitNode)
      case n@Add(Bound(x),Bound(y)) => setBound(n, x + y)
      case n@Mul(Bound(x),Bound(y)) => setBound(n, x * y)
      case n@Div(Bound(x),Bound(y)) => setBound(n, x.toDouble / y)
      case n@Sub(Bound(x),Bound(y)) => setBound(n, x - y)
      case _ => // No action
    }
  }
}