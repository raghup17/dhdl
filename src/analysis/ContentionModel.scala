package dhdl.analysis

/*import org.encog.Encog
import org.encog.engine.network.activation.ActivationSigmoid
import org.encog.ml.data.{MLData, MLDataPair, MLDataSet}
import org.encog.ml.data.basic.{BasicMLData, BasicMLDataSet}
import org.encog.neural.networks.BasicNetwork
import org.encog.neural.networks.layers.BasicLayer
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation
import org.encog.persist.EncogDirectoryPersistence

import scala.collection.JavaConverters._
import scala.io.Source
import java.io.File
import dhdl.Config*/

class ContentionModel {
  // c - contention (1 to 13) (number of readers/writers in parallel)
  // r - number of commands (>= 1)
  // b - number of words per command (step of 96)
  // TODO: Probably need to fit these better - these are best guess at the moment
  def evaluate(c: Int, r: Int, b: Int) = {
    val overhead12 = b match {
      case 96  => 0.307/(1 + Math.exp(-0.096*r + 0.21)) // Logistic, bounded by 30.7%
      case 192 => 0.185/(1 + Math.exp(-0.24*r - 0.8))    // Logistic, bounded by 18.5%
      case _ => 0.165
    }
    val overhead = ((1/Math.log(12))*Math.log(c))*overhead12
    //msg("Overhead = %.2f".format(overhead*100))

    Math.ceil( (1+overhead)*(110 + r*(53 + b)) )
  }
}


/*class ContentionModel {
  val name = "Contention"
  var needsInit = true
  var network: BasicNetwork = null
  val LAYER2 = 15
  val verbose = true

  val pwd = new File(".").getAbsolutePath().dropRight(2)

  def init() {
    val pwd = new File(".").getAbsolutePath().dropRight(2)
    val encogFile = s"${pwd}/data/${name}.eg"
    val exists = new File(encogFile).exists

    //println(s"File: $encogFile, exists: $exists")

    if (exists) {
      println("Loaded " + name + " model from file")
      network = EncogDirectoryPersistence.loadObject(new File(encogFile)).asInstanceOf[BasicNetwork]
    }
    else {
      val MODELS = if (Config.quick) 1 else 100

      val data = Source.fromFile(s"${pwd}/data/TileLoad.csv").getLines().toArray.drop(1).map(_.split(",").map(_.trim.toDouble))

      //Invert data
      val input = data.map(_.take(3).map(a => 1/a))
      val output = data.map(_.slice(3,4).map(a => 1/a))
      val trainingSet = new BasicMLDataSet(input, output)
      var iter = 0
      var minError = Double.PositiveInfinity
      var maxError = Double.PositiveInfinity
      while (iter < MODELS) {
        val (curNetwork, curError, curMax) = trainOne(trainingSet)
        if (curMax < maxError) {
          minError = curError
          maxError = curMax
          network = curNetwork
        }
        iter += 1
      }

      if (verbose) {
        for (pair <- trainingSet.asScala) {
          val output = network.compute(pair.getInput())
          val data = Array.tabulate(3){i => (1/pair.getInput().getData(i)).toInt }.mkString(", ")
          val cycles = 1/pair.getIdeal().getData(0)
          val estmt = 1/output.getData(0)
          val error = (cycles - estmt) / estmt
          println(data + s": output = $cycles, ideal = $cycles (error = ${100*error}%)")
        }
      }
      println(name + "\n-----------------")
      println("Neural network results:")
      println(s"Average error: ${100*minError/trainingSet.size}%")
      println(s"Maximum observed error: ${100*maxError}")

      EncogDirectoryPersistence.saveObject(new File(encogFile), network)
    }
  }

  def trainOne(trainingSet: BasicMLDataSet) = {
    val network = new BasicNetwork()
    network.addLayer(new BasicLayer(null,true,3))
    network.addLayer(new BasicLayer(new ActivationSigmoid(),true,LAYER2))
    network.addLayer(new BasicLayer(new ActivationSigmoid(),false,1))
    network.getStructure().finalizeStructure()
    network.reset()

    var epoch = 1
    val train = new ResilientPropagation(network, trainingSet)
    train.iteration()
    while (epoch < 600) {
      //println(s"Epoch #$epoch Error: ${100*train.getError()}")
      epoch += 1
      train.iteration()
    }
    train.finishTraining()
    //
    //println(s"Completed training at epoch $epoch with error of ${100*train.getError()}")


    var errors = 0.0
    var maxError = 0.0
    for (pair <- trainingSet.asScala) {
      val output = network.compute(pair.getInput())
      //val data = Array.tabulate(3){i => pair.getInput().getData(i) }.mkString(", ")
      val diff = output.getData(0) - pair.getIdeal().getData(0)
      val error = diff / pair.getIdeal().getData(0)
      //println(s"output = ${output.getData(0) * maxValues(RLUTS)}, ideal = ${pair.getIdeal().getData(0) * maxValues(RLUTS)} (error = ${100*error}%, true = ${100*trueError}%)")
      if (Math.abs(error) > maxError) maxError = Math.abs(error)
      errors += Math.abs(error)
    }

    (network, errors, maxError)
  }

  def evaluate(contention: Int, rows: Int, cols: Int) = {
    if (needsInit) init()
    val input = Array(1.0/contention, 1.0/rows, 1.0/cols)
    val output = network.compute(new BasicMLData(input))
    (1.0 / output.getData(0)).toInt
  }
}*/