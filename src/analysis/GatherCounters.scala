package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.Design

import scala.collection.mutable.ListBuffer
class GatherCounters(implicit design: Design) extends Traversal {

  val counters = ListBuffer[Ctr]()

  override def visitNode(node: Node) = {
    if (!visited.contains(node)) {
      visited += node
      node match {
        case n: Wire =>
          val parent = n.getParent()
          parent match {
            case c: Ctr =>
              counters.append(c)
            case _ =>
          }
        case _ =>
          super.visitNode(node)
      }
    }
  }
}
