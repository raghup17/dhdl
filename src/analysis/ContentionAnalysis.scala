package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.Design

// "I coulda had class. I coulda been a contender." - Terry, "On the Waterfront"

// Hacky way of deciding how many loads/stores are being done at the same time
class ContentionAnalysis(implicit design: Design) extends Traversal {

  // This model assumes absolutely everything happens in parallel,,

  def isolatedContention(x: CtrlNode): Int = {
    val contention = x match {
      case n: Parallel => n.nodes.map(isolatedContention).sum
      case n: MetaPipeline => n.nodes.map(isolatedContention).sum
      case n: Sequential => n.nodes.map(isolatedContention).max
      case n: TileMemLd => 1
      case n: TileMemSt => 1
      case n: Pipe => 0
      case n => throw new Exception(s"Don't know how to set contention for $n")
    }
    //ct += x -> contention
    contention
  }
  def markContention(x: CtrlNode, parent: Int): Unit = x match {
    case n: Parallel => n.nodes.foreach(n => markContention(n, parent))
    case n: MetaPipeline => n.nodes.foreach(n => markContention(n, parent))
    case n: Sequential => n.nodes.foreach(n => markContention(n, parent))
    case n: TileMemLd => n.contention = parent
    case n: TileMemSt => n.contention = parent
    case n: Pipe => ()
  }

  def analyze(top: Node) = top match {
    case top: CtrlNode =>
      val contention = isolatedContention(top)
      markContention(top, contention)
    case _ => // Nothing
  }
}
