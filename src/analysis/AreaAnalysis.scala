package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.Design
import dhdl.Config
import java.io.PrintWriter
import scala.collection.mutable.Set

class AreaAnalysis(lutRoutingModel: LUTRoutingModel, regFanoutModel: RegFanoutModel, unavailALMModel: UnavailALMModel)(implicit design: Design) extends Traversal {
  var areaScope: List[PrefitResources] = Nil
  override val debug = false

  // TODO: This will eventually be a function of number of memory channels used
  val BaseDesign = PrefitResources(
    lut7 = 468,
    lut6 = 9200,
    lut5 = 12350,
    lut4 = 11140,
    lut3 = 22600,
    mem64 = 4619,
    mem32 = 519,
    mem16 = 559,
    regs = 75400,
    dsps = 0,
    brams = 338
  )

  override def initPass() {
    super.initPass()
    areaScope ::= BaseDesign
  }

  def inScope(x: => Unit) = {
    val outerScope = areaScope
    areaScope = Nil
    x
    val area = areaScope.fold(NoArea){(a,b) => a + b}
    areaScope = outerScope
    (area)
  }

  def latencyOf(node: Node): Double = Model.latencyOf(node)
  def areaOf(node: Node) = Model.areaOf(node)

  // Mimic ASAP scheduling (delay after operation takes place)
  // Note that input arguments are not delayed by MaxJ, even relative to each other
  // All other graph inputs assumed to arrive at the same time
  def dfs_latency(sched: Set[Node], node: Node, ignore: Set[Node] = Set[Node]()): (Double, Double) = {
    if (sched.contains(node) && !node.deps.isEmpty && !ignore.contains(node)) {
      val deps = node.deps
      val regDlys = deps.map(dfs_latency(sched,_,ignore)) // Cycle latencies for each path to this node
      val regs = regDlys.map(_._1)
      val dlys = regDlys.map(_._2)
      val critical = dlys.max             // Length (in cycles) of critical path to this node
      val delayRegs = deps.zip(dlys).map {
        case (n: ArgIn, _) => 0     // Don't use delay registers for ArgIn or consts
        case (n: Const[_], _) => 0
        case (n: CombNode, dly) if !ignore.contains(n) => (critical - dly)*n.t.bits
        case _ => 0
      }.sum
      //msg(s"$node (deps = " + deps.mkString(", ") + s"): critical = $critical, regs = $delayRegs")

      (delayRegs + regs.sum, critical + latencyOf(node))
    }
    else if (sched.contains(node)) (0, latencyOf(node))
    else (0, 0)
  }

  def delayLine(regs: Double) = if (regs < 500) PrefitResources(regs = regs.toInt) else PrefitResources(brams = (Math.ceil(regs/16000.0)).toInt)

  override def visitNode(node: Node) { if (!visited.contains(node)) {
    visited += node
    val area = node match {
      case n: ReduceTree =>
        val allNodes = (Set[Node]() ++ n.graph.flatMap(_.nodes)) - n.accum

        val regDlys = n.accum.deps.map{out => dfs_latency(allNodes, out, Set(n.accum)) }
        val dlys = regDlys.map(_._2)
        val critical = dlys.max
        val delays = regDlys.map(_._1).sum + dlys.map{dly => (critical - dly)*n.accum.t.bits }.sum
        allNodes.foreach(x => visited += x)
        visited += n.accum
        //msg(s"Delay bits in $n: $delayRegs")

        val area = areaOf(n.accum) + allNodes.toList.map(areaOf).fold(NoArea){_+_} + delayLine(delays)
        //msg("Reduce tree area: " + area )
        area

      case n: GraphNode if n.isEmpty => NoArea
      case n: GraphNode =>
        n.nodes.foreach(x => dbg(s"$x: ${areaOf(x)}"))

        val delays = n.getouts.map{out => val (delayLines,dly) = dfs_latency(n.nodes,out); delayLines}.sum
        n.nodes.foreach(x => visited += x)
        dbg(s"Total delay bits for $n: $delays")

        val nodeSum = n.nodes.toList.map(areaOf).fold(NoArea){_+_}
        //msg(nodeSum)

        nodeSum + delayLine(delays)

      case n: Ctr if !n.isUnitCtr =>
        val maxes = inScope { n.max.foreach(visitNode) }
        maxes + areaOf(n)

      case n: Parallel =>
        val body = inScope { n.nodes.foreach(visitNode) }
        body + areaOf(n)

      case n: Stage =>
        val map = inScope { visitNode(n.mapNode) } //* (if (Config.quick) par else 1)
        val red = if (n.hasReduce && !Config.quick) {
          inScope { visitNode(n.reduceNode.get) }
        }
        else if (n.hasReduce && Config.quick) {
          val rn = n.reduceNode.get
          val allNodes = (Set[Node]() ++ rn.graph.flatMap(_.nodes)) - rn.accum

          val reduceArea = allNodes.toList.map(areaOf).fold(NoArea){_+_}
          var p = n.getParent.asInstanceOf[Pipe].ctr.par.reduce{_*_}
          var nNodes = 0
          var delays = 0
          while (p > 1) {
            nNodes = nNodes + p/2
            if (p % 2 == 0) p = p/2 else { p = p/2 + 1; delays += 1 }
          }
          val regDlys = rn.accum.deps.map{out => dfs_latency(allNodes, out, Set(rn.accum)) }
          val critical = regDlys.map(_._2).max
          //msg(s"$nNodes nodes in reduction tree from par of $par")
          //msg( (rn.accum.t.bits * delays * critical) + " delay registers")

          reduceArea * (nNodes + 1) + areaOf(rn.accum) + PrefitResources(regs = (rn.accum.t.bits * delays * critical).toInt)
        } else {
          NoArea
        }
        map + red

      case n: Pipe =>
        val par = n.ctr.par.reduce{_*_}


        val ctr = inScope { visitNode(n.ctr) }
        val stageAreas = n.stageList.map { s => inScope {visitNode(s)} }
        val totalStageArea = stageAreas.reduce {_+_}
        //msg("Map area: " + map)
        //msg("Reduce area: " + red)

        ctr + totalStageArea + areaOf(n)

      case n: MetaPipeline =>
        val ctr = inScope { visitNode(n.ctr) }
        val nIter = inScope { visitNode(n.numIter) }

        val chain = if (Config.quick) {
          val regs = n.size * n.ctr.ctrs.map(wire => wire.t.bits * wire.vecWidth).sum
          PrefitResources(lut3=regs, regs = 4*regs)
        }
        else {
          inScope { visitNode(n.regChain) }
        }
        dbg("n counter registers = " + chain)

        val body = inScope {
          n.nodes.foreach(visitNode)
          dbg(s"MetaPipeline ${n}:")
          areaScope.foreach(area => dbg("\t" + area))
        }
        dbg("\tIters: " + nIter)

        ctr + nIter + body + areaOf(n)

      case n: Sequential =>
        val ctr = inScope { visitNode(n.ctr) }
        val nIter = inScope { visitNode(n.numIter) }
        val body = inScope { n.nodes.foreach(visitNode) }
        ctr + nIter + body + areaOf(n)

      case _ =>
        super.visitNode(node)
        areaOf(node)
    }
    dbg(s"$node: $area")
    areaScope ::= area
  }}

  def analyze(top: Node) = {
    areaScope = Nil
    run(top)

    /*val extraBramCount = design.allNodes.toList.map{
      case n: Div => 10
      case n: Mul => 10
      case n: Sqrt => 150
      case n: Exp => 150
      case n: Log => 150
      case _ => 0
    }.sum*/

    //msg("Leftovers from transformers:")
    //design.allNodes.filter(n => n.stage != design.stage && !n.isInstanceOf[Const[_]]).foreach(n => msg(n.toString + " (" + n.stage + ")"))

    // HACK: We can miss resources that don't exist within a block
    //msg("Running clean up pass on all unvisited nodes...")
    //msg("Missed resources: ")
    //design.allNodes.filter(n => !visited(n) && (n.stage == design.stage || n.isInstanceOf[Const[_]])).foreach(n => msg(n.toString))

    val skipped = design.allNodes.filter(n => !visited(n) && n.stage == design.stage || n.isInstanceOf[Const[_]]).toList.map(areaOf).fold(NoArea){_+_}
    val total = areaScope.reduce{_+_} + skipped //+ extraBram

    // EXPERIMENTAL: Trained neural network on some basic designs, trying to model routing costs
    // TODO: Should model for unavailable be based on counts of each primitive instead?

    val routingLUTs = lutRoutingModel.evaluate(total)   // 21000
    val fanoutRegs = regFanoutModel.evaluate(total)     // 3600
    val unavailable = unavailALMModel.evaluate(total)   // 400
    //val dupBram = bramModel.evaluate(total)

    // Rough approximation of packing
    val recoverable = total.lut3/2 + total.lut4/2 + total.lut5/2 + total.lut6/10 + total.mem16/2  + routingLUTs/2

    val logicALMs = total.lut3 + total.lut4 + total.lut5 + total.lut6 + total.lut7 +
                    total.mem16 + total.mem32 + total.mem64 + routingLUTs - recoverable + unavailable
    val regs = total.regs + fanoutRegs

    val regALMs = Math.max( ((regs - (logicALMs*2.16))/3).toInt, 0)

    val totalALMs = logicALMs + regALMs

    val dupBRAMs = Math.max(0.02*routingLUTs - 500, 0.0).toInt

    val totalBRAM = total.brams + dupBRAMs

    if (Config.genMaxJ) {
      msg(s"Initial estimates before routing cost and ALM packing: ")
      msg("Estimated unavailable ALMs: " + unavailable)
      msg(s"LUT3: ${total.lut3}")
      msg(s"LUT4: ${total.lut4}")
      msg(s"LUT5: ${total.lut5}")
      msg(s"LUT6: ${total.lut6}")
      msg(s"LUT7: ${total.lut7}")
      msg("Estimated routing luts: " + routingLUTs)
      msg(s"MEM64: ${total.mem64}")
      msg(s"MEM32: ${total.mem32}")
      msg(s"MEM16: ${total.mem16}")
      msg(s"Implementation registers: ${total.regs}")
      msg("Estimated fanout regs: " + fanoutRegs)
      msg("Estimated used brams: " + total.brams)
      msg("Estimated fanout brams: " + dupBRAMs)

      msg("Logic+register ALMs: " + logicALMs)
      msg("Register-only ALMS: " + regALMs)
      msg(s"DSP: ${total.dsps}")
      msg(s"BRAM: " + totalBRAM)

      msg(s"Recovered: $recoverable")
      msg("")
    }

    Resources(totalALMs,regs,total.dsps,totalBRAM)
  }
}
