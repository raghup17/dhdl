package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.Design

import scala.collection.mutable.ListBuffer

class MPDblBufAnalysis(implicit design: Design) extends Traversal {


  def getTopNode(nodeList: List[CtrlNode]) = {
    // Early-out if nodeList only contains one node
    if (nodeList.size == 1) {
      nodeList(0)
    } else {
      // For each node, iterate through all its parent nodes until 'null' is reached
      // Check if parent exists in the list. If so, this node is not the topmost.
      // If parent doesn't exist in List, then it is the topmost node
      val topnode = nodeList.filter { n =>
        val parentList = ListBuffer[CtrlNode]()
        var tmp = n.getParent()
        while (tmp != null) {
          parentList.append(tmp)
          tmp = tmp.getParent()
        }
        println(s"""Parent list for $n = $parentList""")
        val isParentNotInList = parentList.map{ !nodeList.contains(_) }
        println(s"""isParentNotInList = $isParentNotInList""")
        isParentNotInList.reduce { _ & _ }
      }

      if (topnode.size > 1) throw new Exception(s"""
        Found >1 nodes (${topnode}) whose parent doesn't exist in the list!
        This is most likely caused because a double buffer is being addressed using multiple
        counters who are not part of the same design hierarchy. This form of addressing is
        currently not supported in DHDL""")

      topnode(0)
    }
  }

  override def visitNode(node: Node) = {
    if (!visited.contains(node)) {
      visited += node
      node match {
        case n: TileMemSt =>
          if (n.buf.isDblBuf) {
  //          println(s"""Setting writer of ${n.buf} to ${n}""")
            n.buf.addReader(n)
          }
        case n: TileMemLd =>
          n.buf.map { b =>
            if (b.isDblBuf) {
  //            println(s"""Adding reader $n to ${n.buf}""")
              b.setWriter(n)
            }
          }
        case n@St(mem, addr, data, start, stride) =>
          if (mem.isDblBuf) {
            assert(n.getParent() != null, s"""
              Parent of $n is null, cannot set reader to dblbuf $mem.
              Did you forget to add $n to GraphNode()?""")
            mem.setWriter(n.getParent())
          }
        case n@Ld(mem, addr, stride) =>
          if (mem.isDblBuf) {
            assert(n.getParent() != null, s"""
              Parent of $n is null, cannot set reader to dblbuf $mem.
              Did you forget to add $n to GraphNode()?""")
            val ctrAnalysis = new GatherCounters()
            ctrAnalysis.run(n)
            println(s"""[DblBufAnalysis] Mem in question: ${mem}""")
            println(s"""[DblBufAnalysis] ctrAnalysis counters: ${ctrAnalysis.counters}""")
            val readers = ctrAnalysis.counters.map { _.getParent() }.distinct.toList
            println(s"""[DblBufAnalysis] Readers: ${readers}""")
            val topnode = getTopNode(readers)
            println(s"""[DblBufAnalysis] readers for $mem: $readers, will take topmost ($topnode)""")
            println(s"""[DblBufAnalysis] Already existing readers: ${mem.getReaders()}""")
            mem.addReader(topnode)
          }
        case _ =>
          super.visitNode(node)
      }
    }
  }
}
