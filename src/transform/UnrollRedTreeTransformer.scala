package dhdl.transform
import dhdl.graph._
import dhdl.Design

import scala.collection.mutable.Set
import scala.collection.mutable.Queue
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer

/**
 *  ReduceTree unroller: This transformation unrolls a GraphNode
 *  in ReduceTree by an amount equal to the vector width of the data written
 *  into the accumulator register. The GraphNode to be replicated is added
 *  to the 'allowReps' set so that multiple calls to transformNode will return
 *  multiple copies of the GraphNode. Before each call to transformNode, the
 *  two input values (accum and oldval) are substituted with a new pair of CombNodes.
 *  A simple queue is used to keep track of substitution CombNodes so that the
 *  transformed graph naturally is connected as a tree.
 */
class UnrollRedTreeTransformer(implicit val design: Design) extends Transformer {
  override val name = "unrollRedTree"

  override def transformNode(node: Node) = {
    if (transformed.contains(node) && !allowReps.contains(node)) {
      transformed(node)
    } else {
      val tnode = node match {
        case r@ReduceTree(oldval, accum, graph @_*) =>
          // Make sure RedTree isn't already unrolled!
          assert(graph.size == 1, s"Reduction tree already unrolled! Number of contained GraphNodes: ${graph.size}")
          val body = graph(0)
          val par = oldval.vecWidth

          if (par == 1) {
            super.transformNode(node)
          } else {
            // Add body to a list of 'replicatable' nodes
            allowReps += body
            body.nodes.map { allowReps += _ }

            // Remember old substitutions - also means that oldval must have
            // been transformed by now.
            val prevOldvalSubst = transformed(oldval).asInstanceOf[CombNode]

            // Replicate, keep track of result node
            val resNodeMap = HashMap[GraphNode, CombNode]()
            val substQueue = Queue[CombNode]()
            (0 until par) map { i => substQueue += prevOldvalSubst(i) }

            val newgraphs = ListBuffer[GraphNode]()
            while(substQueue.size > 1) {
              val v1 = substQueue.dequeue
              val v2 = substQueue.dequeue
              transformed += oldval -> v1
              transformed += accum -> v2
              val t = transformNode(body).asInstanceOf[GraphNode]
              val resnode = transformed(accum.input).asInstanceOf[CombNode]
              substQueue += resnode
              resNodeMap += t -> resnode
              newgraphs.append(t)
            }

            val finalV1 = substQueue.dequeue
            val finalAccum = Reg(accum.t, accum.isDblBuf)
            finalAccum.setParent(accum.getParent)
            if (accum.hasReset) {
              finalAccum.reset = transformNode(accum.reset).asInstanceOf[GraphNode]
            }
            transformed += oldval -> finalV1
            transformed += accum -> finalAccum
            transformed += finalAccum -> finalAccum
            val t = transformNode(body).asInstanceOf[GraphNode]
            newgraphs.append(t)
            val resnode = transformed(accum.input).asInstanceOf[CombNode]
            finalAccum.write(resnode)

            // Remove body from replicatable list
            allowReps -= body
            body.nodes.map { allowReps -= _ }

            transformed -= oldval
            transformed += oldval -> prevOldvalSubst
            val toldval = transformNode(oldval).asInstanceOf[CombNode]
            val taccum = transformNode(accum).asInstanceOf[Reg]
            new ReduceTree(toldval, taccum, newgraphs:_*)
          }
        case _ =>
          super.transformNode(node)
      }

      // [HACK] This part is duplicated with the parent, which is bad design
      if (transformed.contains(node)) {
        design.archiveNode(transformed(node))
      }
      transformed += node -> tnode
      transformed += tnode -> tnode
      design.addNode(tnode)
      tnode
    }
  }

}
