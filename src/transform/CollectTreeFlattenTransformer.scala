package dhdl.transform
import dhdl.graph._
import dhdl.Design

import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer

class CollectTreeFlattenTransformer(implicit design: Design) extends Transformer {
  override val name = "CollectTreeFlatten"

  override def transformNode(node: Node) = {
    if (transformed.contains(node)) {
      transformed(node)
    } else {
      val tnode = node match {
        case m@MetaPipeline(ctr, regChain, numIter, nodes @ _*)  =>
          val tctr = transformNode(ctr).asInstanceOf[Ctr]

          val newnodes = nodes.map { s =>
            val newstage = transformNode(s).asInstanceOf[CtrlNode]
            newstage match {
              case CollectReduceTree(in, accum, body @_*) =>
                body.toList
              case _ =>
                List(newstage)
            }
          }
          val newnode = new {override val name = m.name} with MetaPipeline(tctr, regChain, numIter, newnodes.flatten:_*)
          newnode
        case _ =>
          super.transformNode(node)
      }

      // [HACK] This part is duplicated with the parent, which is bad design
      if (transformed.contains(node)) {
        design.archiveNode(transformed(node))
      }
      transformed += node -> tnode
      transformed += tnode -> tnode
      design.addNode(tnode)
      tnode
    }
  }
}
