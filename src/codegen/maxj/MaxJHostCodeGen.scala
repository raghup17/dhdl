package dhdl.codegen.maxj

import dhdl.codegen.Codegen
import dhdl.graph._
import dhdl.Design
import java.io.PrintWriter
import java.io.File
import scala.collection.mutable.Set
import scala.collection.mutable.HashMap

class MaxJHostCodeGen(path: String)(implicit design: Design) extends Codegen {

  override val ext = "c"
  val typeHdrFile = "types.h"
  val typeFilePath = path.split("/").dropRight(1).mkString("/") + "/" + typeHdrFile
  val cf: PrintWriter = new PrintWriter(path)
  val typef: PrintWriter = new PrintWriter(typeFilePath)
  val structEmittedSet = Set[CompositeType]()
  override def quote(n: Any) = {
    n match {
      case Const(x) => x.toString
      case _ => super.quote(n)
    }
  }
  def tpstr(t: Type): String = {
    t match {
      case ct@CompositeType(ts) =>
        val structName = "str_" + ts.map { tpstr(_) }.mkString("_")
        if (!structEmittedSet.contains(ct)) {
          structEmittedSet += ct
          val structBody = ts.zipWithIndex.map { t =>
            s"${tpstr(t._1)} _${t._2};"
          }
          fp(typef, s"""struct $structName {""")
          structBody.map { f => fp(typef, s"$f") }
          fp(typef, s"""};""")
          fp(typef, s"""typedef struct $structName $structName;""")
        }
        s"$structName"
      case FixPt(d, f, s) =>
        val signedPrefix = if (s) "" else "u"
        val body = (d+f) match {
          case x:Int if (x <= 16) =>
            "int16"
          case x:Int if (x >16 && x <= 32) =>
            "int32"
          case x:Int if (x>32) =>
            "int64"
        }
        s"${signedPrefix}${body}_t"
      case FloatPt(m, e) => if (m <= 24) s"float" else "double"
      case _ => throw new Exception(s"Unknown type $t")
    }
  }

  def cvtStr(t: Type) = {
    t match {
      case FixPt(d, f, s) => "atoi"
      case FloatPt(m, e) => "atof"
      case _ => throw new Exception(s"Unknown type $t")
    }
  }

  def formatStr(t: Type) = {
    t match {
      case FixPt(d, f, s) => "%d"
      case FloatPt(m, e) => tpstr(t) match {
        case "float" => "%f"
        case "double" => "%lf"
        case _ => throw new Exception(s"FloatPt($m, $e) mapped to unknown host type ${tpstr(t)}")
      }
      case _ => throw new Exception(s"Unknown type $t")
    }
  }

  def defaultVal(t: Type, zeroInit: Boolean): String = {
    t match {
      case CompositeType(ns) => s"(${tpstr(t)}) {${ns.map(defaultVal(_, zeroInit)).mkString(",")}}"
      case _  => s"(${tpstr(t)})" + (if (zeroInit) "0" else "i")
      case _ => throw new Exception(s"Unknown type $t")
    }
  }


  val headers = List(
    "stdlib.h",
    "stdio.h",
    "stdint.h",
    "sys/time.h",
    "Maxfiles.h",
    "MaxSLiCInterface.h",
    "types.h"
  )

  def emitIncludes() = {
    headers.map { h => fp(cf, s"""#include \"$h\"""") }
    fp(typef, s"""#include <stdint.h>""")
  }

  def helperMethods() = {
fp(cf, s"""
//uint32_t burstSize = 384;
//int32_t nextLMemAddr = burstSize * 1024;
//uint32_t getNextLMemAddr(uint32_t size) {
//  uint32_t addr = nextLMemAddr;
//  nextLMemAddr += size + (burstSize - size%burstSize);
//  return addr;
//}
""")
  }

  def mainPreamble(argIn: List[ArgIn], argOut: List[ArgOut]) = {
    val usageStr = argIn.sortBy(_.arg).map(a => s"<${quote(a)}>").mkString(" ")
fp(cf, s"""
float *parser(char* name, int size)
{
  float *arr = malloc(size);
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  long i = 0;
  ssize_t read;
  fp=fopen(name,"r");
  if(fp != NULL)
  {
    while (((read = getline(&line, &len, fp)) != -1) & (i < size/sizeof(float))) {
      arr[i] = atof(line);
      i = i + 1;
    }
    fclose(fp);
  }
  else {
    printf("ERROR: NO DATA");
    for (i=0; i<size; i++) {
      arr[i] = (float)1;
    }
  }
  return arr;
}

int main(int argc, char **argv)
{
uint32_t i=0, j=0;
struct timeval t1, t2;

if (argc != (${argIn.size}+1)) {
  fprintf(stderr, "Usage: %s ${usageStr}\\n\\n", argv[0]);
  exit(-1);
}
""")

    argIn.map { a =>
      fp(cf, s"""${tpstr(a.t)} ${quote(a)} = ${cvtStr(a.t)}(argv[${a.arg}]);""")
    }

    argOut.map { a =>
      // MaxJ honors all input types, but up-casts floats to doubles for output arguments
      // Let's do the same here
      val typeStr = tpstr(a.t) match {
        case "float" => "double"
        case _ => tpstr(a.t)
      }
      fp(cf, s"""$typeStr ${quote(a)} = ($typeStr) 0;""")
    }
      fp(cf, s"""uint64_t cycles = (uint64_t) 0;""")
  }


  // Current TileLd/St templates expect that LMem addresses are
  // statically known during graph build time in MaxJ. That can be
  // changed, but until then we have to assign LMem addresses
  // statically. Assigning each OffChipArray a 384MB chunk now
  val burstSize = 384
  var nextLMemAddr = burstSize * 1024 * 1024
  def getNextLMemAddr() = {
    val addr = nextLMemAddr
    nextLMemAddr += burstSize * 1024 * 1024;
    addr
  }

  val seenHostMem = Set[OffChipArray]()
  val offChipArrayAddr = HashMap[OffChipArray, Int]()
  def initHostMemory(mem: OffChipArray, zeroInit: Boolean = false) = {
    if (!seenHostMem.contains(mem)) {
      // Declare and allocate host array
      val tstr = tpstr(mem.t)
      val sizeStr = mem.sizes.map(s => s"${quote(s)}").mkString("*")
      fp(cf, s"""uint32_t ${quote(mem)}_size = $sizeStr;""")
      if (mem.toString contains "offchip_model") {
        fp(cf, s"""$tstr *${quote(mem)} = parser(\"/kunle/users/mattfel/dataM",${quote(mem)}_size * sizeof($tstr));""")
      }
      else if (mem.toString contains "offchip_train_x"){
        fp(cf, s"""$tstr *${quote(mem)} = parser(\"/kunle/users/mattfel/dataX",${quote(mem)}_size * sizeof($tstr));""")
      }
      else if (mem.toString contains "offchip_train_y"){
        fp(cf, s"""$tstr *${quote(mem)} = parser(\"/kunle/users/mattfel/dataY",${quote(mem)}_size * sizeof($tstr));""")
      }
      else if (mem.toString contains "offchip_train_x_T"){
        fp(cf, s"""$tstr *${quote(mem)} = parser(\"/kunle/users/mattfel/dataX_T",${quote(mem)}_size * sizeof($tstr));""")
      }
      else if (mem.toString contains "vec1"){
        fp(cf, s"""$tstr *${quote(mem)} = parser(\"/kunle/users/mattfel/datavec1",${quote(mem)}_size * sizeof($tstr));""")
      }
      else if (mem.toString contains "vec2"){
        fp(cf, s"""$tstr *${quote(mem)} = parser(\"/kunle/users/mattfel/datavec2",${quote(mem)}_size * sizeof($tstr));""")
      }
      else {
        fp(cf, s"""$tstr *${quote(mem)} = malloc(${quote(mem)}_size * sizeof($tstr));""")
        // For-loop to initialize
        fp(cf, s"""for (i=0; i<${quote(mem)}_size; i++) {""")
          fp(cf, s"""${quote(mem)}[i] = ${defaultVal(mem.t, zeroInit)};""")
        fp(cf,"}")
      }
      // Assign Lmem address - see comment above
      // The correct solution would be to use the emitted helper
      // method so that addresses are assigned at runtime based on the sizes
      // of the data structures.
      fp(cf, s"""const uint32_t ${quote(mem)}_lmem_addr = ${offChipArrayAddr(mem)};""")
      seenHostMem += mem
    }
  }

  def configureFPGA() = {
    fp(cf, s"""
// Load max file from disk -> DRAM
max_file_t *maxfile = Top_init();
// Configure the FPGA
max_engine_t *engine = max_load(maxfile, "local:*");
int burstSize = max_get_burst_size(maxfile, NULL);
printf("Burst size for architecture: %d bytes\\n", burstSize);
""")
  }

  def copyHostToFPGA(mem: OffChipArray) = {
    fp(cf, s"""
// Transfer DRAM -> LMEM
Top_writeLMem_actions_t ${quote(mem)}_wrAct;
${quote(mem)}_wrAct.param_size = ${quote(mem)}_size * sizeof(${tpstr(mem.t)});
${quote(mem)}_wrAct.param_start = ${quote(mem)}_lmem_addr;
${quote(mem)}_wrAct.instream_fromcpu = ${quote(mem)};
Top_writeLMem_run(engine, &${quote(mem)}_wrAct);
""")
  }

  def copyFPGAToHost(mem: OffChipArray) = {
    fp(cf, s"""
// Transfer LMEM -> DRAM
// (sizeInBytes, address, dstptr)
Top_readLMem_actions_t ${quote(mem)}_rdAct;
${quote(mem)}_rdAct.param_size = ${quote(mem)}_size *sizeof(${tpstr(mem.t)});
${quote(mem)}_rdAct.param_start = ${quote(mem)}_lmem_addr;
${quote(mem)}_rdAct.outstream_tocpu = ${quote(mem)};
fprintf(stderr, "Starting FPGA -> CPU copy\\n");
Top_readLMem_run(engine, &${quote(mem)}_rdAct);
fprintf(stderr, "FPGA -> CPU copy done\\n");
""")
  }

  def runFPGA(argIn: List[ArgIn], argOut: List[ArgOut]) = {
    fp(cf, s"""
// Run kernel on FPGA
fprintf(stderr, "Running Kernel\\n");
Top_actions_t runAct;
""")

  // TODO: This only handles scalarInputs, does not handle scalarOutputs from DFE yet
  // For scalarOutputs, need to pass pointer
  argIn.map(a => fp(cf,s"""runAct.param_${quote(a)} = ${quote(a)};"""))
  argOut.map(a => fp(cf,s"""runAct.outscalar_TopKernel_${quote(a)} = &${quote(a)};"""))
  fp(cf,s"""runAct.outscalar_TopKernel_cycles = &cycles;""")

  fp(cf, s"""
gettimeofday(&t1, 0);
Top_run(engine, &runAct);
gettimeofday(&t2, 0);
double elapsed = (t2.tv_sec-t1.tv_sec)*1000000 + t2.tv_usec-t1.tv_usec;
fprintf(stderr, "Kernel done, elapsed time = %lf\\n", elapsed/1000000);
""")
  }

  def dumpResults(mem: OffChipArray) = {
    fp(cf, s"""
fprintf(stderr, "Printing ${quote(mem)} to file\\n");
FILE *outfile_${quote(mem)} = fopen("outfile_${quote(mem)}.txt", "w");""")

    val iterators = (0 until mem.sizes.size).map { i =>
      s"""${quote(mem)}_iter_$i"""
    }.toList

    iterators.map { i =>
      fp(cf, s"""uint32_t $i = 0;""")
    }

    val iterMaxTuple = iterators.zip(mem.sizes)

    iterMaxTuple.map { t =>
      val iter = t._1
      val max = t._2
      fp(cf, s"""for ($iter = 0; $iter < ${quote(max)}; $iter++) {""")
    }

      val addrStr = (1 until iterMaxTuple.size).map { i =>
        val iter = iterMaxTuple(i-1)._1
        val max = iterMaxTuple(i)._2
        s"${iter}*${quote(max)}"
      }.mkString("+") + "+" + iterators.last
    fp(cf, s"""uint32_t addr = $addrStr;""")
    fp(cf, s"""fprintf(outfile_${quote(mem)}, "${formatStr(mem.t)} ", ${quote(mem)}[addr]);""")

    iterators.reverse.map { i =>
      fp(cf, s"""} // $i""")
      fp(cf, s"""fprintf(outfile_${quote(mem)},"\\n");""")
    }
  }

  def dumpScalars(argOut: List[ArgOut]) = {
    fp(cf, s"""FILE *cycles_out = fopen("cycles_out.txt", "w");""")
    fp(cf, s"""fprintf(cycles_out, "cycles = %lu\\n", cycles);""")
    fp(cf, s"""fclose(cycles_out);""")
    if (argOut.size > 0) {
      fp(cf, s"""
  fprintf(stderr, "Printing scalars to file\\n");
  FILE *outfile_scalars = fopen("outfile_scalars.txt", "w");""")

      argOut.map { a =>
        // MaxJ honors all input types, but up-casts floats to doubles for output arguments
        // Let's do the same here
        val format = formatStr(a.t) match {
          case "%f" => "%lf"
          case _ => formatStr(a.t)
        }
        fp(cf, s"""fprintf(outfile_scalars, "${quote(a)} = $format\\n", ${quote(a)});""")
      }
      fp(cf, s"""fclose(outfile_scalars);""")
    }
  }


  def mainEpilogue(mems: List[OffChipArray]) = {
    fp(cf, s"""
  // Cleanup""")

  mems.map { m => fp(cf, s"""free(${quote(m)});""") }
  fp(cf, """max_unload(engine);
fprintf(stderr, "Exiting\\n");
return 0;
}
""")
  }

  override def run(root: Node) = {
    initPass()

    val argIn = design.allNodes.filter(_.isInstanceOf[ArgIn]).toList.asInstanceOf[List[ArgIn]]
    val argOut = design.allNodes.filter(_.isInstanceOf[ArgOut]).toList.asInstanceOf[List[ArgOut]]

    // Assign off-chip memories
    val offChipArrays = design.allNodes.filter(_.isInstanceOf[OffChipArray]).toList.asInstanceOf[List[OffChipArray]]
    offChipArrays.map { arr => offChipArrayAddr += arr -> getNextLMemAddr() }


    val tileMemLds = design.allNodes.filter(_.isInstanceOf[TileMemLd]).map{_.asInstanceOf[TileMemLd]}.toSet
    val tileMemSts = design.allNodes.filter(_.isInstanceOf[TileMemSt]).map{_.asInstanceOf[TileMemSt]}.toSet
    val offChipOut = tileMemSts.map { t => t.baseAddr }.toList
    val offChipIn = tileMemLds.map { t => t.baseAddr }.filterNot(offChipOut.toSet).toList

    mainPreamble(argIn, argOut)
    offChipIn.map { a => initHostMemory(a) }
    offChipOut.map { a => initHostMemory(a, true) }
    configureFPGA()
    offChipIn.map { a => copyHostToFPGA(a) }
    runFPGA(argIn, argOut)
    offChipOut.map { a => copyFPGAToHost(a) }
    offChipOut.map { a => dumpResults(a) }
    dumpScalars(argOut)
    mainEpilogue(offChipIn ++ offChipOut)

    finPass()
  }

  override def initPass() = {
    emitIncludes()
    helperMethods()

    isInit = true
  }

  override def finPass() = {
    cf.flush()
    cf.close()
    typef.flush()
    cf.close()
  }
}
