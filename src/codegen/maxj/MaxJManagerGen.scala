package dhdl.codegen.maxj

import dhdl.Config
import dhdl.codegen.Codegen
import dhdl.graph._
import dhdl.Design
import java.io.PrintWriter
import java.io.File

class MaxJManagerGen(path: String)(implicit design: Design) extends Codegen {

  override val ext = "maxj"
  val mf: PrintWriter = new PrintWriter(path)
  val mImportPrefix = "com.maxeler.maxcompiler.v2"
  val streamClock =150
  val memClock = 400
  val mImports = List(
    "build.EngineParameters",
    "kernelcompiler.Kernel",
    "managers.standard.Manager",
    "managers.standard.Manager.MemAccessPattern",
    "managers.standard.Manager.IOType",
    "managers.engine_interfaces.EngineInterface",
    "managers.engine_interfaces.EngineInterface.Direction",
    "managers.engine_interfaces.InterfaceParam",
    "managers.engine_interfaces.CPUTypes",
    "managers.custom.CustomManager",
    "managers.custom.DFELink",
    "managers.custom.blocks.KernelBlock",
    "managers.custom.stdlib.DebugLevel",
    "managers.BuildConfig",
    "managers.custom.stdlib.MemoryControllerConfig",
    "kernelcompiler.KernelConfiguration",
    "kernelcompiler.KernelConfiguration.OptimizationOptions",
    "kernelcompiler.KernelConfiguration.OptimizationOptions.OptimizationTechnique"
  )

  val oldMemImports = List(
    "managers.custom.stdlib.MemoryControlGroup"
    )

  val newMemImports = List(
    "managers.custom.stdlib.LMemCommandGroup",
    "managers.custom.stdlib.LMemInterface"
  )

  def tpstr(t: Type) = {
    t match {
      case FixPt(d, f, s) =>
        val signedPrefix = if (s) "I" else "U"
        val body = (d+f) match {
          case x:Int if (x <= 32) =>
            "32"
          case x:Int if (x>32) =>
            "64"
        }
        s"${signedPrefix}${body}"
      case FloatPt(m, e) => if (m <= 24) s"FLOAT" else "DOUBLE"
      case _ => throw new Exception(s"Unknown type $t")
    }
  }

  val mPreamble =
s"""
class TopManager extends CustomManager {
  private static final CPUTypes I32 =    CPUTypes.INT32;
  private static final CPUTypes I64 =    CPUTypes.INT64;
  private static final CPUTypes U32 =    CPUTypes.UINT32;
  private static final CPUTypes U64 =    CPUTypes.UINT64;
  private static final CPUTypes FLOAT  = CPUTypes.FLOAT;
  private static final CPUTypes DOUBLE = CPUTypes.DOUBLE;
"""

  val mEpilogue =
s"""
  public static void main(String[] args) {
    TopManager m = new TopManager(new EngineParameters(args));

    BuildConfig c = new BuildConfig(BuildConfig.Level.FULL_BUILD);
    c.setBuildEffort(BuildConfig.Effort.HIGH);
    c.setEnableTimingAnalysis(true);
    m.setBuildConfig(c);

    m.createSLiCinterface(interfaceRead("readLMem"));
    m.createSLiCinterface(interfaceWrite("writeLMem"));
    m.createSLiCinterface(interfaceDefault());
    m.build();
  }
}
"""

val mReadIntf =
s"""
  // CPU -> LMEM (read interface)
  private static EngineInterface interfaceRead(String name) {
    EngineInterface ei = new EngineInterface(name);
    InterfaceParam size = ei.addParam("size", U32);
    InterfaceParam start = ei.addParam("start", U32);
    InterfaceParam sizeInBytes = size;

    // Stop the kernel from running
    ei.setScalar("TopKernel", "en", 0);

    // Set up address map and access pattern
    ei.setLMemLinear("fromlmem", start, sizeInBytes);
    ei.setStream("tocpu", U32, sizeInBytes);
    ei.ignoreAll(Direction.IN_OUT);
    return ei;
  }
"""

val mWriteIntf =
s"""
  // LMEM -> CPU (write interface)
  private static EngineInterface interfaceWrite(String name) {
    EngineInterface ei = new EngineInterface(name);
    InterfaceParam size = ei.addParam("size", U32);
    InterfaceParam start = ei.addParam("start", U32);
    InterfaceParam sizeInBytes = size;

    // Stop the kernel from running
    ei.setScalar("TopKernel", "en", 0);

    // Set up address map and access pattern
    ei.setLMemLinear("tolmem", start, sizeInBytes);
    ei.setStream("fromcpu", U32, sizeInBytes);
    ei.ignoreAll(Direction.IN_OUT);
    return ei;
  }
"""

val mDefaultIntfPreamble =
s"""
  // Interface to run DFE (default interface)
  private static EngineInterface interfaceDefault() {
    EngineInterface ei = new EngineInterface();
    ei.setTicks("TopKernel", Long.MAX_VALUE);
    ei.setScalar("TopKernel", "en", 1);
"""

val mDefaultIntfEpilogue =
s"""
    ei.setLMemInterruptOn("intrStream");
    ei.ignoreAll(Direction.IN_OUT);
    return ei;
  }
"""

val cpuIntfOld =
s"""
    // Set up CPU <-> FPGA stream
    DFELink fromcpu = addStreamFromCPU("fromcpu");
    DFELink tocpu = addStreamToCPU("tocpu");
    DFELink fromlmem = addStreamFromOnCardMemory("fromlmem", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
    DFELink tolmem = addStreamToOnCardMemory("tolmem", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
    tolmem <== fromcpu;
    tocpu <== fromlmem;
"""
val cpuIntfNew =
s"""
    // Set up CPU <-> FPGA stream
    DFELink fromcpu = addStreamFromCPU("fromcpu");
    DFELink tocpu = addStreamToCPU("tocpu");
    DFELink fromlmem = addStreamFromLMem("fromlmem", LMemCommandGroup.MemoryAccessPattern.LINEAR_1D);
    DFELink tolmem = addStreamToLMem("tolmem", LMemCommandGroup.MemoryAccessPattern.LINEAR_1D);
    tolmem <== fromcpu;
    tocpu <== fromlmem;
"""

val cpuIntf = if (Config.newMemAPI) cpuIntfNew else cpuIntfOld

val intrIntfOld =
s"""
    // Set up interrupt stream
    DFELink intrStream = addStreamToOnCardMemory("intrStream", k.getOutput("intrCmd"));
    intrStream <== k.getOutput("intrStream");
"""
val intrIntfNew =
s"""
    // Set up interrupt stream
    DFELink intrStream = addStreamToLmem("intrStream", k.getOutput("intrCmd"));
    intrStream <== k.getOutput("intrStream");
"""
val intrIntf = if (Config.newMemAPI) intrIntfNew else intrIntfOld

val mConstructorPreamble =
s"""
  TopManager(EngineParameters engineParameters) {
    super(engineParameters);

    // Disable stream status blocks
    DebugLevel debugLevel = new DebugLevel();
    debugLevel.setHasStreamStatus(false);
    debug.setDebugLevel(debugLevel);

    // Set up stream clock and memory clock
    config.setDefaultStreamClockFrequency($streamClock);
    config.setOnCardMemoryFrequency(LMemFrequency.MAX4MAIA_$memClock);
    config.setEnableAddressGeneratorsInSlowClock(true);

    // Allow non-multiple transitions for parallel tile loaders: may affect area
    config.setAllowNonMultipleTransitions(true);

    // Set up memory controller clock and config
//    MemoryControllerConfig mem_cfg = new MemoryControllerConfig();
////    mem_cfg.setBurstSize(4); //MAX3: 4 = 4*384 bits, 8 = 8*384 bits
//    mem_cfg.setDataReadFIFOExtraPipelineRegInFabric(true);
//    mem_cfg.setDataFIFOExtraPipelineRegInFabric(true); //timing-23may
//    //mem_cfg.setDataFIFOPrimitiveWidth(5*72);
//    config.setMemoryControllerConfig(mem_cfg);

    // Create a KernelConfiguration object that sets the OptimizationTechnique
    // to optimize for area, which is the default in the 2014.1 compiler
    // TODO: This causes build failures with MaxJ during source annotation. Investigate why
    // KernelConfiguration kernelConfig = getCurrentKernelConfig();
    // kernelConfig.optimization.setOptimizationTechnique(OptimizationTechnique.AREA);
    // KernelBlock k = addKernel(new TopKernel(makeKernelParameters("TopKernel", kernelConfig)));

    KernelBlock k = addKernel(new TopKernel(makeKernelParameters("TopKernel")));

    $cpuIntf

    $intrIntf
"""

val mConstructorEpilogue =
s"""
  }
"""

  def emitImports() = {
    mImports.map(i => fp(mf, s"import $mImportPrefix.$i;"))
    if (Config.newMemAPI) {
      newMemImports.map { i => fp(mf, s"import $mImportPrefix.$i;") }
    } else {
      oldMemImports.map { i => fp(mf, s"import $mImportPrefix.$i;") }
    }
  }

  def emitConstructor(offChipIn: List[(TileMemLd, OffChipArray)], offChipOut: List[(TileMemSt, OffChipArray)]) = {
    fp(mf, mConstructorPreamble)

    fp(mf, "    // Set up LMEM -> DFE streams (input streams to DFE)")
    offChipIn.map { t =>
      val node = t._1
      val arr = t._2
      val streamName = s"${arr}_${node}"
      fp(mf, s"""    DFELink ${streamName}_in = addStreamFromOnCardMemory("${streamName}_in", k.getOutput("${streamName}_in_cmd"));""")
      fp(mf, s"""    k.getInput("${streamName}_in") <== ${streamName}_in;""")
    }

    fp(mf, "    // Set up DFE -> LMEM (output streams from DFE)")
    offChipOut.map { t =>
      val node = t._1
      val arr = t._2
      val streamName = s"${arr}_${node}"
      fp(mf, s"""    DFELink ${streamName}_out = addStreamToOnCardMemory("${streamName}_out", k.getOutput("${streamName}_out_cmd"));""")
      fp(mf, s"""    ${streamName}_out <== k.getOutput("${streamName}_out");""")
    }

    fp(mf, mConstructorEpilogue)
  }

  def emitRWInterface() = {
    fp(mf, mReadIntf)
    fp(mf, mWriteIntf)
  }

  def emitDefaultInterface(argIn: List[ArgIn], argOut: List[ArgOut]) = {
    fp(mf, mDefaultIntfPreamble)
    argIn.map { a =>
      fp(mf, s"""    InterfaceParam ${quote(a)} = ei.addParam("${quote(a)}", ${tpstr(a.t)});""")
      fp(mf, s"""    ei.setScalar(\"TopKernel\", \"${quote(a)}\", ${quote(a)});""")
    }
    argOut.map { a =>
      fp(mf, s"""    ei.unignoreScalar(\"TopKernel\", \"${quote(a)}\");""")
    }
      fp(mf, s"""    ei.unignoreScalar(\"TopKernel\", \"cycles\");""")
    fp(mf, mDefaultIntfEpilogue)
  }

  override def run(root: Node) = {
    initPass()

    val argIn = design.allNodes.filter(_.isInstanceOf[ArgIn]).toList.asInstanceOf[List[ArgIn]]
    val argOut = design.allNodes.filter(_.isInstanceOf[ArgOut]).toList.asInstanceOf[List[ArgOut]]
    val tileMemLds = design.allNodes.filter(_.isInstanceOf[TileMemLd]).toList.asInstanceOf[List[TileMemLd]]
    val tileMemSts = design.allNodes.filter(_.isInstanceOf[TileMemSt]).toList.asInstanceOf[List[TileMemSt]]
    println(s"""tileMemLds = $tileMemLds""")
    val offChipIn = tileMemLds.map { t => (t,t.baseAddr) }
    val offChipOut = tileMemSts.map { t => (t,t.baseAddr) }
    emitConstructor(offChipIn, offChipOut)
    emitRWInterface()
    emitDefaultInterface(argIn, argOut)

    finPass()
  }

  override def initPass() = {
    fp(mf, "package engine;")
    emitImports()
    fp(mf, mPreamble)
    isInit = true
  }

  override def finPass() = {
    fp(mf, mEpilogue)
    mf.flush()
    mf.close()
  }

}
