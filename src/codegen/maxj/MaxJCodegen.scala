package dhdl.codegen.maxj

import dhdl.codegen.Codegen
import dhdl.graph._
import java.io.PrintWriter
import java.io.File
import scala.collection.mutable.HashMap
import scala.collection.mutable.Set
import dhdl.Design

class MaxJCodegen(implicit design: Design) extends Codegen with MaxJGenSeqSM with MaxJGenParSM with MaxJGenMPSM with MaxJGenDblBufSM {

  // Constants
  val prefix = "Top"
  override val ext = "maxj"

  // Initialized / created in initPass

  // PrintWriters
  var lf: PrintWriter = null

  def fp(a: Any) = lf.println(a)

  override def quote(x: Any): String = {
    x match {
      case Some(v) => v.toString
      case _ => x.toString
    }
  }

  val importPrefix = "com.maxeler.maxcompiler.v2"
  val imports = List(
    "kernelcompiler.stdlib.core.Count.Counter",
    "kernelcompiler.stdlib.core.CounterChain",
    "kernelcompiler.stdlib.core.Count",
    "kernelcompiler.stdlib.core.Count.Params",
    "kernelcompiler.stdlib.memory.Memory",
    "kernelcompiler.Kernel",
    "kernelcompiler.KernelParameters",
    "kernelcompiler.types.base.DFEVar",
    "utils.MathUtils",
    "utils.Bits",
    "kernelcompiler.KernelLib",
    "kernelcompiler.stdlib.KernelMath",
    "kernelcompiler.types.base.DFEType",
    "kernelcompiler.stdlib.core.Stream.OffsetExpr",
    "kernelcompiler.stdlib.Reductions",
    "kernelcompiler.SMIO",
    "kernelcompiler.stdlib.Accumulator",
    "kernelcompiler.types.base.DFEType",
    "kernelcompiler.types.composite.DFEVector",
    "kernelcompiler.types.composite.DFEVectorType",
    "kernelcompiler.types.base.DFEFix.SignMode"
  )

  val dirs = List("CPUCode", "EngineCode/src", "EngineCode/src/engine", "RunRules")

  // Set of nodes which already have their enable signal emitted
  val enDeclaredSet = Set[CtrlNode]()

  // Set of nodes which already have their done signal emitted
  val doneDeclaredSet = Set[CtrlNode]()

  // HashMap of CtrlNode -> MemNode populated during BRAM codegen
  // A CtrlNode -> MemNode entry exists if the CtrlNode is a reader
  // of the MemNode. This hashmap must be checked when every CtrlNode
  // is code generated, so that the 'done' signals are wired up
  // correctly
  val readerToMemMap = HashMap[CtrlNode, Set[MemNode]]()

  // HashMap of MemNodes (which are double buffers) to a set containing
  // all 'rdone' signals that have been emitted.
  val rdoneSet = HashMap[MemNode, Set[CtrlNode]]()

  override def run(root: Node) = {
    initPass()

    // First print all argIn nodes
    val argInNodes = design.allNodes.filter(_.isInstanceOf[ArgIn])
    argInNodes.map(visitNode(_))
    visitNode(root)
    val argOutNodes = design.allNodes.filter(_.isInstanceOf[ArgOut])
    argOutNodes.map(visitNode(_))


    // Emit manager
    val managerGen = new MaxJManagerGen(s"${outdir}/${dirs(2)}/${prefix}Manager.${ext}")
    managerGen.run(root)

    // Emit C host code
    val hostCodeGen = new MaxJHostCodeGen(s"${outdir}/${dirs(0)}/cpucode.c")
    hostCodeGen.run(root)

    finPass()
  }

  // TODO: Duplicated with C code.
  // Current TileLd/St templates expect that LMem addresses are
  // statically known during graph build time in MaxJ. That can be
  // changed, but until then we have to assign LMem addresses
  // statically. Assigning each OffChipArray a 384MB chunk now
  val burstSize = 384
  var nextLMemAddr = burstSize * 1024 * 1024
  def getNextLMemAddr() = {
    val addr = nextLMemAddr
    nextLMemAddr += burstSize * 1024 * 1024;
    addr/burstSize
  }

  val offChipArrayAddr = HashMap[OffChipArray, Int]()
  override def initPass() = {
    super.initPass()

    // Assign off-chip memories
    val offChipArrays = design.allNodes.filter(_.isInstanceOf[OffChipArray]).toList.asInstanceOf[List[OffChipArray]]
    offChipArrays.map { arr => offChipArrayAddr += arr -> getNextLMemAddr() }

    // Create Additional directories
    dirs.map(d => new File(s"$outdir/$d").mkdirs())

    // Copy templates, RunRules, Makefiles, TopKernel
    copyDir(s"$filesDir/templates", s"$outdir/${dirs(1)}")
    copyDir(s"$filesDir/RunRules", s"$outdir")
    copyFile(s"$filesDir/Makefile.c", s"$outdir/${dirs(0)}/Makefile")
    copyFile(s"$filesDir/Makefile.files.include", s"$outdir/${dirs(0)}/Makefile.files.include")
    copyFile(s"$filesDir/Makefile.top", s"$outdir/Makefile")
    copyFile(s"$filesDir/run.sh", s"$outdir/run.sh")
    copyFile(s"$filesDir/sedify.sh", s"$outdir/sedify.sh")
    copyFile(s"$filesDir/run_fpga.sh", s"$outdir/run_fpga.sh")
    copyFile(s"$filesDir/unpack.sh", s"$outdir/unpack.sh")
    copyFile(s"$filesDir/TopKernel.maxj", s"$outdir/${dirs(2)}/TopKernel.maxj")

    lf = new PrintWriter(s"${outdir}/${dirs(2)}/${prefix}KernelLib.${ext}")

    fp(s"""package engine;""")
    imports.map(x => fp(s"""import ${importPrefix}.${x};"""))
    fp(s"""class TopKernelLib extends KernelLib {""")
    fp(s"""TopKernelLib(KernelLib owner, DFEVar top_en, DFEVar top_done) {""")
    fp(s"""super(owner);""")
  }

  override def finPass() = {
    fp(s"}")
    fp(s"}")
    lf.flush()
    lf.close()
  }

  def tpstr(t: Type, vecWidth: Int = 1) : String = {
    val scalart = t match {
      case CompositeType(ts) =>
        assert(vecWidth == 1, "Error: Composity types with vecWidth > 1 not supported")
        "new DFEType[]{" + ts.map { tpstr(_) }.mkString(",") + "}"
      case FixPt(d, f, s) => if (s) s"dfeFixOffset(${d+f}, $f, SignMode.TWOSCOMPLEMENT)" else s"dfeFixOffset(${d+f}, $f, SignMode.UNSIGNED)"
      case FloatPt(m, e) => s"dfeFloat($e, $m)"
      case _ => throw new Exception(s"Unknown type $t")
    }
    if (vecWidth > 1) s"new DFEVectorType<DFEVar>($scalart, $vecWidth)" else scalart
  }

  def pre(n: CombNode) = {
    val str = n.vecWidth match {
      case 1 => "DFEVar"
      case _ => "DFEVector<DFEVar>"
    }
    val arraySuffix = n.t match {
      case CompositeType(_) => "[]"
      case _ => ""
    }
    str + arraySuffix
  }


  /**
   *  Return a boolean value indicating if this ReduceTree must be specialized
   *  in MaxJ. Currently accumulations (both integer and floating point) are specialized.
   *  TODO: streamMin, streamMax
   */
  def specializeReduce(r: ReduceTree) = {
    val lastGraph = r.graph.takeRight(1)(0)
    (lastGraph.nodes.size == 1) & (r.accum.input match {
      case in:Add => true
      case in:MinWithMetadata => true
      case in:MaxWithMetadata => true
      case in:Max => true
      case _ => false
    })
//    && (r.accum.t match {
//      case fpt:FixPt => true
//      case _ => false
//    })
  }

  /** Emit MaxJ CounterChain for a [[dhdl.graph.Ctr]] node instead of the CtrChainLib template.
   *  Using CtrChainLib to emit counters for [[dhdl.graph.Sequential]] or
   *  [[dhdl.graph.MetaPipeline]] causes an illegal loop. Using MaxJ stream
   *  offsets doesn't seem to help resolve the loop. One way to resolve this
   *  seems to be to use the CounterChain primitive in MaxJ, although it is
   *  more restrictive than CtrChainLib.
   *  @param n: The [[dhdl.graph.Ctr]] node to be emitted as a CounterChain
   *  @param en: String representing the 'enable' control signal to the CounterChain.
   *  This is typically the 'done' signal of the last stage of a [[dhdl.graph.Sequential]]
   *  node, or the 'done' signal of the first stage of a [[dhdl.graph.MetaPipeline]] node.
   *  For a [[dhdl.graph.Pipe]] counter, this field is set accordingly based on whether the
   *  Pipe is a unitPipe, Pipe with a ReduceTree or a Pipe with a Map accumulator.
   *  @param done: Optional string defining the driver for the 'done' signal. This is required
   *  to handle Pipes with Map accumulators.
   */
  def emitMaxJCounterChain(n: Ctr, en: Option[String], done: Option[String] = None) = {
    visited += n
    for (m <- n.max) {
      visitNode(m)
    }
    for (ctr <- n.ctrs) {
      visited += ctr
    }

    // 'En' and 'done' signal contract: Enable signal is declared here, done signal must be
    // declared before this method is called.
    // Both signals are defined here.
    if (!enDeclaredSet.contains(n)) {
      fp(s"""DFEVar ${quote(n)}_en = ${en.get};""")
      enDeclaredSet += n
    }

    fp(s"""CounterChain ${quote(n)}_chain = control.count.makeCounterChain(${quote(n)}_en);""")

    // For Pipes, max must be derived from PipeSM
    // For everyone else, max is as mentioned in the ctr
    val maxStrs = n.max.zipWithIndex map { t =>
      val m = t._1
      val i = t._2
      n.getParent() match {
        case p: Pipe => s"${quote(n)}_max_$i"
        case _ => quote(m)
      }
    }
    (0 until n.ctrs.size) map { i =>
      val ctr = n.ctrs(i)
      val max = maxStrs(i)
      val stride = n.stride(i)
      val vecWidth = n.par(i)
      val pre = if (vecWidth == 1) "DFEVar" else "DFEVector<DFEVar>"
      if (vecWidth == 1) {
        fp(s"""$pre ${quote(ctr)} = ${quote(n)}_chain.addCounter(${quote(max)}, ${quote(stride)});""")
      } else {
        fp(s"""$pre ${quote(ctr)} = ${quote(n)}_chain.addCounterVect($vecWidth, ${quote(max)}, ${quote(stride)});""")
      }
    }

    val doneStr = if (!done.isDefined) {
        s"stream.offset(${quote(n)}_chain.getCounterWrap(${quote(n.ctrs(0))}),-1)"
    } else {
      done.get
    }

    if (!doneDeclaredSet.contains(n)) {
      fp(s"""DFEVar ${quote(n)}_done = $doneStr;""")
      doneDeclaredSet += n
    } else {
      fp(s"""${quote(n)}_done <== $doneStr;""")
    }
  }

  override def visitNode(node: Node) : Unit = {
    if (!isInit) {
      initPass()
    }
    if (!visited.contains(node)) {
      visited += node
      node match {
        case n@Fix2Float(op, t) =>
          visitNode(op)
          fp(s"""DFEVar ${quote(n)} = ${quote(op)}.cast(${tpstr(t)});""")
        case n@Float2Fix(op, t) =>
          visitNode(op)
          fp(s"""DFEVar ${quote(n)} = ${quote(op)}.cast(${tpstr(t)});""")
        case n@Not(op) =>
          visitNode(op)
          fp(s"""${pre(n)} ${quote(n)} = ~($op);""")
        case n@And(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 & $op2;""")
        case n@Or(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 | $op2;""")
        case n@Gt(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 > $op2;""")
        case n@Le(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 <= $op2;""")
        case n@Ge(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 >= $op2;""")
        case n@Add(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 + $op2;""")
        case n@Sub(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 - $op2;""")
        case n@Mul(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 * $op2;""")
        case n@Abs(op) =>
          visitNode(op)
          // Issue #32: Cannot use overloaded exp method if vecWidth > 1, so
          // need to explicitly compute individual elements
          if (n.vecWidth > 1) {
            // Create sourceless Vector stream, then assign individually
            fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t, n.vecWidth)}.newInstance(this);""")
            for (i <- 0 until n.vecWidth) {
              fp(s"""${quote(n)}[$i] <== KernelMath.abs(${quote(op)}[$i]);""")
            }
          } else {
            fp(s"""${pre(n)} ${quote(n)} = KernelMath.abs(${quote(op)});""")
          }

        case n@Exp(op) =>
          visitNode(op)
          // Issue #32: Cannot use overloaded exp method if vecWidth > 1, so
          // need to explicitly compute individual elements
          if (n.vecWidth > 1) {
            // Create sourceless Vector stream, then assign individually
            fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t, n.vecWidth)}.newInstance(this);""")
            for (i <- 0 until n.vecWidth) {
              fp(s"""${quote(n)}[$i] <== KernelMath.exp(${quote(op)}[$i], ${tpstr(n.t)});""")
            }
          } else {
            fp(s"""${pre(n)} ${quote(n)} = KernelMath.exp(${quote(op)}, ${tpstr(n.t)});""")
          }


        case n@Log(op) =>
          visitNode(op)
          // Issue #32: Cannot use overloaded exp method if vecWidth > 1, so
          // need to explicitly compute individual elements
          if (n.vecWidth > 1) {
            // Create sourceless Vector stream, then assign individually
            fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t, n.vecWidth)}.newInstance(this);""")
            for (i <- 0 until n.vecWidth) {
              fp(s"""${quote(n)}[$i] <== KernelMath.log(new KernelMath.Range(-Float.MAX_VALUE, Float.MAX_VALUE), ${quote(op)}[$i], ${tpstr(n.t)});""")
            }
          } else {
            fp(s"""${pre(n)} ${quote(n)} = KernelMath.log(new KernelMath.Range(-Float.MAX_VALUE, Float.MAX_VALUE), ${quote(op)}, ${tpstr(n.t)});""")
          }

        case n@Sqrt(op) =>
          visitNode(op)
          // Issue #32: Cannot use overloaded exp method if vecWidth > 1, so
          // need to explicitly compute individual elements
          if (n.vecWidth > 1) {
            // Create sourceless Vector stream, then assign individually
            fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t, n.vecWidth)}.newInstance(this);""")
            for (i <- 0 until n.vecWidth) {
              fp(s"""${quote(n)}[$i] <== KernelMath.sqrt(null, ${quote(op)}[$i], ${tpstr(n.t)});""")
            }
          } else {
            fp(s"""${pre(n)} ${quote(n)} = KernelMath.sqrt(null, ${quote(op)}, ${tpstr(n.t)});""")
          }

        case n@Mux(sel, i1, i2) =>
          visitNode(sel)
          visitNode(i1)
          visitNode(i2)
          fp(s"""${pre(n)} ${quote(n)} = $sel ? $i1 : $i2;""")

        case n@MinWithMetadata(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          visitNode(n.sel)
          visitNode(n.key)
          visitNode(n.metadata)
          fp(s"""DFEVar[] ${quote(n)} = {${quote(n.key)}, ${quote(n.metadata)}};""")

        case n@MaxWithMetadata(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          visitNode(n.sel)
          visitNode(n.key)
          visitNode(n.metadata)
          fp(s"""DFEVar[] ${quote(n)} = {${quote(n.key)}, ${quote(n.metadata)}};""")

        case n@Max(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""DFEVar ${quote(n)} = (${quote(op1)} > ${quote(op2)}) ? ${quote(op1)} : ${quote(op2)};""")

        case n@Div(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""${pre(n)} ${quote(n)} = $op1 / $op2;""")
        case n@Lt(op1, op2) =>
          fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t, n.vecWidth)}.newInstance(this);""")
          visitNode(op1)
          visitNode(op2)
          fp(s"""${quote(n)} <== $op1 < $op2;""")
        case n@Eq(op1, op2) =>
          fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t, n.vecWidth)}.newInstance(this);""")
          visitNode(op1)
          visitNode(op2)
          fp(s"""${quote(n)} <== $op1 === $op2;""")
        case n: ArgIn =>
          fp(s"""DFEVar ${quote(n)} = io.scalarInput(\"${quote(n)}\", ${tpstr(n.t)});""")
        case n@ArgOut(id,value) =>
          visitNode(value)

          // [HACK]: Storing from an accumulator. Need to store from the value held using
          // Reductions.streamHold, else the older value is read
          val valueStr = if (value.isInstanceOf[Reg]) {
            val regData = value.asInstanceOf[Reg]
            s"${value}_hold"
          } else {
            s"$value"
          }


          val controlStr = if (!n.hasParent()) s"top_done" else s"${quote(n.getParent())}_done"
          fp(s"""io.scalarOutput(\"${quote(n)}\", $valueStr, ${tpstr(n.t)}, $controlStr);""")
        case n@Ld(mem, addr, stride) =>
          visitNode(mem)
          visitNode(addr)
          if (mem.isDblBuf) {
            fp(s"""${pre(n)} ${quote(n)} = $mem.connectRport($addr);""")
            if (mem.getReaders().contains(n.getParent())) {
              val r = n.getParent()
              if (!rdoneSet(mem).contains(r)) {
                fp(s"""${quote(mem)}.connectRdone(${quote(r)}_done);""")
                rdoneSet(mem) += r
              }
            }
          } else {
            mem match {
            // If loading from a PQ, set the 'rd_en'signal.
            // TODO: This signal can be passed to every MemNode - this special case can be removed
            case _:PQ =>
              fp(s"""${pre(n)} ${quote(n)} = $mem.connectRport($addr, ${n.getParent()}_en);""")
            case _ =>
              fp(s"""${pre(n)} ${quote(n)} = $mem.connectRport($addr);""")
            }
          }

          // Handle if loading a composite type
          n.compositeValues.zipWithIndex.map { t =>
            val v = t._1
            val idx = t._2
            visitNode(v)
            fp(s"""${quote(v)} <== ${quote(n)}[$idx];""")
          }
        case n@St(mem, addr, data, start, stride) =>
          visitNode(mem)
          visitNode(addr)
          visitNode(data)

          // [HACK]: Storing from an accumulator. Need to store from the value held using
          // Reductions.streamHold, else the older value is read
          val dataStr = if (data.isInstanceOf[Reg]) {
            val regData = data.asInstanceOf[Reg]
            s"${data}_hold"
          } else {
            s"$data"
          }
//          if (mem.isDblBuf) {
//          } else {
            if (mem.isAccum) {
              val offsetStr = s"${data.getParent()}_offset"

              // [HACK]: Storing into an accumulator must be driven by the ctr_en_from_pipesm coming from the parent's state machine.
              // If driven by ${parent}_en (which is a Pipe), the write stays enabled for a longer period because of the delay added
              // by PipeSM. This lets incorrect values through to BRAM.
              val parentPipe = n.getParent().asInstanceOf[Pipe]
              val parentCtr = parentPipe.ctr
              if (mem.isDblBuf) {
                fp(s"""$mem.connectWport(stream.offset(${quote(addr)}, -$offsetStr), stream.offset($dataStr, -$offsetStr), ${quote(parentCtr)}_en_from_pipesm, $start, $stride);""")
              } else {
                fp(s"""$mem.connectWport(stream.offset($addr, -$offsetStr), stream.offset($dataStr, -$offsetStr), ${quote(parentCtr)}_en_from_pipesm, $start, $stride);""")
              }
            } else {
              if (mem.isDblBuf) {
                fp(s"""// mem writer: ${mem.getWriter()} """)
                fp(s"""$mem.connectWport(${quote(addr)}, $dataStr, ${quote(n.getParent())}_en, ${n.start}, ${n.stride});""")
              } else {
                fp(s"""$mem.connectWport($addr, $dataStr, ${quote(n.getParent())}_en, ${n.start}, ${n.stride});""")
              }
            }
//          }
        case n@Const(value) =>
          if (value.isInstanceOf[Boolean]) {
            fp(s"""DFEVar ${quote(n)} = constant.var($value);""")
          } else {
            fp(s"""DFEVar ${quote(n)} = constant.var(${tpstr(n.t)}, $value);""")
          }
        case n@Slice(in, from, to) =>
          visitNode(in)
          fp(s"""${pre(n)} ${quote(n)} = ${quote(in)}[$from];""")
        case n: Ctr =>
          emitMaxJCounterChain(n, Some(s"${quote(n.getParent())}_en"))
//          for (m <- n.max) {
//            visitNode(m)
//          }
//          for (ctr <- n.ctrs) {
//            visitNode(ctr)
//          }
//
//          fp(s"""DFEVar ${quote(n)}_en = ${quote(n.getParent())}_en;""")
//          fp(s"""DFEVar ${quote(n)}_done = dfeBool().newInstance(this);""")
//          fp(s"""DFEVar[] ${quote(n)}_max = {${n.max.map(m=>s"${quote(m)}").mkString(",")}};""")
//          fp(s"""int[] ${quote(n)}_strides = {${n.stride.map(s=>s"${quote(s)}").mkString(",")}};""")
//          fp(s"""DFEVar[] ${quote(n)}_out = {${n.ctrs.map(c=>s"${quote(c)}").mkString(",")}};""")
//          fp(s"""OffsetExpr ${quote(n)}_offset = stream.makeOffsetAutoLoop(\"${quote(n)}_offset\");""")
//          fp(s"""OffsetExpr ${quote(n.getParent())}_offset = ${quote(n)}_offset;""")
//          fp(s"""CtrChainLib ${quote(n)} = new CtrChainLib(owner, ${quote(n)}_en, ${quote(n)}_done, ${quote(n)}_max, ${quote(n)}_strides, ${quote(n)}_out, ${quote(n)}_offset);""")
        case n@GraphNode(nodes @ _*) =>
          val sched = n.topSort
          n.deps.map(visitNode(_))
          sched.map(visitNode(_))

        case n@ReduceTree(oldval, accum, graph @_*) =>
          visitNode(oldval)
          graph.dropRight(1).map { visitNode(_) }
          if (specializeReduce(n)) {
            visited += accum
            // [HACK] If the CombNode writing to Reg has Pipe as a parent, use the PipeSM's ctr_en output as the enable signal.
            // See codegen rule for @Reg for more details
            val enSignalStr = accum.producer match {
              case p@Pipe(ctr,_) =>
                s"${quote(ctr)}_en_from_pipesm"
              case _ =>
                s"${quote(accum.producer)}_en"
            }
            val rstSignalStr = if(accum.hasReset) {
              visitNode(accum.reset)
            s"stream.offset(${quote(accum.reset.getouts()(0))}, -1)"
            } else {
              s"${quote(accum.getParent)}_rst_en"
            }
            // We don't emit the last graph. Hence, input to the specialized accumulator is
            // the output of the last-but-one graph i.e., the input of the input to accum which isn't accum itself.
            val input = accum.input.deps.filter (_ != accum)(0)
            fp(s"""// Specialize for accumulator here""")
            accum.input match {
              case Add(_,_) =>
                accum.t match {
                  case FixPt(_,_,_) =>
                    fp(s"""Accumulator.Params ${quote(accum)}_accParams = Reductions.accumulator.makeAccumulatorConfig(${tpstr(accum.t)}).withClear($rstSignalStr).withEnable($enSignalStr);""")
                    fp(s"""DFEVar ${quote(accum)}_hold = Reductions.accumulator.makeAccumulator(${quote(input)}, ${quote(accum)}_accParams);""")
                    fp(s"""DFEVar ${quote(accum)} = ${quote(accum)}_hold;""")
                  case FloatPt(_,_) =>
                    fp(s"""DFEVar ${quote(accum)}_hold = FloatingPointAccumulator.accumulateWithReset(${quote(input)}, ${enSignalStr}, $rstSignalStr, true);""");
                    fp(s"""DFEVar ${quote(accum)} = ${quote(accum)}_hold;""");
                  case _ =>
                    throw new Exception(s"Unsupported type ${accum.t} for reduction specialization!")
                }

              case m: MinWithMetadata =>
                  val inputAsCombnode = input.asInstanceOf[CombNode]
                  fp(s"""Reductions.StreamMinInfo ${quote(accum)}_mininfo = Reductions.streamMinWithMetadata(${inputAsCombnode.get(0)}.cast(dfeUInt(63)), ${inputAsCombnode.get(1)}, $rstSignalStr, $enSignalStr);""")
                  visited += accum.get(0)
                  visited += accum.get(1)
                  fp(s"""DFEVar ${quote(accum.get(0))} = ${quote(accum)}_mininfo.getMin();""")
                  fp(s"""DFEVar ${quote(accum.get(1))} = ${quote(accum)}_mininfo.getMetaData();""")
                  fp(s"""DFEVar ${quote(accum)}_hold =  ${quote(accum.get(1))};""")
                  fp(s"""DFEVar ${quote(accum)} = ${quote(accum)}_hold;""")

              case m: MaxWithMetadata =>
                  val inputAsCombnode = input.asInstanceOf[CombNode]
                  fp(s"""Reductions.StreamMaxInfo ${quote(accum)}_maxinfo = Reductions.streamMaxWithMetadata(${inputAsCombnode.get(0)}.cast(dfeUInt(63)), ${inputAsCombnode.get(1)}, $rstSignalStr, $enSignalStr);""")
                  visited += accum.get(0)
                  visited += accum.get(1)
                  fp(s"""DFEVar ${quote(accum.get(0))} = ${quote(accum)}_maxinfo.getMax();""")
                  fp(s"""DFEVar ${quote(accum.get(1))} = ${quote(accum)}_maxinfo.getMetaData();""")
                  fp(s"""DFEVar ${quote(accum)}_hold =  ${quote(accum.get(1))};""")
                  fp(s"""DFEVar ${quote(accum)} = ${quote(accum)}_hold;""")

              case Max(_,_) =>
                accum.t match {
                  case FixPt(_,_,_) =>
                    fp(s"""DFEVar ${quote(accum)}_hold = Reductions.streamMax(${quote(input)}, $rstSignalStr, $enSignalStr);""")
                    fp(s"""DFEVar ${quote(accum)} = ${quote(accum)}_hold;""")
                  case _ =>
                    throw new Exception(s"Unsupported type ${accum.t} for Max reduction specialization!")
                }
              case _ =>
                throw new Exception(s"Error: Cannot specialize for accum input ${accum.input}")
            }
          } else {
            visitNode(accum)
            visitNode(graph.takeRight(1)(0))
          }

        case n@Pipe(ctr, stages) =>
          if (!n.hasParent()) {
            fp(s"""DFEVar ${quote(n)}_en = top_en;""")
            fp(s"""DFEVar ${quote(n)}_done = dfeBool().newInstance(this);""")
            fp(s"""top_done <== ${quote(n)}_done;""")
            enDeclaredSet += n
            doneDeclaredSet += n
          }

          fp(s"""SMIO ${quote(n)}_sm = addStateMachine("${quote(n)}_sm", new PipeSM(this, ${ctr.ctrs.size}));""")
          fp(s"""${quote(n)}_sm.connectInput("sm_en", ${quote(n)}_en);""")
          fp(s"""${quote(n)}_done <== stream.offset(${quote(n)}_sm.getOutput("sm_done"),-1);""")
          (0 until ctr.max.size) map { i =>
            val max = ctr.max(i)
            visitNode(max)
            fp(s"""${quote(n)}_sm.connectInput("sm_maxIn_$i", ${quote(max)});""")
            fp(s"""DFEVar ${quote(ctr)}_max_$i = ${quote(n)}_sm.getOutput("ctr_maxOut_$i");""")
          }
          fp(s"""DFEVar ${quote(ctr)}_done = dfeBool().newInstance(this);""")
          fp(s"""${quote(n)}_sm.connectInput("ctr_done", ${quote(ctr)}_done);""")
          fp(s"""DFEVar ${quote(ctr)}_en_from_pipesm = ${quote(n)}_sm.getOutput("ctr_en");""")
          doneDeclaredSet += ctr

          fp(s"""DFEVar ${quote(n)}_rst_done = dfeBool().newInstance(this);""")
          fp(s"""${quote(n)}_sm.connectInput("rst_done", ${quote(n)}_rst_done);""")
          fp(s"""DFEVar ${quote(n)}_rst_en = ${quote(n)}_sm.getOutput("rst_en");""")

          // Identify if the map parts of this Pipe, in any stage, writes to an accumulator
          val allMapNodes = stages.map { _.mapNode.nodes }.flatten
          val writesToAccumRam = allMapNodes.filter { _.isInstanceOf[St] }.exists { _.asInstanceOf[St].mem.isAccum }

          // Identify if the map parts of this Pipe, in any stage, has a Reg
          val writesToReg = allMapNodes.exists { _.isInstanceOf[Reg] }

          if (ctr.isUnitCtr & !writesToAccumRam & !writesToReg) {
            fp(s"""${quote(n)}_rst_done <== constant.var(true);""")
          } else {
            fp(s"""OffsetExpr ${quote(n)}_offset = stream.makeOffsetAutoLoop(\"${quote(n)}_offset\");""")
            fp(s"""${quote(n)}_rst_done <== stream.offset(${quote(n)}_rst_en, -${quote(n)}_offset-1);""")
          }

          // --- Analyze stages for reductions ---
          // 1. Does any of the stages do a reduction which cannot be specialized? If so:
          //    - If Pipe writes to accumRam, throw exception.
          //      Why? Counter needs special control signaling in case there is a write to a BRAM accumulator
          //      This conflicts with the control signal required to make non-specialized reduce work
          //      The real fix: Generate specialized accumulator libraries at codegen that use multiple
          //      partial accumulators and a second reduction tree. This is similar to the FloatingPointAccumulator
          //      template. We need to know the latency of the reduction function statically to do this.
          //    - If the non-specializable reduction is in stage0, fine.
          //    - Else, throw an exception temporarily
          //      Why? Currently, non-specialized reductions are code-generated in a rather dumb way,
          //      using counters that stall the outer pipeline. Having more of these reduction trees
          //      and counters can make the Pipe unmanageable, where it would be stalled for most of
          //      the time.
          //      The real fix: Generate specialized accumulator libraries at codegen that use multiple
          //      partial accumulators and a second reduction tree. This is similar to the FloatingPointAccumulator
          //      template. We need to know the latency of the reduction function statically to do this.
          // 2. All reductions can be specialized? Great! Do nothing
          // 3. No reductions? Great! Do nothing
          var nonSpecializedReduce = false
          stages.zipWithIndex.map { t =>
            val (s, i) = (t._1, t._2)
            if (s.hasReduce) {
              if (!specializeReduce(s.reduceNode.get)) {
                // Non-specializable reduction cannot exist in conjunction with BRAM accumulation
                if (writesToAccumRam) {
                  throw new Exception(s"""Cannot have a non-specialized reduce AND accumulation to BRAM in the same Pipe!""")
                }
                // Non-specializable reduction, allowed only with stage0
                if (i != 0) {
                  throw new Exception(s"""Non-specialized reductions allowed only in stage 0!""")
                } else {
                  fp(s"""DFEVar ${quote(n)}_loopLengthVal = ${quote(n)}_offset.getDFEVar(this, dfeUInt(8));""")
                  fp(s"""CounterChain ${quote(n)}_redLoopChain = control.count.makeCounterChain(${quote(ctr)}_en_from_pipesm);""")
                  fp(s"""DFEVar ${quote(n)}_redLoopCtr = ${quote(n)}_redLoopChain.addCounter(${quote(n)}_loopLengthVal, 1);""")
                  fp(s"""DFEVar ${quote(n)}_redLoop_done = stream.offset(${quote(n)}_redLoopChain.getCounterWrap(${quote(n)}_redLoopCtr), -1);""")
                  nonSpecializedReduce = true
                }
              }
            }
          }

          // Counter emission code
          if (nonSpecializedReduce) {
              emitMaxJCounterChain(ctr, Some(s"${quote(ctr)}_en_from_pipesm & ${quote(n)}_redLoop_done"))
          } else {
            // Check if this is an accumulator map - whether it writes to a memory marked 'isAccum'
            if (writesToAccumRam) {
              emitMaxJCounterChain(ctr, Some(s"${quote(ctr)}_en_from_pipesm | ${quote(n)}_rst_en"),
                    Some(s"stream.offset(${quote(ctr)}_en_from_pipesm & ${quote(ctr)}_chain.getCounterWrap(${quote(ctr.ctrs(0))}), -${quote(n)}_offset-1)"))
            } else {
              emitMaxJCounterChain(ctr, Some(s"${quote(ctr)}_en_from_pipesm"))
            }
          }

          // Emit each stage
          stages.foreach { s =>
            fp(s"""// Stage ${quote(s)} {""")
            visitNode(s.mapNode)
            if (s.hasReduce) {
              visitNode(s.reduceNode.get)
            }
            fp(s"""// } Stage ${quote(s)}""")
          }

        case n@Parallel(nodes @ _*) =>
          val parFile = new PrintWriter(s"${outdir}/${dirs(2)}/${quote(n)}ParSM.${ext}")
          emitParallelSM(parFile, s"${quote(n)}", nodes.size)
          parFile.flush()
          parFile.close()

          // If control signals have not yet been defined, define them here
          if (!n.hasParent()) {
            fp(s"""DFEVar ${quote(n)}_en = top_en;""")
            fp(s"""DFEVar ${quote(n)}_done = dfeBool().newInstance(this);""")
            fp(s"""top_done <== ${quote(n)}_done;""")
            enDeclaredSet += n
            doneDeclaredSet += n
          }


          fp(s"""// Parallel ${quote(n)} {""")
          fp(s"""
            SMIO ${quote(n)}_sm = addStateMachine(\"${quote(n)}_sm\", new ${quote(n)}_ParSM(this));
            ${quote(n)}_sm.connectInput(\"sm_en\", ${quote(n)}_en);
            ${quote(n)}_done <== stream.offset(${quote(n)}_sm.getOutput(\"sm_done\"),-1);
            """)

          n.nodes.zipWithIndex.map { t =>
            val s = t._1
            val idx = t._2
           fp(s"""DFEVar ${quote(s)}_done = dfeBool().newInstance(this);
            ${quote(n)}_sm.connectInput(\"s${quote(idx)}_done\", ${quote(s)}_done);
            DFEVar ${quote(s)}_en = ${quote(n)}_sm.getOutput(\"s${quote(idx)}_en\");""")
            enDeclaredSet += s
            doneDeclaredSet += s
          }

          for (node <- n.nodes) {
            visitNode(node)
          }


          fp(s"""// Parallel ${quote(n)} }""")

        case n: TileMemLd =>
          // If TileMemLd is loading from one stream into many (>1) BRAMs,
          // it is in SOA mode - the input stream is an array-of-structs,
          // but each field in the input stream is read into a separate BRAM.
          // The 'soaMode' flag detects if we are in this mode
          val soaMode = if (n.buf.size > 1) true else false

          // Parallel loading of multiple entries is currently not supported
          // in soa mode. Explicitly catch that and error out here
          if (soaMode) {
            assert(n.par == 1, s"ERROR: TileMemLd $n is trying to load an array of structs from off-chip memory in SOA mode with parallelism (${n.par}) >1. This is not supported")
          }

          // If control signals have not yet been defined, define them here
          if (!n.hasParent()) {
            fp(s"""DFEVar ${quote(n)}_en = top_en;""")
            fp(s"""DFEVar ${quote(n)}_done = dfeBool().newInstance(this);""")
            fp(s"""top_done <== ${quote(n)}_done;""")
            enDeclaredSet += n
            doneDeclaredSet += n
          }

          visitNode(n.baseAddr)
          visitNode(n.colDim)
          visitNode(n.idx0)
          visitNode(n.idx1)
          n.buf.map { visitNode(_) }
          visitNode(n.force)
          visitNode(n.isLoading)

          // Emit control signals
          fp(s"""DFEVar ${quote(n)}_forceLdSt = ${quote(n.force)};""")
          fp(s"""DFEVar ${quote(n)}_isLoading = dfeBool().newInstance(this);""")

          // In soa mode, emit a single waddr stream and multiple wdata streams - one per field of struct - that go into the write ports of separate BRAMs
          if (soaMode) {
            fp(s"""DFEVar ${quote(n)}_waddr = dfeUInt(MathUtils.bitsToAddress(${n.tileDim0 * n.tileDim1})).newInstance(this);""")
            fp(s"""DFEVar[] ${quote(n)}_wdata = new DFEVar[${n.buf.size}];""")
            for (i <- 0 until n.buf.size) {
              fp(s"""${quote(n)}_wdata[$i] = ${tpstr(n.buf(i).t)}.newInstance(this);""")
            }
          } else {
            val scalarAddrT = s"dfeUInt(MathUtils.bitsToAddress(${n.tileDim0 * n.tileDim1}))"
            fp(s"""DFEVector<DFEVar> ${quote(n)}_waddr = new DFEVectorType<DFEVar>($scalarAddrT, ${n.par}).newInstance(this);""")
            fp(s"""DFEVector<DFEVar> ${quote(n)}_wdata = new DFEVectorType<DFEVar>(${tpstr(n.buf(0).t)}, ${n.par}).newInstance(this);""")
          }

          fp(s"""DFEVar ${quote(n)}_wen = dfeBool().newInstance(this);""")

          fp(s"""new BlockLoaderLib(
owner,
${quote(n)}_en, ${quote(n)}_done,
${quote(n)}_isLoading, ${quote(n)}_forceLdSt,
${quote(n.colDim)},
${quote(n.idx0)}, ${quote(n.idx1)},
${quote(n.baseAddr)}, \"${quote(n.baseAddr)}_${quote(n)}_in\",
${n.tileDim0}, ${n.tileDim1},
${quote(n)}_waddr, ${quote(n)}_wdata, ${quote(n)}_wen);""")

          if (soaMode) {
            for (i <- 0 until n.buf.size) {
              fp(s"""${n.buf(i)}.connectWport(${quote(n)}_waddr, ${quote(n)}_wdata[$i], ${quote(n)}_wen);""")
            }
          } else {
            fp(s"""${n.buf(0)}.connectWport(${quote(n)}_waddr, ${quote(n)}_wdata, ${quote(n)}_wen);""")
          }
          fp(s"""${n.isLoading} <== Reductions.streamHold(${quote(n)}_isLoading, ${quote(n)}_en);""")
        case n: TileMemSt =>
          // If control signals have not yet been defined, define them here
          if (!n.hasParent()) {
            fp(s"""DFEVar ${quote(n)}_en = top_en;""")
            fp(s"""DFEVar ${quote(n)}_done = dfeBool().newInstance(this);""")
            fp(s"""top_done <== ${quote(n)}_done;""")
            enDeclaredSet += n
            doneDeclaredSet += n
          }

          visitNode(n.baseAddr)
          visitNode(n.colDim)
          visitNode(n.idx0)
          visitNode(n.idx1)
          visitNode(n.buf)
          visitNode(n.force)
          val scalarAddrT = s"dfeUInt(MathUtils.bitsToAddress(${n.tileDim0 * n.tileDim1}))"
          fp(s"""DFEVector<DFEVar> ${quote(n)}_raddr = new DFEVectorType<DFEVar>($scalarAddrT, ${n.par}).newInstance(this);""")

          n.buf match {
            // If loading from a PQ, set the 'rd_en'signal.
            // TODO: This signal can be passed to every MemNode - this special case can be removed
            case _:PQ =>
              fp(s"""DFEVar ${quote(n)}_rdata = ${quote(n.buf)}.connectRport(${quote(n)}_raddr, ${quote(n)}_en);""")
            case _ =>
              fp(s"""DFEVector<DFEVar> ${quote(n)}_rdata = ${quote(n.buf)}.connectRport(${quote(n)}_raddr);""")
          }
          if (n.buf.isDblBuf) {
            if (n.buf.getReaders().contains(n)) {
              val r = n
              if (!rdoneSet(n.buf).contains(r)) {
                fp(s"""${quote(n.buf)}.connectRdone(${quote(r)}_done);""")
                rdoneSet(n.buf) += r
              }
            }
          }
          fp(s"""DFEVar ${quote(n)}_forceLdSt = ${quote(n.force)};""")
          fp(s"""DFEVar ${quote(n)}_isLoading = dfeBool().newInstance(this);""")
          fp(s"""new BlockStorerLib(
owner,
${quote(n)}_en, ${quote(n)}_done,
${quote(n)}_isLoading, ${quote(n)}_forceLdSt,
${quote(n.colDim)},
${quote(n.idx0)}, ${quote(n.idx1)},
${quote(n.baseAddr)}, \"${quote(n.baseAddr)}_${quote(n)}_out\",
${n.tileDim0}, ${n.tileDim1},
${quote(n)}_raddr, ${quote(n)}_rdata);""")

        case n@MetaPipeline(ctr, regChain, numIter, nodes @ _*) =>
          val mpFile = new PrintWriter(s"${outdir}/${dirs(2)}/${quote(n)}MPSM.${ext}")
          emitMPSM(mpFile, s"${quote(n)}", nodes.size)
          mpFile.flush()
          mpFile.close()
          fp(s"""// Metapipeline ${quote(n)} {""")

          // If control signals have not yet been defined, define them here
          if (!n.hasParent()) {
            fp(s"""DFEVar ${quote(n)}_en = top_en;""")
            fp(s"""DFEVar ${quote(n)}_done = dfeBool().newInstance(this);""")
            fp(s"""top_done <== ${quote(n)}_done;""")
            enDeclaredSet += n
            doneDeclaredSet += n
          }

          // Emit all nodes that are involved in calculating the
          // total number of iterations. Note that this makes sense
          // only if the controller is centralized. Currently this is the
          // only kind of controller supported.
          visitNode(n.numIter)

          fp(s"""DFEVar ${quote(n)}_numIter = ${quote(n.numIter.getouts()(0))};""")
          fp(s"""
            SMIO ${quote(n)}_sm = addStateMachine(\"${quote(n)}_sm\", new ${quote(n)}_MPSM(this));
            ${quote(n)}_sm.connectInput(\"sm_en\", ${quote(n)}_en);
            ${quote(n)}_sm.connectInput(\"sm_numIter\", ${quote(n)}_numIter);
            ${quote(n)}_done <== stream.offset(${quote(n)}_sm.getOutput(\"sm_done\"),-1);
            """)

          fp(s"""DFEVar ${quote(n)}_rst_en = ${quote(n)}_sm.getOutput("rst_en");""")

          n.nodes.zipWithIndex.map { t =>
            val s = t._1
            val idx = t._2
           fp(s"""DFEVar ${quote(s)}_done = dfeBool().newInstance(this);
            ${quote(n)}_sm.connectInput(\"s${quote(idx)}_done\", ${quote(s)}_done);
            DFEVar ${quote(s)}_en = ${quote(n)}_sm.getOutput(\"s${quote(idx)}_en\");""")
            enDeclaredSet += s
            doneDeclaredSet += s
          }

          fp(s"""DFEVar ${quote(ctr)}_done = dfeBool().newInstance(this);""")
          doneDeclaredSet += ctr
          emitMaxJCounterChain(ctr,  Some(s"${quote(n.nodes(0))}_done"))

          visitNode(n.regChain)

          for (node <- n.nodes) {
            visitNode(node)
          }

          if (readerToMemMap.contains(n)) {
            val dblbufSet = readerToMemMap(n)
            dblbufSet.foreach { dblbuf =>
              if (!rdoneSet(dblbuf).contains(n)) {
                fp(s"""${quote(dblbuf)}.connectRdone(${quote(n)}_done);""")
                rdoneSet(dblbuf) += n
              }
            }
          }
          fp(s"""// Metapipeline ${quote(n)} }""")
        case n@Sequential(ctr, numIter, nodes @ _*) =>
          val seqFile = new PrintWriter(s"${outdir}/${dirs(2)}/${quote(n)}_SeqSM.${ext}")
          emitSeqSM(seqFile, s"${quote(n)}", nodes.size)
          seqFile.flush()
          seqFile.close()
          // If control signals have not yet been defined, define them here
          if (!n.hasParent()) {
            fp(s"""DFEVar ${quote(n)}_en = top_en;""")
            fp(s"""DFEVar ${quote(n)}_done = dfeBool().newInstance(this);""")
            fp(s"""top_done <== ${quote(n)}_done;""")
            enDeclaredSet += n
            doneDeclaredSet += n
          }

          // Emit all nodes that are involved in calculating the
          // total number of iterations. Note that this makes sense
          // only if the controller is centralized. Currently this is the
          // only kind of controller supported.
          visitNode(n.numIter)

          fp(s"""DFEVar ${quote(n)}_numIter = ${quote(n.iters)};""")
          fp(s"""// Sequential ${quote(n)} {""")
          fp(s"""
            SMIO ${quote(n)}_sm = addStateMachine(\"${quote(n)}_sm\", new ${quote(n)}_SeqSM(this));
            ${quote(n)}_sm.connectInput(\"sm_en\", ${quote(n)}_en);
            ${quote(n)}_sm.connectInput(\"sm_numIter\", ${quote(n)}_numIter);
            ${quote(n)}_done <== stream.offset(${quote(n)}_sm.getOutput(\"sm_done\"),-1);
            """)

          fp(s"""DFEVar ${quote(n)}_rst_en = ${quote(n)}_sm.getOutput("rst_en");""")

          n.nodes.zipWithIndex.map { t =>
            val s = t._1
            val idx = t._2
           fp(s"""DFEVar ${s}_done = dfeBool().newInstance(this);
            ${quote(n)}_sm.connectInput(\"s${quote(idx)}_done\", ${quote(s)}_done);
            DFEVar ${quote(s)}_en = ${quote(n)}_sm.getOutput(\"s${quote(idx)}_en\");""")
            enDeclaredSet += s
            doneDeclaredSet += s
          }

          fp(s"""DFEVar ${quote(ctr)}_done = dfeBool().newInstance(this);""")
          doneDeclaredSet += ctr
          emitMaxJCounterChain(ctr,  Some(s"${quote(n.nodes.last)}_done"))

          for (node <- n.nodes) {
            visitNode(node)
          }

          if (readerToMemMap.contains(n)) {
            val dblbufSet = readerToMemMap(n)
            dblbufSet.foreach { dblbuf =>
              if (!rdoneSet(dblbuf).contains(n)) {
                fp(s"""${quote(dblbuf)}.connectRdone(${quote(n)}_done);""")
                rdoneSet(dblbuf) += n
              }
            }
          }
          fp(s"""// Sequential ${quote(n)} }""")
        case n@BRAM(size, isDblBuf) =>
          if (isDblBuf) {
            val ctrlFile = new PrintWriter(s"${outdir}/${dirs(2)}/${quote(n)}_DblBufSM.${ext}")
            val readers = n.getReaders().distinct
            readers.foreach { r =>
              if (!readerToMemMap.contains(r)) readerToMemMap += r -> Set[MemNode]()
              readerToMemMap(r) += n
            }
            rdoneSet += n -> Set()
            emitDblBufSM(ctrlFile, s"${quote(n)}", readers.size)
            ctrlFile.flush()
            ctrlFile.close()

            fp(s"""// ${quote(n)} has ${readers.size} readers - ${readers}""")
            fp(s"""SMIO ${quote(n)}_sm = addStateMachine("${quote(n)}_sm", new ${quote(n)}_DblBufSM(this));""")
            fp(s"""DblBufKernelLib ${quote(n)} = new DblBufKernelLib(this, ${quote(n)}_sm, ${size(0)}, ${size(1)}, ${tpstr(n.t)}, ${n.banks}, ${n.stride}, ${readers.size});""")

            //  If writer is a unit Pipe, wait until parent is finished
            val doneSig = n.getWriter().getOrElse(throw new Exception(s"BRAM $n has no writer")) match {
              case p: Pipe if p.isUnitPipe =>
                var writer = p.getParent()
                while (!(writer.isInstanceOf[Sequential] || writer.isInstanceOf[MetaPipeline])) {
                  writer = writer.getParent()
                }
                s"${quote(writer)}_done"
              case _ =>
                s"${quote(n.getWriter())}_done"

            }
            fp(s"""${quote(n)}.connectWdone($doneSig);""")
//            fp(s"""${quote(n)}.connectWdone(${quote(n.getWriter())}_done);""")
//            n.getReaders().map { r =>
//              fp(s"""${quote(n)}.connectRdone(${quote(r)}_done);""")
//            }
          } else {
            fp(s"""BramLib ${quote(n)} = new BramLib(this, ${size(0)}, ${size(1)}, ${tpstr(n.t)}, ${n.banks}, ${n.stride});""")
          }

        case n@PQ(depth, isDblBuf) =>
          if (n.banks > 1) {
            throw new Exception(s"Error: Cannot create a priority queue with >1 banks (attempted ${n.banks}")
          }
          val readers = n.getReaders().distinct
          if (readers.size > 1) {
            throw new Exception(s"Error: PQ does not currently support more than one reader (attempted ${readers.size}")
          }

          if (isDblBuf) {
            val ctrlFile = new PrintWriter(s"${outdir}/${dirs(2)}/${quote(n)}_DblBufSM.${ext}")
            rdoneSet += n -> Set()
            emitDblBufSM(ctrlFile, s"${quote(n)}", readers.size)
            ctrlFile.flush()
            ctrlFile.close()

            fp(s"""// ${quote(n)} has ${readers.size} readers - ${readers}""")
            fp(s"""SMIO ${quote(n)}_sm = addStateMachine("${quote(n)}_sm", new ${quote(n)}_DblBufSM(this));""")
            // TODO: This is incorrect
            fp(s"""PQDblBufKernelLib ${quote(n)} = new PQDblBufKernelLib(this, ${quote(n)}_sm, $depth, ${tpstr(n.t)}, ${n.banks}, ${n.stride}, ${readers.size});""")

            //  If writer is a unit Pipe, wait until parent is finished
            val doneSig = n.getWriter().get match {
              case p: Pipe if p.isUnitPipe =>
                var writer = p.getParent()
                while (!(writer.isInstanceOf[Sequential] || writer.isInstanceOf[MetaPipeline])) {
                  writer = writer.getParent()
                }
                s"${quote(writer)}_done"
              case _ =>
                s"${quote(n.getWriter())}_done"

            }
            fp(s"""${quote(n)}.connectWdone($doneSig);""")
          } else {
            fp(s"""PQ ${quote(n)} = new PQ(this, "${quote(n)}_pqsm", $depth, ${tpstr(n.t)});""")
          }

        case n@Reg(isDblBuf,init) =>
          val parent = if (!n.hasParent()) "top" else s"${quote(n.getParent())}"
          val wen = s"${quote(parent)}_en"
          val rst = if(n.hasReset) {
            visitNode(n.reset)
            s"stream.offset(${quote(n.reset.getouts()(0))}, -1)"
          }else {
            s"${quote(parent)}_rst_en"
          }
          val din = n.input
          if (n.t.isInstanceOf[CompositeType]) {
            throw new Exception("Cannot codegen composite Regs directly; must go through specializeReduce at the moment :-)")
          }
          if (isDblBuf) {
            visitNode(din)
            fp(s"""DblRegFileLib ${quote(n)}_lib = new DblRegFileLib(this, ${tpstr(n.t)}, \"${quote(n)}\", ${n.vecWidth});""")
            if (n.vecWidth > 1) {
              fp(s"""DFEVector<DFEVar> ${quote(n)} = ${quote(n)}_lib.readv();""")
            } else {
              fp(s"""DFEVar ${quote(n)} = ${quote(n)}_lib.read();""")
            }
            fp(s"""${quote(n)}_lib.write($din, ${quote(n.getWriter())}_done);""")
            fp(s"""${quote(n)}_lib.connectWdone(${quote(n.getWriter())}_done);""")
            n.getReaders().map { r =>
              fp(s"""${quote(n)}_lib.connectRdone(${quote(r)}_done);""")
            }
          } else {
            fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t)}.newInstance(this);""")
            visitNode(n.input)
            if (!n.input.hasParent) {
              throw new Exception(s"""Reg ${quote(n)}'s input ${n.input} does not have a parent. How is that possible?""")
            }

            // [HACK] If the CombNode writing to Reg has Pipe as a parent, use the PipeSM's ctr_en output as the enable signal
            // This is because Pipes are controlled by a PipeSM, which adds some delay to when the 'en' signal goes down. Due
            // to this delay, the Reg is staying enabled for more than necessary, hence accumulating incorrect value.
            val enSignalStr = n.producer match {
              case p@Pipe(ctr,_) =>
                s"${quote(ctr)}_en_from_pipesm"
              case _ =>
                s"${quote(n.producer)}_en"
            }
            fp(s"""DFEVar ${quote(n.input)}_real = $enSignalStr ? ${quote(n.input)} : ${quote(n)}; // enable""")

            // Hold signal for a reg: Control signal that is input to Reductions.streamHold that determines
            // when a given input is valid. Values are considered stable when:
            //  (1) The reset is high, or
            //  (2) There is a ReduceTree writing to the Reg, and the loop controlling the ReduceTree says that it is done.
            //  We need to pattern match on the Reg's producer to catch case (2), hence the block of code below.
            val holdSignalStr = n.producer match {
              case p@Pipe(ctr,_) =>
                if (p.hasReduce) {
                  s"""$rst | ${quote(n.producer)}_redLoop_done"""
                } else {
                  rst
                }
              case _ =>
                rst
            }

            fp(s"""DFEVar ${quote(n)}_hold = Reductions.streamHold(${quote(n.input)}_real, ($holdSignalStr));""")
            fp(s"""${quote(n)} <== $rst ? constant.var(${tpstr(n.t)},0) : stream.offset(${quote(n)}_hold, -${quote(n.producer)}_offset); // reset""")
          }

        case n@OffChipArray(id, sizes @ _*) =>
          fp(s"""int ${quote(n)} = ${offChipArrayAddr(n)};""")
        case n: Wire =>
          fp(s"""${pre(n)} ${quote(n)} = ${tpstr(n.t)}.newInstance(this); """)

        case n@Bundle(args@_*) =>
          fp(s"""DFEVar[] ${quote(n)} = new DFEVar[${args.size}];""")
          (0 until args.size).map { i =>
            fp(s"""${quote(n)}[$i] = ${tpstr(args(i).t)}.newInstance(this);""")
            fp(s"""${quote(n)}[$i] <== ${quote(args(i))};""")
          }
        case n@DummyCtrlNode() =>
          fp(s"""// ${quote(n)}""")
            fp(s"""${quote(n)}_done <== stream.offset(${quote(n)}_en, -1);""")
        case n: DummyMemNode =>
          fp(s"""DummyMemLib ${quote(n)} = new DummyMemLib(this, ${tpstr(n.t)}, ${n.banks});""")

        case _ =>
          super.visitNode(node)
      }
    }
  }
}
