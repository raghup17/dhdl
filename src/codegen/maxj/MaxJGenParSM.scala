package dhdl.codegen.maxj

import java.io.PrintWriter

trait MaxJGenParSM {
  private var stream: PrintWriter = null
  def emitParallelSM(pw: PrintWriter, name: String, numParallel: Int) = {
    stream = pw
  stream.println(s"""
  package engine;
  import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
  import com.maxeler.maxcompiler.v2.statemachine.DFEsmInput;
  import com.maxeler.maxcompiler.v2.statemachine.DFEsmOutput;
  import com.maxeler.maxcompiler.v2.statemachine.DFEsmStateEnum;
  import com.maxeler.maxcompiler.v2.statemachine.DFEsmStateValue;
  import com.maxeler.maxcompiler.v2.statemachine.kernel.KernelStateMachine;
  import com.maxeler.maxcompiler.v2.statemachine.types.DFEsmValueType;

  class ${name}_ParSM extends KernelStateMachine {

    // States
    enum States {
      INIT,
      RUN,
      DONE
    }

    // State IO
    private final DFEsmOutput sm_done;
    private final DFEsmInput sm_en;""");

    for(i <- 0 until numParallel) {
  stream.println(s"""
  private final DFEsmInput s${i}_done;
  private final DFEsmOutput s${i}_en;
  """)
    }

  stream.println(s"""
    // State storage
    private final DFEsmStateEnum<States> stateFF;
    private final DFEsmStateValue[] bitVector;

    private final int numParallel = $numParallel;
    // Initialize state machine in constructor
    public ${name}_ParSM(KernelLib owner) {
      super(owner);

      // Declare all types required to wire the state machine together
      DFEsmValueType counterType = dfeUInt(32);
      DFEsmValueType wireType = dfeBool();
      // Define state machine IO
      sm_done = io.output("sm_done", wireType);
      sm_en = io.input("sm_en", wireType);
  """)
  for(i <- 0 until numParallel) {
      stream.println(s"""
        s${i}_done = io.input("s${i}_done", wireType);
        s${i}_en = io.output("s${i}_en", wireType);
      """)
    }

  stream.println(s"""
      // Define state storage elements and initial state
      stateFF = state.enumerated(States.class, States.INIT);

      bitVector = new DFEsmStateValue[numParallel];
      for (int i=0; i<numParallel; i++) {
        bitVector[i] = state.value(wireType, 0);
      }
    }

    private void resetBitVector() {
      for (int i=0; i<numParallel; i++) {
        bitVector[i].next <== 0;
      }
    }

    @Override
    protected void nextState() {
      IF(sm_en) {
  """)

    for(i <- 0 until numParallel) {
      stream.println(s"""
          IF (s${i}_done) {
            bitVector[$i].next <== 1;
          }""")
    }

    stream.println(s"""
          SWITCH(stateFF) {
            CASE (States.INIT) {
              stateFF.next <== States.RUN;
            }
            """)

    stream.println(s"""
            CASE (States.RUN) {""")
    val condStr = (0 until numParallel).map("bitVector[" + _ + "]").reduce(_ + " & " + _)
    stream.println(s"""
              IF($condStr) {
                resetBitVector();
                stateFF.next <== States.DONE;
              }
            }

          CASE (States.DONE) {
            resetBitVector();
            stateFF.next <== States.INIT;
          }
          OTHERWISE {
            stateFF.next <== stateFF;
          }
        }
      }
    }
  """)

    stream.println("""
    @Override
    protected void outputFunction() {
      sm_done <== 0;""")
    for (i <- 0 until numParallel) {
      stream.println(s"""
        s${i}_en <== 0;""")
    }

    stream.println("""
      IF (sm_en) {
        SWITCH(stateFF) {
          CASE(States.RUN) {""")
    for (i <- 0 until numParallel) {
      stream.println(s"""s${i}_en <== ~(bitVector[${i}] | s${i}_done);""")
    }
    stream.println(s"""
          }
          CASE(States.DONE) {
            sm_done <== 1;
          }
        }
      }
    }
  }""")
    }

}
