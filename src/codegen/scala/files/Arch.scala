import java.io.{BufferedReader,BufferedWriter,InputStreamReader,OutputStreamWriter}
import java.io.PrintWriter

trait Parsable[T] {
  def parse(x: String): T
}

trait Arch {
  def parsable[A:Parsable] = implicitly[Parsable[A]]
  def mparse[A,B](m: Parsable[A]): Parsable[B] = m.asInstanceOf[Parsable[B]]

  case class Mem[T:Parsable](var v: Array[T]) {
    val pT = parsable[T]
    def apply(i: Int) = v(i)
    def update(i: Int, data: T) { v(i) = data }
  }
  case class Reg[T:Parsable](var v: T) { val pT = parsable[T] }

  object CMD extends Enumeration { val NONE, GET, SET, ACK, ERR = Value }

  // Code duplication...
  implicit object ParseDbl extends Parsable[Double]{ def parse(x:String) = x.toDouble }
  implicit object ParseInt extends Parsable[Int]{ def parse(x:String) = x.toInt }
  implicit object ParseLng extends Parsable[Long]{ def parse(x:String) = x.toLong }
  implicit object ParseBln extends Parsable[Boolean]{ def parse(x:String) = x.toBoolean }

  /*
    Valid Input Commands:
    get register contents:                GET REG_ID
    get memory contents at INDEX:         GET MEM_ID INDEX
    set register contents to DATA:        SET REG_ID DATA
    set memory contents at INDEX to DATA: SET MEM_ID INDEX DATA
    begin next phase:                     ACK
    terminate early:                      ERR
    no action:                            NONE

    Tokens are separated by newline characters
  */

  private val reader: BufferedReader = new BufferedReader(new InputStreamReader(System.in))
  private val writer: BufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out))
  private var mems: Map[String, Mem[_]] = Map.empty
  private var regs: Map[String, Reg[_]] = Map.empty
  private var args: Set[String] = Set.empty
  private var setArgs: Set[String] = Set.empty

  def registerMems(m: (String, Mem[_])*) { mems ++= m }
  def registerRegs(m: (String, Reg[_])*) { regs ++= m }
  def registerArgs(m: String*) { args ++= m }

  private def readln() = { reader.readLine }

  private def writeln(str: String) {
    writer.write(str)
    writer.newLine
    writer.flush
  }
  private def sendCmd(cmd: CMD.Value) { writeln(cmd.id.toString) }

  private def sendErr(error: String) {
    sendCmd(CMD.ERR)
    writeln(error)
  }

  private def setMem[T:Parsable](id: String, mem: Mem[_], i: Int, data: T) {
    //log.println(s"Setting memory $id $mem at index $i to $data")
    if (mem eq null)
      sendErr(s"Error setting memory $id: cannot set memory prior to setting all input arguments.")
    else {
      mem.asInstanceOf[Mem[T]].v.update(i, data)
      sendCmd(CMD.ACK)
    }
  }

  private def getMem(id: String, mem: Mem[_], i: Int) {
    if (mem eq null)
      sendErr(s"Error getting memory $id: cannot get memory prior to setting all input arguments.")
    else {
      val data = mem.apply(i)
      //log.println(s"Getting memory $id $mem at index $i: $data")
      sendCmd(CMD.ACK)
      writeln(data.toString)
    }
  }

  private def setReg[T:Parsable](reg: Reg[_], data: T) {
    reg.asInstanceOf[Reg[T]].v = data
    sendCmd(CMD.ACK)
  }

  private def getReg(reg: Reg[_]) {
    val data = reg.v
    sendCmd(CMD.ACK)
    writeln(data.toString)
  }

  private def get() {
    val id = readln()

    if (mems.contains(id))
      getMem(id, mems(id), readln.toInt)
    else if (regs.contains(id))
      getReg(regs(id))
    else
      sendErr(s"No memory with id $id found")
  }

  private def set() {
    val id = readln()

    if (mems.contains(id)) {
      val mem = mems(id)
      val i = readln.toInt
      val v = mem.pT.parse(readln)
      setMem(id, mem, i, v)(mparse(mem.pT))
    }
    else if (regs.contains(id)) {
      if (setArgs.contains(id)) {
        sendErr(s"Cannot set input argument $id twice")
      }
      else {
        if (args.contains(id)) { args -= id; setArgs += id }
        val reg = regs(id)
        val v = reg.pT.parse(readln)
        setReg(reg, v)(mparse(reg.pT))
      }
    }
    else
      sendErr(s"No memory with id $id found")
  }

  var memsInitialized = false

  def setup() {
    //log.println("In setup mode"); log.flush()
    var cmd = CMD.NONE
    while (cmd != CMD.ACK) {
      try { cmd = CMD(readln.toInt) }
      catch { case _: Throwable => cmd = CMD.ERR }
      cmd match {
        case CMD.GET => get()
        case CMD.SET => set()
        case CMD.ACK => // Start run
        case CMD.ERR => sys.exit(-1)
        case CMD.NONE => // Ignore
      }
      //log.println("Received command " + cmd); log.flush()

      if (args.isEmpty && !memsInitialized) {
        initMems
        memsInitialized = true
      }
    }
    if (!memsInitialized) { initMems; memsInitialized = true }
  }

  def arch(): Unit
  def initMems(): Unit

  def check() {
    //log.println("In check mode"); log.flush()
    var cmd = CMD.NONE
    while (cmd != CMD.ACK) {
      try { cmd = CMD(readln.toInt) }
      catch { case _: Throwable => cmd = CMD.ERR }
      cmd match {
        case CMD.GET => get()
        case CMD.SET => // Do nothing
        case CMD.ACK => // Complete
        case CMD.ERR => sys.exit(-1)
        case CMD.NONE => // Ignore
      }
    }
  }

  //val log = new PrintWriter("log.txt")
  //def regExit() { log.println("Exiting."); log.flush(); log.close() }

  def main(args: Array[String]) {
    sendCmd(CMD.ACK)  // Synchronize output stream
    setup()
    arch()
    check()
    //regExit()
  }
}