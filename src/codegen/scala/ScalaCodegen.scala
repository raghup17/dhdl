package dhdl.codegen.scalagen // bad things happen if this is just named codegen.scala

import dhdl.codegen.Codegen
import dhdl.graph._
import java.io.PrintWriter
import java.io.File
import dhdl.Design

class ScalaCodegen(implicit design: Design) extends Codegen {

  val prefix = "Top"
  override val ext = "scala"
  var tab = 0

  var lf: PrintWriter = null
  def pl(a: Any) { lf.println("  "*tab + a) }
  def po(a: Any) { pl(a); tab += 1 }
  def pc(a: Any) { tab -= 1; pl(a) }


  def pregen(n: Node) = n match {
    case n: TileMemLd =>
      // Error out for struct loads
      if (n.buf.size > 1) throw new Exception("TileMemLd not implemented yet for CompositeType loads!")
      po(s"object $n {")
      pl("private var prevStartRow = -1")
      pl("private var prevStartCol = -1")

      po("def apply(startRow: Int, startCol: Int, cols: Int) {")
      pl(s"${n.isLoading} = startRow != prevStartRow || startCol != prevStartCol || prevStartCol == -1 || prevStartRow == -1")
      pl("prevStartRow = startRow")
      pl("prevStartCol = startCol")

      po(s"if (${n.isLoading}) {")
      // Copy from base array to tile
      po(s"for(i <- 0 until ${quote(n.tileDim0)}.toInt) {")
      po(s"for(j <- 0 until ${quote(n.tileDim1)}.toInt) {")
      pl(s"val addr_in = ( (i + startRow) * cols) + j + startCol")
      pl(s"val addr_out = (i * ${quote(n.tileDim1)}) + j")
      pl(s"${n.buf.head}(addr_out) = ${n.baseAddr}.apply(addr_in)")
      pc("}")
      pc("}")
      pc("}")
      pc("}")
      pc("}")
      pl("")

    case n: TileMemSt =>
      po(s"object $n { ")
      pl("private var prevStartRow = -1")
      pl("private var prevStartCol = -1")

      po("def apply(startRow: Int, startCol: Int, cols: Int) {")
      pl(s"${n.isStoring} = startRow != prevStartRow || startCol != prevStartCol || prevStartCol == -1 || prevStartRow == -1")

      po(s"if (${n.isStoring}) {")
      po(s"for(i <- 0 until ${quote(n.tileDim0)}.toInt) {")
      po(s"for(j <- 0 until ${quote(n.tileDim1)}.toInt) {")
      pl(s"val addr_out = ( (i + startRow) * cols) + j + startCol")
      pl(s"val addr_in = (i * ${quote(n.tileDim1)}) + j")
      pl(s"${n.baseAddr}(addr_out) = ${n.buf}.apply(addr_in)")
      pc("}")
      pc("}")
      pc("}")
      pc("}")
      pc("}")

    case _ =>
  }

  var argsOut: List[ArgOut] = Nil

  override def initPass() = {
    super.initPass()
    copyFile(s"$filesDir/Makefile", s"$outdir/Makefile")
    copyFile(s"$filesDir/Arch.scala", s"$outdir/Arch.scala")

    lf = new PrintWriter(s"${outdir}/${prefix}.${ext}")

    // TODO: Briefer way to express filter with type casting?
    val offChip = design.allNodes.flatMap{case m: OffChipArray => Some(m); case _ => None}
    val localMem = design.allNodes.flatMap{case m: BRAM => Some(m); case _ => None}
    val regs = design.allNodes.flatMap{case r: Reg => Some(r); case _ => None}
    val argsIn = design.allNodes.flatMap{case a: ArgIn => Some(a); case _ => None}
    argsOut = design.allNodes.flatMap{case a: ArgOut => Some(a); case _ => None}.toList
    val wires = design.allNodes.flatMap{case a: Wire => Some(a); case _ => None}

    po(s"object $prefix extends Arch {")

    pl("/* Constants */")
    design.allNodes.filter(_.isInstanceOf[Const[_]]).foreach(visitNode)
    pl("")
    // Main problem with generating software from hardware description is that everything in hardware
    // exists in a global namespace (i.e. space). In software this requires either some fancy analsis or
    // just preallocating everything upfront prior to running the program. Going with the preallocation for now.

    // Generated Scala can't handle more than some maximum number of arguments / register call
    def registerMems[T](reg: String, nodes: Iterable[T])(id: T => String) {
      nodes.grouped(20).foreach{group =>
        pl(s"register${reg}(" + group.map{mem => s"""\"${id(mem)}\"->$mem"""}.mkString(",") + ")")
      }
    }

    pl("/* Wires */")
    wires.foreach{n => pl(s"var $n: ${tpstr(n.t)} = ${zero(n.t)} // ${n.getParent}")}

    pl("/* Registers */")
    regs.foreach{n => pl(s"val $n = Reg[${tpstr(n.t)}](${n.init.getOrElse(zero(n.t))})")}
    registerMems("Regs", regs)(_.id)
    pl("")

    pl("/* Input Arguments */")
    argsIn.foreach{n => pl(s"val $n = Reg[${tpstr(n.t)}](${zero(n.t)})")}
    registerMems("Regs", argsIn)(_.id)
    argsIn.grouped(20).foreach{group =>
      pl("registerArgs(" + group.map{mem => s""" \"${mem.id}\" """}.mkString(",") + ")")
    }
    pl("")

    pl("/* Output Arguments */")
    argsOut.foreach{n => pl(s"val $n = Reg[${tpstr(n.t)}](${zero(n.t)}) // ${n.value}")}
    registerMems("Regs", argsOut)(_.id)
    pl("")

    pl("/* Off-Chip Memories */")
    // TODO: Bit of a problem here - size of off-chip arrays is dependent on input arguments, but
    // the user may try to write to an array before it's been sized. Best way to resolve this?
    offChip.foreach{n => pl(s"val $n = Mem[${tpstr(n.t)}](null)") }
    registerMems("Mems", offChip)(_.id)
    pl("")

    pl("/* On-Chip Memories */")
    localMem.foreach{n => 
      val temp = n.size(0) * n.size(1);
      pl(s"val $n = Mem(new Array[${tpstr(n.t)}](${temp}))")}
    registerMems("Mems", localMem)(_.id)
    pl("")

    po("def initMems() {")
    offChip.foreach{n =>
      val size = if (n.sizes.isEmpty) "0" else n.sizes.map(quote(_)).mkString("*")
      pl(s"$n.v = new Array[${tpstr(n.t)}]((${size}).toInt)")
    }
    pc("}")
    pl("")

    design.allNodes.foreach(pregen)

    po("def arch() {")
  }

  override def finPass() = {
    pc("} // End arch")
    pc{"} // End design"}
    lf.flush()
    lf.close()
  }

  /* Stringify scalar types */
  def tpstr(t: Any) = t match {
    case t if t.isInstanceOf[Boolean] => "Boolean"
    case FixPt(1, 0, _) => "Boolean"
    case FixPt(_, f, _) if f > 0 => "Double"
    case FixPt(d, _, s) if (d <= 32 && s) || (d <= 31 && !s) => "Int"
    case FixPt(d, _, _) =>"Long"
    case FloatPt(_, _) => "Double"
    case _ => throw new Exception(s"Unknown type $t")
  }
  def zero(t: Any) = t match {
    case t if t.isInstanceOf[Boolean] => "false"
    case FixPt(1, 0, _) => "false"
    case FixPt(_, f, _) if f > 0 => "0.0"
    case FixPt(_, _, _) => 0
    case FloatPt(_,_) => "0.0"
    case _ => throw new Exception(s"Unknown type $t")
  }

  override def quote(x: Any) = x match {
    case x: Reg => s"${x}.v"
    case a: ArgIn => s"${a}.v"
    case a: ArgOut => s"${a}.v"
    case _ => x.toString
  }

  def emitReset(n: Node) = n match {
    case n: Reg =>
      pl(s"${quote(n)} = ${n.init.getOrElse(zero(n.t))}")

    case _ => // TODO: Does any other node need reset? BRAMs maybe?
  }

  // HACK: Asynchronous reset for controller nodes
  def emitChildrenReset(n: CtrlNode) {
    design.allNodes.filter{c => c.hasParent && c.getParent == n}.foreach(emitReset)
  }

  // HACK: ArgOut apparently isn't always in the traversal pass (should it be?)
  // This is used to update the argOut value immediately after its input is updated
  def emitOuts(n: CombNode) {
    argsOut.filter(_.value == n).foreach(visitNode)
  }

  override def visitNode(node: Node) : Unit = {
    if (!isInit) { initPass() }
    if (!visited.contains(node)) {
      visited += node

      node match { case n: CtrlNode => emitChildrenReset(n); case _ => }

      node match {
      case n@Fix2Float(op, t) =>
        visitNode(op)
        pl(s"val $n = ${quote(op)}.to${tpstr(t)}")

      case n@Float2Fix(op, t) =>
        visitNode(op)
        pl(s"val $n = ${quote(op)}.to${tpstr(t)}")


      case n@Add(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} + ${quote(op2)}")

      case n@Sub(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} - ${quote(op2)}")

      case n@Mul(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} * ${quote(op2)}")

      case n@Mux(sel, i1, i2) =>
        visitNode(sel)
        visitNode(i1)
        visitNode(i2)
        pl(s"val $n = if(${quote(sel)}) { ${quote(i1)} } else { ${quote(i2)} }")

      case n@Max(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = if(${quote(op1)} > ${quote(op2)}) { ${quote(op1)} } else { ${quote(op2)} }")

      case n@Div(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} / ${quote(op2)}")

      case n@Gt(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} > ${quote(op2)}")

      case n@Lt(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} < ${quote(op2)}")

      case n@Eq(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} == ${quote(op2)}")

      case n@Le(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} <= ${quote(op2)}")

      case n@Ge(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} >= ${quote(op2)}")

      // TODO: This only works on Long, Int, and Boolean
      case n@And(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} & ${quote(op2)}")

      case n@Or(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
        pl(s"val $n = ${quote(op1)} | ${quote(op2)}")

      case n@Abs(op) =>
        visitNode(op)
        pl(s"val $n = math.abs(${quote(op)})")

      case n@Exp(op) =>
        visitNode(op)
        pl(s"val $n = math.exp(${quote(op)})")

      case n@Log(op) =>
        visitNode(op)
        pl(s"val $n = math.log(${quote(op)})")

      case n@Sqrt(op) =>
        visitNode(op)
        pl(s"val $n = math.sqrt(${quote(op)})")

      case n@Not(op1) =>
        visitNode(op1)
        pl(s"val $n = !${quote(op1)}")

      case n: ArgIn =>

      case n: ArgOut =>
        visitNode(n.value)
        pl(s"${quote(n)} = ${quote(n.value)}")

      case n@Ld(mem, addr, stride) =>
        visitNode(mem)
        visitNode(addr)
        pl(s"val $n = $mem.apply(${quote(addr)}.toInt)")

      case n@St(mem, addr, data, start, stride) =>
        visitNode(mem)
        visitNode(addr)
        visitNode(data)
        pl(s"$mem(${quote(addr)}.toInt) = ${quote(data)}")

      case n@Const(value) =>
        pl(s"val $n: ${tpstr(n.t)} = ${quote(value)}  // Constant")

      // TODO: What is the offset / OffsetAutoLoop?
      case n: Ctr =>
        for (m <- n.max) { visitNode(m) }
        val maxNStride = n.max.zip(n.stride)
        n.ctrs.zip(maxNStride).foreach{ case(ctr,(max,stride)) =>
          po(s"for(${quote(ctr)} <- 0 until ${quote(max)}.toInt by ${quote(stride)}.toInt) {")
        }

      case n@GraphNode() =>
        val sched = n.topSort
        sched.map(visitNode(_))

      case n@Pipe(ctr, stages) if n.isUnitPipe =>
        stages.foreach { visitNode(_) }

      // HACK: Have combinational node outputs escape from pipe
      case n@Pipe(ctr, stages) =>
        val outs = stages.map { s =>
          val (map, red) = (s.mapNode, s.reduceNode)
          if (red.isEmpty) {
            map.getouts().filter(o => !o.isInstanceOf[Reg] && o.isInstanceOf[CombNode]).asInstanceOf[List[CombNode]]
          } else {
            List[CombNode]()
          }
        }.flatten
        outs.foreach(out => pl(s"var ${quote(out)}_temp: ${tpstr(out.t)} = ${zero(out.t)}"))
        outs.foreach(out => pl(s"var ${quote(out)}: ${tpstr(out.t)} = ${zero(out.t)}"))
        visitNode(ctr)
        stages.foreach { visitNode(_) }
        outs.foreach(out => pl(s"${quote(out)}_temp = ${quote(out)}"))
        for (i <- 0 until ctr.size) { pc(s"} End of Pipe $n") }
        outs.foreach(out => pl(s"${quote(out)} = ${quote(out)}_temp"))

      case n@Stage(map, red) =>
        visitNode(map)
        if (n.hasReduce) visitNode(red.get)

      case n@Parallel(nodes @ _*) =>
        pl(s"// Begin Parallel $n ")
        for (node <- n.nodes) {
          visitNode(node)
        }
        pl(s"// End Parallel $n ")

      case n: TileMemLd =>
        visitNode(n.baseAddr)  // Should have already been emitted
        visitNode(n.colDim)
        visitNode(n.idx0)
        visitNode(n.idx1)
        n.buf.map { visitNode(_) }
        // visitNode(n.force) - ignoring force for now
        pl(s"${n}(${quote(n.idx0)}.toInt, ${quote(n.idx1)}.toInt, ${quote(n.colDim)}.toInt)")

      case n: TileMemSt =>
        visitNode(n.baseAddr)  // Should have already been emitted
        visitNode(n.colDim)
        visitNode(n.idx0)
        visitNode(n.idx1)
        visitNode(n.buf)
        // visitNode(n.force) - ignoring force for now
        pl(s"${n}(${quote(n.idx0)}.toInt, ${quote(n.idx1)}.toInt, ${quote(n.colDim)}.toInt)")

      case n@MetaPipeline(ctr, regChain, numIter, nodes @ _*) =>
        pl(s" // Begin Metapipeline $n ")
        visitNode(ctr)
        for (node <- n.nodes) {
          visitNode(node)
        }
        for (i <- 0 until ctr.size) { pc(s"} // End Metapipeline $n") }

      case n@Sequential(ctr, numIter, nodes @ _*) =>
        pl(s"// Begin Sequential $n")
        visitNode(ctr)
        for (node <- n.nodes) {
          visitNode(node)
        }
        for (i <- 0 until ctr.size) { pc(s"} // End Sequential $n") }

      case n@Reg(isDblBuf,init) =>
        visitNode(n.input)
        pl(s"${quote(n)} = ${quote(n.input)} ")

      case n@BRAM(depth, isDblBuf) =>
      case n@OffChipArray(id, sizes @ _*) =>

      case n@Bundle(args@_*) =>
        throw new Exception("Scala concatentation not implemented yet")

      // Wire is equivalent to bound var - shouldn't emit anything here
      case n: Wire =>

      case n@DummyCtrlNode() =>
        pl(s"// $n - dummy control node")

      case _ =>
        super.visitNode(node)
      }

      node match { case n: CombNode => emitOuts(n); case _ => }

    }


  }



}
